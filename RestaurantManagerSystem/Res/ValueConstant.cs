﻿
namespace GoldManager.Res
{
    public class ValueConstant
    {

        #region Session value
        /// <summary>
        /// Key in session to store bookmark page.
        /// </summary>
        public const string SESSION_BOOKMARK_KEY = "SESSION_BOOKMARK_KEY";

        /// <summary>
        /// Store scope of screens that will be available for temp session.
        /// </summary>
        public const string SESSION_TEMP_SCOPE_KEY = "SESSION_TEMP_SCOPE_KEY";

        /// <summary>
        /// Store temp data to session with this key.
        /// </summary>
        public const string SESSION_TEMP_KEY = "SESSION_TEMP_KEY";

        /// <summary>
        /// Store Nurse Licenseno to session with this key.
        /// </summary>
        public const string SESSION_NURSE_LICENSE_NO = "SESSION_NURSE_LICENSE_NO";

        public const string SESSION_ASSOCIATION_USER_ID = "SESSION_ASSOCIATION_USER_ID";

        /// <summary>
        /// Store value Apli ID to session with this key
        /// </summary>
        public const string SESSION_APLI_ID_KEY = "SESSION_APLI_ID_KEY";

        /// <summary>
        /// Store value Resume ID to session with this key
        /// </summary>
        public const string SESSION_RESUME_ID_KEY = "SESSION_RESUME_ID_KEY";

        /// <summary>
        /// The key is used for identifying content of error message.
        /// </summary>
        public const string ERROR_MESSAGE_CONTENT_KEY = "ERROR_MESSAGE_CONTENT_KEY";

        /// <summary>
        /// For screen AMPD01LS-AMPD01DT
        /// </summary>
        public const string SESSION_KEY_YEAR_SEARCH = "yearsearch";
        public const string SESSION_KEY_YEAR = "year";
        public const string SESSION_KEY_APLIDIV_SEARCH = "aplidivsearch";
        public const string SESSION_KEY_APLIDIV = "aplidiv";
        public const string SESSION_KEY_CERTDIV_SEARCH = "certdivsearch";
        public const string SESSION_KEY_CERTDIV = "certdiv";
        public const string SESSION_KEY_FIELDCD_SEARCH = "fieldcdSearch";
        public const string SESSION_KEY_FIELDCD = "fieldcd";
        public const string SESSION_KEY_FIELDNAME = "fieldname";
        public const string SESSION_KEY_DOCPROBPUBSTARTDT = "docprobpubstartdt";
        public const string SESSION_KEY_PASSPUBSTARTDT = "passpubstartdt";
        public const string SESSION_KEY_PRESS_SEARCH_BUTTON = "PressedSearch";
        // Start: TRB00797   2012/12/18        Le Phan               AMPD0101 AMPD0102 AMPD0103 合格者公開期間を追加する
        public const string SESSION_KEY_PASSNURSEPUBSTARTDT = "passnursepubstartdt";
        public const string SESSION_KEY_PASSNURSEPUBENDDT = "passnursepubendtd";
        public const string SESSION_KEY_APLIDIVCODE = "aplidivcode";
        // End  : TRB00797   2012/12/18        Le Phan               AMPD0101 AMPD0102 AMPD0103 合格者公開期間を追加する

        /// <summary>
        /// For screen AMAP01LS-AMAP01DT
        /// </summary>
        public const string SESSION_YEAR = "SESSION_YEAR";
        public const string SESSION_CERT_DIV = "SESSION_CERT_DIV";
        public const string SESSION_APLI_DIV = "SESSION_APLI_DIV";
        public const string SESSION_CERT_DIV_TEXT = "SESSION_CERT_DIV_TEXT";
        public const string SESSION_APLI_DIV_TEXT = "SESSION_APLI_DIV_TEXT";
        public const string SESSION_SELECTED_YEAR = "SESSION_SELECTED_YEAR";
        public const string SESSION_SELECTED_CERT_DIV = "SESSION_SELECTED_CERT_DIV";
        public const string SESSION_SELECTED_APLI_DIV = "SESSION_SELECTED_APLI_DIV";
        public const string SESSION_BUTTON_FLAG = "SESSION_BUTTON_FLAG";
        public const string SESSION_NO_DISPLAY = "SESSION_NO_DISPLAY";

        public const string SESSION_ACHVPOINTCD = "SESSION_ACHVPOINTCD";
        public const string SESSION_SCORE = "SESSION_SCORE";
        public const string SESSION_STYLE = "SESSION_STYLE";
        public const string SESSION_DOCUMENT = "SESSION_DOCUMENT";
        public const string SESSION_NESSESARY_ITEM = "SESSION_NESSESARY_ITEM";
        public const string SESSION_REMARK = "SESSION_REMARK";
        public const string SESSION_LARGE_TYPE = "SESSION_LARGE_TYPE";
        public const string SESSION_NORMAL_TYPE = "SESSION_NORMAL_TYPE";
        public const string SESSION_SMALL_TYPE = "SESSION_SMALL_TYPE";
        public const string SESSION_LARGE_TYPE_NAME = "SESSION_LARGE_TYPE_NAME";
        public const string SESSION_NORMAL_TYPE_NAME = "SESSION_NORMAL_TYPE_NAME";
        public const string SESSION_SMALL_TYPE_NAME = "SESSION_SMALL_TYPE_NAME";

        public const string SESSION_GRID_PAGE_SIZE = "pageSize";
        public const string SESSION_GRID_PAGE_INDEX = "pageIndex";
        public const string SESSION_GRID_PAGE_SORT_CONDITION = "sortCondition";
        public const string SESSION_GRID_PAGE_SORT_DIRECTION = "sortDirection";
        public const string SESSION_GRID_CURRENT_PAGE = "currentPage";
        public const string SESSION_GRID_VIRTUAL_COUNT = "virtualCount";

        /// <summary>
        /// For screen CTCT01
        /// </summary>
        public const string SESSION_APLI_ID = "SESSION_APLI_ID";

        //Start: TRB01414   2015/05/13        Le Chi                ACAP0001 AODC0605 履修単位自己申告書レポート出力機能追加
        public const string SESSION_FLD_CD = "SESSION_FLD_CD";
        //End: TRB01414   2015/05/13        Le Chi                ACAP0001 AODC0605 履修単位自己申告書レポート出力機能追加

        /// <summary>
        /// SESSION_NURSING_PFM_REC_ID = "NursingPfmRecID"
        /// </summary>
        public const string SESSION_NURSING_PFM_REC_ID = "NursingPfmRecID";

        /// <summary>
        /// SESSION_DOCS_ID = "docsID"
        /// </summary>
        public const string SESSION_DOCS_ID = "docsID";

        /// <summary>
        /// For screen ACAP00-ACAP01
        /// </summary>
        public const string SESSION_SEARCH_NURSENO = "SESSION_SEARCH_NURSENO";
        public const string SESSION_SEARCH_APLIID = "SESSION_SEARCH_APLIID";
        public const string SESSION_SEARCH_APLIYEAR = "SESSION_SEARCH_APLIYEAR";
        public const string SESSION_SEARCH_APLIDIV = "SESSION_SEARCH_APLIDIV";
        public const string SESSION_SEARCH_CERTDIV = "SESSION_SEARCH_CERTDIV";
        public const string SESSION_SEARCH_FLDCD = "SESSION_SEARCH_FLDCD";
        public const string SESSION_SEARCH_RESITDIV = "SESSION_SEARCH_RESITDIV";
        public const string SESSION_SEARCH_ONLINEDOCSTATUS = "SESSION_SEARCH_ONLINEDOCSTATUS";
        public const string SESSION_SEARCH_REVIEWCMTTID = "SESSION_SEARCH_REVIEWCMTTID";
        public const string SESSION_SEARCH_OLCHECKRESULT = "SESSION_SEARCH_OLCHECKRESULT";
        public const string SESSION_SEARCH_PAPERCHECKRESULT = "SESSION_SEARCH_PAPERCHECKRESULT";
        public const string SESSION_SEARCH_DOCSPASSFAIL = "SESSION_SEARCH_DOCSPASSFAIL";
        public const string SESSION_SEARCH_PROBPASSFAIL = "SESSION_SEARCH_PROBPASSFAIL";
        public const string SESSION_SEARCH_PAPERTESTATTENDANCE = "SESSION_SEARCH_PAPERTESTATTENDANCE";
        public const string SESSION_SEARCH_WRITTENEXAMSCORES = "SESSION_SEARCH_WRITTENEXAMSCORES";
        public const string SESSION_SEARCH_WRITTENEXAMSCORESTYPESEARCH = "SESSION_SEARCH_WRITTENEXAMSCORESTYPESEARCH";
        public const string SESSION_SEARCH_EXAMPLACECD = "SESSION_SEARCH_EXAMPLACECD";
        public const string SESSION_SEARCH_EXAMNO = "SESSION_SEARCH_EXAMNO";
        public const string SESSION_SEARCH_PAYACNTNO = "SESSION_SEARCH_PAYACNTNO";
        public const string SESSION_SEARCH_PAYSLPPERSONNAME = "SESSION_SEARCH_PAYSLPPERSONNAME";
        public const string SESSION_SEARCH_CNCLSTAT = "SESSION_SEARCH_CNCLSTAT";
        public const string SESSION_SEARCH_EXAMPAYMENTSTATUS = "SESSION_SEARCH_EXAMPAYMENTSTATUS";
        public const string SESSION_SEARCH_ACCREDITATIONPAYMENTSTATUS = "SESSION_SEARCH_ACCREDITATIONPAYMENTSTATUS";
        public const string SESSION_SEARCH_TYPE = "SESSION_SEARCH_TYPE";

        //<Start>1.7         TRB01168    2014/06/19       Bao Bao               Common CNS看護実績報告書の仕様変更
        public const string SESSION_SEARCH_NURSINGPFMRECEVALSEARCH = "SESSION_SEARCH_NURSINGPFMRECEVALSEARCH";
        //<End>1.7         TRB01168    2014/06/19       Bao Bao               Common CNS看護実績報告書の仕様変更

        //Start 1.6 TRB01144   2014/06/02        Bao Bao               Common 実践報告書について段階評価をおこない、結果を開示する
        ////Start 1.4         TRB00800   2012/12/24        Pham Tuyen            ACAP0001 ACAP0205 AODC0302 実践報告書優秀者の設定・抽出
        //public const string SESSION_SEARCH_PRCTCRECEXCELLENCE = "SESSION_SEARCH_PRCTCRECEXCELLENCE";
        ////End   1.4         TRB00800   2012/12/24        Pham Tuyen            ACAP0001 ACAP0205 AODC0302 実践報告書優秀者の設定・抽出
        public const string SESSION_SEARCH_PRCTCRECEVAL = "SESSION_SEARCH_PRCTCRECEVAL";

        public const string SESSION_SEARCH_ACHVPOINTEVAL = "SESSION_SEARCH_ACHVPOINTEVAL";
        //End 1.6 TRB01144   2014/06/02        Bao Bao               Common 実践報告書について段階評価をおこない、結果を開示する

        //Start 1.5         TRB00892   2013/01/30        Bao Bao               Add search condition [看護実績報告書点数]
        public const string SESSION_SEARCH_NURSINGPFMRECSCORE = "SESSION_SEARCH_NURSINGPFMRECSCORE";
        //End 1.5         TRB00892   2013/01/30        Bao Bao               Add search condition [看護実績報告書点数]

        //Start 2.5         TRB01161        2014/07/16          Bao Bao          Common CNS履修単位自己申告書のオンライン化（作成中）
        public const string SESSION_SEARCH_TAKECRDTREPPASSFAIL = "SESSION_SEARCH_TAKECRDTREPPASSFAIL";
        //End 2.5         TRB01161        2014/07/16          Bao Bao          Common CNS履修単位自己申告書のオンライン化（作成中）

        /// <summary>
        /// For screen AMLS03LS - AMLS03DT
        /// </summary>
        public const string SESSION_MAIL_DEF_ID = "SESSION_MAIL_DEF_ID";
        public const string SESSION_CHECK_MAIL_DEF_ID = "SESSION_CHECK_MAIL_DEF_ID";

        /// <summary>
        /// For screen AMST05LS - AMST05DT
        /// </summary>
        public const string SESSION_INST_ID = "SESSION_INST_ID ";

        public const string SESSION_WORK_PLACE_INST_PREFECTURE_CD = "SESSION_WORK_PLACE_INST_PREFECTURE_CD";
        public const string SESSION_WORK_PLACE_CORPO_NAME = "SESSION_WORK_PLACE_CORPO_NAME";
        public const string SESSION_WORK_PLACE_INST_NAME = "SESSION_WORK_PLACE_INST_NAME";
        public const string SESSION_WORK_PLACE_ZIP = "SESSION_WORK_PLACE_ZIP";
        public const string SESSION_WORK_PLACE_CITY = "SESSION_WORK_PLACE_CITY";
        public const string SESSION_WORK_PLACE_HOUSE_NO = "SESSION_WORK_PLACE_HOUSE_NO";
        public const string SESSION_WORK_PLACE_BUILDING_NAME = "SESSION_WORK_PLACE_BUILDING_NAME";

        public const string SESSION_FULL_NAME_KANJI = "SESSION_FULL_NAME_KANJI";

        
        #endregion

        #region Authentication group

        /// <summary>
        /// Flag of login: disable
        /// </summary>
        public const string LOGIN_DISABLE_FLAG_ON = "1";

        /// <summary>
        /// Flag of login: enabled.
        /// </summary>
        public const string LOGIN_DISABLE_FLAG_OFF = "0";


        /// <summary>
        /// The key is used for identifying login or not in session.
        /// </summary>
        public const string AUTHENTICATION_LOGIN_KEY = "AUTHENTICATION_KEY";

        //Start:    2016/06/10    Lam Thao  Add constant login for Make Question System
        /// <summary>
        /// The key is used for identifying login or not in session.(Make Question System)
        /// </summary>
        public const string AUTHENTICATION_LOGIN_MAKE_QUEST_KEY = "AUTHENTICATION_MAKE_QUEST_KEY";
        //End:    2016/06/10    Lam Thao  Add constant login for Make Question System

        /// <summary>
        /// The key is used for store authenticating information.
        /// </summary>
        public const string AUTHENTICATION_INFO_KEY = "AUTHENTICATION_INFO_KEY";

        /// <summary>
        /// Login flag
        /// </summary>
        public const string AUTHENTICATION_FLAG_LOGIN = "1";

        //Start:    2016/06/10    Lam Thao  Add constant login for Make Question System
        /// <summary>
        /// Login flag(Make Question System)
        /// </summary>
        public const string AUTHENTICATION_FLAG_LOGIN_MAKE_QUEST = "1";
        //End:    2016/06/10    Lam Thao  Add constant login for Make Question System

        /// <summary>
        /// Refresh key
        /// </summary>
        public const string FLAG_REFRESH_KEY = "FLAG_REFRESH_KEY";

        #endregion

        #region Message group

        /// <summary>
        /// Path of file that store main messages of application.
        /// </summary>
        public const string XML_MESSAGE_FILE_PATH = "~/App_Data/JnaMessage.xml";

        #endregion

        #region Webconfig value group        

        /// <summary>
        /// the year when system start
        /// </summary>
        public const string WEBCONFIG_SYSTEM_START_YEAR = "SystemStartYear";             

        /// <summary>
        /// Page Title
        /// </summary>
        public const string WEBCONFIG_PAGETITLE = "PageTitle";

        /// <summary>
        /// Max Search result
        /// </summary>
        public const string WEBCONFIG_MAX_SEARCH_RESULT = "MaxSearchResult";

        /// <summary>
        /// Popup title
        /// </summary>
        public const string WEBCONFIG_POPUPTITLE = "PopupTitle";

        /// <summary>
        /// The size of one page in gridview in case: paging
        /// </summary>
        public const string WEBCONFIG_PAGING_SIZE = "PagingSize";

        /// <summary>
        /// Log path of db error
        /// </summary>
        public const string WEBCONFIG_DBERRORLOGPATH = "DbErrorLogPath";

        /// <summary>
        /// Maintain mode
        /// </summary>
        public const string WEBCONFIG_MAINTENANCEMODE = "UseMaintenanceMode";

        /// <summary>
        /// Agent Login Mode
        /// </summary>
        public const string WEBCONFIG_LOGINMODE = "AgentLoginMode";

        /// <summary>
        /// Url display information of CNS
        /// </summary>
        public const string WEBCONFIG_URLFORCNS = "UrlForInfoCNS";

        /// <summary>
        /// Url display information of CN
        /// </summary>
        public const string WEBCONFIG_URLTESTCENTER_CN = "UrlDetailTestCenterCN";

        /// <summary>
        /// Url display information of CNA
        /// </summary>
        public const string WEBCONFIG_URLTESTCENTER_CNA = "UrlDetailTestCenterCNA";

        /// <summary>
        /// Url display information of CNS
        /// </summary>
        public const string WEBCONFIG_URLTESTCENTER_CNS = "UrlDetailTestCenterCNS";

        /// <summary>
        /// Url Agent Login
        /// </summary>
        public const string WEBCONFIG_URL_AGENT_LOGIN = "UrlAgentLogin";

        /// <summary>
        /// Url For Incident Report
        /// </summary>
        public const string WEBCONFIG_URL_INCIDENT_REPORT = "UrlIncidentReport";
       
        /// <summary>
        /// Images Extensions
        /// </summary>
        public const string WEBCONFIG_IMG_EXTENSIONS = "ImgExtensions";


        /// <summary>
        /// Upload Image Path
        /// </summary>

        public const string WEBCONFIG_UPLOAD_LICENSE_IMG_PATH = "UploadLicenseImgPath";

        public const string WEBCONFIG_UPLOAD_PASSCERT_IMG_PATH = "UploadPassCertImgPath";

        /// <summary>
        /// The starting year of Birthday
        /// </summary>
        public const string WEBCONFIG_START_YEAR_BIRTHDAY = "StartYearBirthday";

        /// <summary>
        /// The starting year of Update Cert
        /// </summary>
        public const string WEBCONFIG_START_YEAR_CERT = "StartYearCert";

        /// <summary>
        /// The starting year of financial
        /// </summary>
        public const string WEBCONFIG_START_YEAR_FINANCIAL = "StartYearFinancial";

        /// <summary>
        /// The ending year of financial
        /// </summary>
        public const string WEBCONFIG_END_YEAR_FINANCIAL = "EndYearFinancial";

        /// <summary>
        /// Managed year difference from current year
        /// </summary>
        public const string WEBCONFIG_MANAGED_YEAR_DIFF = "ManagedYearDiff";

        /// <summary>
        /// The starting year of Admission
        /// </summary>
        public const string WEBCONFIG_END_YEAR_ADMISSION = "StartYearAdmission";

        /// <summary>
        /// Temp directory to store temp uploading image
        /// </summary>

        public const string WEBCONFIG_UPLOAD_LICENSE_IMG_TEMP_PATH = "UploadLicenseImgTempPath";

        public const string WEBCONFIG_UPLOAD_PASSCERT_IMG_TEMP_PATH = "UploadPassCertImgTempPath";

        public const string WEBCONFIG_UPLOAD_COMMON_DOC_TEMP_PATH = "QuestCommonDoc_UploadTemp";

        public const string WEBCONFIG_UPLOAD_COMMON_DOC_PATH = "QuestCommonDoc_Upload";

        /// <summary>
        /// Max number of update
        /// </summary>
        public const string WEBCONFIG_MAX_NUMBER_UPDATE = "MaxNumberUpdates";

		 /// <summary>
        /// Upload Image Physical Path
        /// </summary>
        public const string WEBCONFIG_UPLOAD_IMG_PHYSICAL_PATH = "PhysicalUploadPath";

        /// <summary>
        /// Image upload Virtual directory path
        /// </summary>
        public const string WEBCONFIG_UPLOAD_ROOT_URL = "UrlImageUpload";

        /// <summary>
        /// Image width
        /// </summary>
        public const string WEBCONFIG_WIDTH_IMG = "WidthImg";

        /// <summary>
        /// Image height
        /// </summary>
        public const string WEBCONFIG_HEIGHT_IMG = "HeightImg";

        /// <summary>
        /// Number image of row
        /// </summary>
        public const string WEBCONFIG_NUM_IMG_OF_ROW = "NumImgOfRow";

        /// <summary>
        /// Number image of page
        /// </summary>
        public const string WEBCONFIG_NUM_IMG_OF_PAGE = "NumImgOfPage";

        //Start:TRB00403   2012/04/05        Le Chi                ■  ACLI0101 画像のサイズ、画像の表示枚数などを制御する設定値が、修了証画像管理と同様の設定値を参照している	

        /// <summary>
        /// Image width NurseLicense
        /// </summary>
        public const string WEBCONFIG_WIDTH_IMG_NURSE_LICENSE = "WidthImgNurseLicense";

        /// <summary>
        /// Image height NurseLicense
        /// </summary>
        public const string WEBCONFIG_HEIGHT_IMG_NURSE_LICENSE = "HeightImgNurseLicense";

        /// <summary>
        /// Number image of row NurseLicense
        /// </summary>
        public const string WEBCONFIG_NUM_IMG_OF_ROW_NURSE_LICENSE = "NumImgOfRowNurseLicense";

        /// <summary>
        /// Number image of page NurseLicense
        /// </summary>
        public const string WEBCONFIG_NUM_IMG_OF_PAGE_NURSE_LICENSE = "NumImgOfPageNurseLicense";

        //End:TRB00403   2012/04/05        Le Chi                ■  ACLI0101 画像のサイズ、画像の表示枚数などを制御する設定値が、修了証画像管理と同様の設定値を参照している	

        /// <summary>
        /// Example image name
        /// </summary>
        public const string WEBCONFIG_EXAMPLE_IMAGE_NURSE = "ExampleImageNurse";

        /// <summary>
        /// Example image name
        /// </summary>
        public const string WEBCONFIG_EXAMPLE_IMAGE_DIPLOMA = "ExampleImageDiploma";

        /// <summary>
        /// Example image name
        /// </summary>
        public const string WEBCONFIG_EXAMPLE_IMAGE_PLACE = "ExampleImagePlace";

        /// <summary>
        /// Nurse or Association
        /// </summary>
        public const string WEBCONFIG_SITE_ID = "SiteID";

        /// <summary>
        /// Opening Minutes
        /// </summary>
        public const string WEBCONFIG_OPENING_MIN = "OpeningMinutes";

        /// <summary>
        /// Focus Minutes
        /// </summary>
        public const string WEBCONFIG_FOCUS_MIN = "FocusMinutes";
		
		/// <summary>
        /// Physical place map path
        /// </summary>
        public const string WEBCONFIG_PHYSICAL_PLACE_MAP_PATH = "PhysicalPlaceMapPath";
		
        /// <summary>
        /// Place map path
        /// </summary>
        public const string WEBCONFIG_PLACE_MAP_PATH = "PlaceMapPath";

        /// <summary>
        /// Place map height image
        /// </summary>
        public const string WEBCONFIG_PLACE_MAP_HEIGHT_IMG = "PlaceMapHeightImg";

        /// <summary>
        /// Privacy for Nurse
        /// </summary>
        public const string WEBCONFIG_PRIVACY_NURSE = "PrivacyNurse";

        /// <summary>
        /// Url for Experience and Research of CN
        /// </summary>
        public const string WEBCONFIG_URL_LIST_EXPERIENCE_RESEARCH_CN = "UrlListExperienceAndResearchCN";

        /// <summary>
        /// Url for Experience and Research of CNA
        /// </summary>
        public const string WEBCONFIG_URL_LIST_EXPERIENCE_RESEARCH_CNA = "UrlListExperienceAndResearchCNA";

        /// <summary>
        /// Url for Experience and Research of CNS
        /// </summary>
        public const string WEBCONFIG_URL_LIST_EXPERIENCE_RESEARCH_CNS = "UrlListExperienceAndResearchCNS";

        /// <summary>
        /// Month day start of year
        /// </summary>
        public const string WEBCONFIG_MONTH_DAY_START_OF_YEAR = "MonthDayStartOfYear";

        /// <summary>
        /// Month day end of year
        /// </summary>
        public const string WEBCONFIG_MONTH_DAY_END_OF_YEAR = "MonthDayEndOfYear";

        /// <summary>
        /// Jna Information Mail From
        /// </summary>
        public const string WEBCONFIG_JNA_INFORMATION_MAIL_FROM = "JnaInformationMailFrom";

        //Start:TRB00771   2012/12/07        Le Chi                ALGI0101 協会と委員でログインサイトを分ける

        /// <summary>
        /// Association Cmtt Div
        /// </summary>
        public const string WEBCONFIG_ASSOCIATION_CMTT_DIV = "AssociationCmttDiv";

        //End:TRB00771   2012/12/07        Le Chi                ALGI0101 協会と委員でログインサイトを分ける

        /// <summary>
        /// UploadPlaceMapImgPath
        /// </summary>
        public const string WEBCONFIG_UPLOAD_PLACE_MAP_IMG_PATH = "UploadPlaceMapImgPath";

        /// <summary>
        /// UploadPlaceMapImgTempPath
        /// </summary>
        public const string WEBCONFIG_UPLOAD_PLACE_MAP_IMG_TEMP_PATH = "UploadPlaceMapImgTempPath";

        /// <summary>
        /// AMST16DT_MaxRow_項目追加
        /// </summary>
        public const string WEBCONFIG_AMST16DT_MaxRow_項目追加 = "AMST16DT_MaxRow_項目追加";

        /// <summary>
        /// AMST17DT_MaxRow_要素追加
        /// </summary>
        public const string WEBCONFIG_AMST17DT_MaxRow_要素追加 = "AMST17DT_MaxRow_要素追加";
        
        /// <summary>
        /// AMST16DT_MaxRow_項目追加
        /// </summary>
        public const string WEBCONFIG_QUESTION_URL = "QuestionUrl";

        /// <summary>
        /// RadioButtonSymbolInCharAAQS0102
        /// </summary>
        public const string WEBCONFIG_RADIOBUTTON_SYMBOL_IN_CHAR_AAQS0102 = "RadioButtonSymbolInCharAAQS0102";

        /// <summary>
        /// RadioButtonSpaceBetweenInCharAAQS0102
        /// </summary>
        public const string WEBCONFIG_RADIOBUTTON_SPACE_BETWEEN_IN_CHAR_AAQS0102 = "RadioButtonSpaceBetweenInCharAAQS0102";

        /// <summary>
        /// RadioButtonTotalCharsAAQS0102
        /// </summary>
        public const string WEBCONFIG_RADIOBUTTON_TOTAL_CHARS_AAQS0102 = "RadioButtonTotalCharsAAQS0102";

        /// <summary>
        /// CheckboxSymbolInCharAAQS0102
        /// </summary>
        public const string WEBCONFIG_CHECKBOX_SYMBOL_IN_CHAR_AAQS0102 = "CheckboxSymbolInCharAAQS0102";

        /// <summary>
        /// CheckboxSpaceBetweenInCharAAQS0102
        /// </summary>
        public const string WEBCONFIG_CHECKBOX_SPACE_BETWEEN_IN_CHAR_AAQS0102 = "CheckboxSpaceBetweenInCharAAQS0102";

        /// <summary>
        /// CheckboxTotalCharsAAQS0102
        /// </summary>
        public const string WEBCONFIG_CHECKBOX_TOTAL_CHARS_AAQS0102 = "CheckboxTotalCharsAAQS0102";

        /// <summary>
        /// TextAreaRows
        /// </summary>
        public const string WEBCONFIG_TEXTAREA_ROWS = "TextAreaRows";

        /// <summary>
        /// TextAreaCols
        /// </summary>
        public const string WEBCONFIG_TEXTAREA_COLS = "TextAreaCols";

        /// <summary>
        /// JnaMailCopySendTo
        /// </summary>
        public const string WEBCONFIG_JNA_MAIL_COPY_SENDTO = "JnaMailCopySendTo";

        /// <summary>
        /// JnaMailCopySendFrom
        /// </summary>
        public const string WEBCONFIG_JNA_MAIL_COPY_SENDFROM = "JnaMailCopySendFrom";

        /// <summary>
        /// ExtensionPastExamUpload
        /// </summary>
        public const string WEBCONFIG_EXTENSION_PAST_EXAM_UPLOAD = "ExtensionPastExamUpload";

        /// <summary>
        /// MaxSizeFileUpload
        /// </summary>
        public const string WEBCONFIG_MAX_SIZE_FILEUPLOAD = "MaxSizeFileUpload";

        /// <summary>
        /// PastExamUpload
        /// </summary>
        public const string WEBCONFIG_PAST_EXAM_BASE_PATH = "pastexamfilebasepath";

        /// <summary>
        /// PlagCheckStartYear
        /// </summary>
        public const string WEBCONFIG_PLAG_CHECK_START_YEAR = "PlagCheckStartYear";

        //Start: TRB01315   2015/01/23        Le Chi                AMST2101,AMST2102 筆記試験開始日時マスタの管理画面新規追加
        /// <summary>
        /// AMST21DT_OpenHours
        /// </summary>
        public const string WEBCONFIG_AMST21DT_OPEN_HOURS = "AMST21DT_OpenHours";

        /// <summary>
        /// AMST21DT_SetTime
        /// </summary>
        public const string WEBCONFIG_AMST21DT_SET_TIME = "AMST21DT_SetTime";
        //End: TRB01315   2015/01/23        Le Chi                AMST2101,AMST2102 筆記試験開始日時マスタの管理画面新規追加

        //Start: TRB01414    2015/05/13          Le Chi                  ACAP0001 AODC0605 履修単位自己申告書レポート出力機能追加

        /// <summary>
        /// ExcelTemplate
        /// </summary>
        public const string WEBCONFIG_FOLDER_PATH_EXCEL_TEMPLATE = "ExcelTemplate";

        /// <summary>
        /// ExcelExportTemp
        /// </summary>
        public const string WEBCONFIG_FOLDER_PATH_EXCEL_EXPORT_TEMP = "ExcelExportTemp";

        //End: TRB01414    2015/05/13          Le Chi                  ACAP0001 AODC0605 履修単位自己申告書レポート出力機能追加

        //Start:     2016/06/10      Huynh Giang         Database LINK
        /// <summary>
        /// DBRefExam
        /// </summary>
        public const string WEBCONFIG_DB_REF_EXAM = "DBRefExam";
        /// <summary>
        /// DBRefEdu
        /// </summary>
        public const string WEBCONFIG_DB_REF_EDU = "DBRefEdu";
        //End:     2016/06/10      Huynh Giang         Database LINK

        /// <summary>
        /// The start year of creating exam
        /// </summary>
        public const string WEBCONFIG_EXAM_CREATE_START_YEAR = "ExamCreateStartYear";

        /// <summary>
        /// The temp folder path for exporting exam file
        /// </summary>
        public const string WEBCONFIG_MAKE_EXAM_EXPORT_TEMP = "MakeExamExportTemp";

        /// <summary>
        /// The temp folder path for exporting selected exam file
        /// </summary>
        public const string WEBCONFIG_SELECTED_EXAM_EXPORT_TEMP = "SelectedExamExportTemp";

        /// <summary>
        /// Get year implement test
        /// </summary>
        public const string WEBCONFIG_AQAS0101_ITM02_FROM = "AQAS0101_ITM02_From";

        /// <summary>
        /// Get year implement test of AQSM0201
        /// </summary>
        public const string WEBCONFIG_AQSM0201_ITM02_FROM = "AQSM0201_ITM02_From";
        
        /// <summary>
        /// The temp folder path for upload Img File
        /// </summary>
        public const string WEBCONFIG_QUESTIMAGE_UPLOADTEMP = "QuestImage_UploadTemp";

        /// <summary>
        /// The temp folder path for upload Img File
        /// </summary>
        public const string WEBCONFIG_QUESTIMAGE_UPLOAD = "QuestImage_Upload";
        /// <summary>
        /// The BaseURL of upload Img File
        /// </summary>
        public const string WEBCONFIG_QUESTIMAGE_BASEURL = "QuestImage_BaseURL";
        /// <summary>
        /// The BaseURL of upload Img File
        /// </summary>
        public const string WEBCONFIG_QUESTIMAGETEMP_BASEURL = "QuestImageTemp_BaseURL";
        /// <summary>
        /// The temp folder path for upload Img File
        /// </summary>
        public const string WEBCONFIG_AQSI0101_DISPLAYROWS = "AQSI0101_DisplayRows";
        /// <summary>
        /// The BaseURL of upload Img File
        /// </summary>
        public const string WEBCONFIG_AQSI0101_DISPLAYCOLUMNS = "AQSI0101_DisplayColumns";

        /// <summary>
        /// Image width Question
        /// </summary>
        public const string WEBCONFIG_WIDTH_IMG_QUEST = "WidthImgQuest";

        /// <summary>
        /// Image height Question
        /// </summary>
        public const string WEBCONFIG_HEIGHT_IMG_QUEST = "HeightImgQuest";

        //Start: TRB01520     2016/06/23      Do Khuong   申請者開示コンテンツ管理※ファイルアップロード
        /// <summary>
        /// FileAnnounce
        /// </summary>
        public const string WEBCONFIG_FOLDER_PATH_ANNOUNCE = "FileAnnounce";

        /// <summary>
        /// FileAnnounce
        /// </summary>
        public const string WEBCONFIG_FILE_UPLOAD_ANNOUNCE_EXTENSION = "FileAnnounceExtensions";
        //End: TRB01520     2016/06/23      Do Khuong   申請者開示コンテンツ管理※ファイルアップロード

        /// <summary>
        /// ExamImage_Favorite
        /// </summary>
        public const string WEBCONFIG_EXAMIMAGE_FAVORITE = "ExamImage_Favorite";
        /// <summary>
        /// ExamImage_NoFavorite
        /// </summary>
        public const string WEBCONFIG_EXAMIMAGE_NO_FAVORITE = "ExamImage_NoFavorite";
        /// <summary>
        /// ダウンロード元のファイルパス
        /// </summary>
        public const string WEBCONFIG_QUESTCMNDOC_BASEURL = "QuestCmnDoc_BaseURL";
        
        /// <summary>
        /// ExamAttachment_Upload
        /// </summary>
        public const string WEBCONFIG_EXAM_ATTACHMENT_UPLOAD = "ExamAttachment_Upload";
        
        /// <summary>
        /// ExamAttachment_UploadTemp
        /// </summary>
        public const string WEBCONFIG_EXAM_ATTACHMENT_UPLOAD_TEMP = "ExamAttachment_UploadTemp";
        
        /// <summary>
        /// ExamAttachment_BaseURL
        /// </summary>
        public const string WEBCONFIG_EXAM_ATTACHMENT_BASEURL = "ExamAttachment_BaseURL";
        
		/// <summary>
        /// 過去問登録開始年度
        /// </summary>
        public const string WEBCONFIG_PAST_EXAM_REGISTER_START_YEAR = "PastExamRegisterStartYear";

        //Start:TRB01534     2016/07/25      Do Khuong         Common(MasterPage) システム名表記の変更
        /// <summary>
        /// ExamScreenList
        /// </summary>
        public const string WEBCONFIG_EXAM_SCREEN_LIST = "ExamScreenList";
        //End:TRB01534     2016/07/25      Do Khuong         Common(MasterPage) システム名表記の変更
        /// <summary>
        /// AQSM0105 QuestFileByYearTmp
        /// </summary>
        public const string WEBCONFIG_QUEST_FILE_BY_YEAR_TMP = "QuestFileByYearTmp";
        /// <summary>
        /// AQSM0105 QuestFileByYear
        /// </summary>
        public const string WEBCONFIG_QUEST_FILE_BY_YEAR = "QuestFileByYear";
        /// <summary>
        /// AQSM0106, AQSM0306
        /// ExamDocxTemplate
        /// </summary>
        public const string WEBCONFIG_EXAM_DOCX_TEMPLATE = "ExamDocxTemplate";
        #endregion

        #region Maintain mode values

        /// <summary>
        /// Web.configにメンテナンスモード利用有無: [1] 利用する
        /// </summary>
        public const int MODE_USEMAINTENANCE_USE = 1;
        /// <summary>
        /// Web.configにメンテナンスモード利用有無: [0] 利用しない
        /// </summary>
        public const int MODE_USEMAINTENANCE_NOT_USE = 0;

        /// <summary>
        /// DBのメンテナンステーブルのメンテナンスモード: [0] 公開モード
        /// </summary>
        public const int MODE_DBMAINTENANCE_PUBLIC = 0;
        /// <summary>
        /// DBのメンテナンステーブルのメンテナンスモード: [1] メンテナンスモード
        /// </summary>
        public const int MODE_DBMAINTENANCE_MAINTENANCE = 1;

        #endregion

        #region Log group

        /// <summary>
        /// Start logic
        /// </summary>
        public const string LOG_LOGIC_START = "Logic start.";
        /// <summary>
        /// End logic
        /// </summary>
        public const string LOG_LOGIC_END = "Logic end.";

        /// <summary>
        /// Start event
        /// </summary>
        public const string LOG_EVENT_START = "Event start.";

        /// <summary>
        /// End event
        /// </summary>
        public const string LOG_EVENT_END = "Event end.";

		/// <summary>
        /// Start process
        /// </summary>
        public const string LOG_PROCESS_START = "Process start.";

        /// <summary>
        /// End process
        /// </summary>
        public const string LOG_PROCESS_END = "Process end.";
		
        #endregion

        /// <summary>
        /// Blank value using for gridview
        /// </summary>
        public const string SPECIAL_DISPLAY__TABLE_BLANK = "-";

        /// <summary>
        /// Blank value using for button of gridview
        /// </summary>
        public const string SPECIAL_DISPLAY__BUTTON_BLANK = "";

        public const string SCRIPT_SHOW_ALERT_KEY = "ShowAlertOnClient";

        public const string SCRIPT_SHOW_CONFIRM_KEY = "ShowConfirmOnClient";

        /// <summary>
        /// Default value of dropdownlist in searching screen
        /// </summary>
        public const string DROPDOWNLIST_SEARCH_DEFAULT_VALUE = "-1";
        /// <summary>
        /// Default text of dropdownlist in searching screen
        /// </summary>
        public const string DROPDOWNLIST_SEARCH_DEFAULT_TEXT = "";

        /// <summary>
        /// DEFAULT_PASSWORD_VALUE
        /// </summary>
        public const string DEFAULT_PASSWORD_VALUE = "******";

        #region Mail

        /// <summary>
        /// Key to get smtp host
        /// </summary>
        public const string MAIL_SMTP_HOST = "SmtpHost";
        /// <summary>
        /// Key to get smtp port
        /// </summary>
        public const string MAIL_SMTP_PORT = "SmtpPort";
        /// <summary>
        /// Key to get smtp user.
        /// </summary>
        public const string MAIL_SMTP_USER_NAME = "SmtpUserName";
        /// <summary>
        /// Key to get smtp password
        /// </summary>
        public const string MAIL_SMTP_PASSWORD = "SmtpPassword";
        /// <summary>
        /// Key to get smtp send address
        /// </summary>
        public const string MAIL_SMTP_SEND_ADDRESS = "SendAddress";

        /// <summary>
        /// Key to get address [TO] for error mail
        /// </summary>
        public const string MAIL_ERRORMAILTO = "ErrorMailTo";

        /// <summary>
        /// Key to get address [FROM] for error mail
        /// </summary>
        public const string MAIL_ERRORMAILFROM = "ErrorMailFrom";

        /// <summary>
        /// Key to get subject for error mail.
        /// </summary>
        public const string MAIL_ERRORMAILSUBJECT = "ErrorMailSubject";

        /// <summary>
        /// Key to get body template for error mail
        /// </summary>
        public const string MAIL_ERRORMAILBODYTEMPLATE = "ErrorMailBodyTemplate";

        /// <summary>
        /// Char for CheckBox Checked disp
        /// </summary>
        public const string CHECKBOX_CHECKED_DISP = "○";


        #endregion

        #region Item name
        /// <summary>
        /// Item name for quest seq mastor
        /// </summary>
        public const string SEQ_QUESTIDENTIFYID_ITEMNAME = "questidentifyID";
        #endregion
    }
}
