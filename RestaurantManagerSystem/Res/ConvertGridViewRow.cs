﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GoldManager.Res
{
    public class ConvertGridViewRow
    {
        private DataGridViewRow row;

        public ConvertGridViewRow(DataGridViewRow r)
        {
            this.row = r;
        }

        public bool getBoolean(string column)
        {
            if (isNull(column))
            {
                return false;
            }
            else
            {
                return Convert.ToBoolean(row.Cells[column].Value);
            }
        }

        public string getString(string column)
        {
            if (isNull(column))
            {
                return string.Empty;
            } else {
                return Convert.ToString(row.Cells[column].Value);
            }
        }

        public int getInt(string column)
        {
            if (isNull(column))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(row.Cells[column].Value);
            }
        }


        public long getLong(string column)
        {
            if (isNull(column))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(row.Cells[column].Value);
            }
        }


        public double getDouble(string column)
        {
            if (isNull(column))
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(row.Cells[column].Value);
            }
        }


        public decimal getDecimal(string column)
        {
            if (isNull(column))
            {
                return 0;
            }
            else
            {
                return Convert.ToDecimal(row.Cells[column].Value);
            }
        }

        private bool isNull(string column)
        {
            return row == null || row.Cells[column].Value == DBNull.Value || row.Cells[column].Value == null;
        }
    }
}
