﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Drawing.Printing;
using System.Data;
using GoldManager.Res.Reports;

namespace GoldManager.PrinterStremming
{
    class PrinterStreaming
    {
        private List<Socket> listSocketClient { get; set; }
        private Socket socketServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private byte[] _buffer = new byte[1024];
        
        public void start()
        {
            listSocketClient = new List<Socket>();

            socketServer.Bind(new IPEndPoint(IPAddress.Any, 100));
            socketServer.Listen(1);
            socketServer.BeginAccept(new AsyncCallback(AppceptCallback), null);
        }

        private void AppceptCallback(IAsyncResult ar)
        {
            Socket workSocket = socketServer.EndAccept(ar);

            workSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), workSocket);

            listSocketClient.Add(workSocket);

            //_serverSocket.BeginAccept(new AsyncCallback(AppceptCallback), null);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            Socket workSocket = (Socket)ar.AsyncState;

            if (workSocket.Connected)
            {
                int received;
                try
                {
                    received = workSocket.EndReceive(ar);
                }
                catch (Exception)
                {
                    return;
                }

                if (received > 0)
                {
                    byte[] dataBuf = new byte[received];
                    Array.Copy(_buffer, dataBuf, received);

                    string input = Encoding.ASCII.GetString(dataBuf);

                    string reponse = string.Empty;

                    for (int i = 0; i < listSocketClient.Count; i++)
                    {
                        string endPoint = listSocketClient[i].RemoteEndPoint.ToString();
                        if (endPoint.Equals(workSocket.RemoteEndPoint.ToString()))
                        {
                            try{
                                PrinterSettings printSetting = new PrinterSettings();
                                printSetting.PrinterName = "Foxit Reader PDF Printer";

                                PaperSize pagerSize = new PaperSize("Custom", 850, 850);

                                PageSettings pageSetting = new PageSettings(printSetting);
                                pageSetting.PaperSize = pagerSize;
                                pageSetting.Landscape = false;

                                //DataTable invoice = HoaDonDAO.getInvoice(5);
                                DataTable invoice = new DataTable();
                                invoice.Columns.Add("SoHD", typeof(string));
                                invoice.Columns.Add("ThoiGianLap", typeof(string));


                                RptInvoice rptInvoice = new RptInvoice();
                                rptInvoice.SetDataSource(invoice);
                                rptInvoice.PrintToPrinter(printSetting, pageSetting, false);
                            } catch(Exception ex){

                            } finally{

                            }
                            
                        }
                    }

                    if (input == "close")
                    {
                        return;
                    }

                    Sendata(workSocket, reponse);
                }
                else
                {
                    for (int i = 0; i < listSocketClient.Count; i++)
                    {
                        string endPoint = listSocketClient[i].RemoteEndPoint.ToString();
                        if (endPoint.Equals(workSocket.RemoteEndPoint.ToString()))
                        {
                            listSocketClient.RemoveAt(i);
                        }
                    }
                    //workSocket.Close();
                }
            }
            workSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), workSocket);
        }

        private void Sendata(Socket socket, string noidung)
        {
            byte[] data = Encoding.ASCII.GetBytes(noidung);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
            socketServer.BeginAccept(new AsyncCallback(AppceptCallback), null);
        }

        private void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }
    }
}
