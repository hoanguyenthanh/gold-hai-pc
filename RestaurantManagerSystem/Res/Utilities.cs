﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Windows.Forms;

namespace GoldManager.Res
{
    class Utilities
    {

        public static string DbNullToEmpty(object obj){
            if (obj == DBNull.Value)
            {
                return string.Empty;
            }
            return (string)obj;
        }

        public static string GetHost()
        {
            return ConfigurationManager.AppSettings["Host"];
        }

        /// <summary>
        /// Validation only input text number
        /// </summary>
        /// <returns></returns>
        public static bool isOnlyNumber(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8)
            {
                return true;
            }

            
            string value = string.Empty;
            if (sender is TextBox)
            {
                value = (sender as TextBox).Text;
            } else if (sender is Label)
            {
                value = (sender as Label).Text;
            }
 
            if (string.IsNullOrEmpty(value) && e.KeyChar == 46){
                return false;
            }

            if (!string.IsNullOrEmpty(value) && value.Contains('.') && e.KeyChar == 46)
            {
                return false;
            }

            if (e.KeyChar == 46)
            {
                if (string.IsNullOrEmpty(value)) return false;
                if (!string.IsNullOrEmpty(value) && value.Contains('.')) return false;
            }

            return (char.IsDigit(e.KeyChar) || e.KeyChar == '.');
        }
    }
}
