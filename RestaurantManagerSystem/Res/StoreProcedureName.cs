﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Res
{
    public class StoreProcedureName
    {
        public const string WS_ORDER_Get = "WS_ORDER_Get";
        public const string WS_ORDER_Get_List = "WS_ORDER_Get_List";
        public const string WS_ORDER_DT_Update = "WS_ORDER_DT_Update";
        public const string WS_ORDER_DT_Delete = "WS_ORDER_DT_Delete";

        public const string WS_PRODUCT_IN_Insert = "WS_PRODUCT_IN_Insert";
        public const string WS_PRODUCT_IN_Update = "WS_PRODUCT_IN_Update";
        public const string WS_PRODUCT_IN_Delete = "WS_PRODUCT_IN_Delete";
    }
}
