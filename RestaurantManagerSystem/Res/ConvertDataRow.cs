﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace GoldManager.Res
{
    public class ConvertDataRow
    {
        private DataRow row;

        public ConvertDataRow (DataRow r)
        {
            this.row = r;
        }

        public int getInt(string column)
        {
            if (isNull(column))
                return 0;
            else
                return Convert.ToInt32(row[column]);
        }

        public string getString(string column)
        {
            if (isNull(column))
                return string.Empty;
            else
                return Convert.ToString(row[column]);
        }

        public long getLong(string column)
        {
            if (isNull(column))
                return 0;
            else
                return Convert.ToInt64(row[column]);
        }

        public decimal getDecimal(string column)
        {
            if (isNull(column))
                return 0;
            else
                return Convert.ToDecimal(row[column]);
        }

        public double getDouble(string column)
        {
            if (isNull(column))
                return 0;
            else
                return Convert.ToDouble(row[column]);
        }

        private bool isNull(string column)
        {
            return row == null || row[column] == DBNull.Value || row[column] == null;
        }
    }
}
