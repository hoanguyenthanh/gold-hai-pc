﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace GoldManager.Res
{
    public class ConvertParameters
    {
        private SqlParameterCollection parameter;

        public ConvertParameters (SqlParameterCollection para)
        {
            this.parameter = para;
        }

        public string getString(string column)
        {
            if (isNull(column))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(parameter[column].Value);
            }
        }

        public int getInt(string column)
        {
            if (isNull(column))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(parameter[column].Value);
            }
        }


        public long getLong(string column)
        {
            if (isNull(column))
            {
                return 0;
            } else {
                return Convert.ToInt64(parameter[column].Value);
            }
        }

        private bool isNull(string column)
        {
            return parameter == null || parameter[column].Value == DBNull.Value || parameter[column].Value == null;
        }
    }
}
