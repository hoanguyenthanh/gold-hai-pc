﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Res
{
    class MConts
    {
        public const string FORMAT_PRODUCT_CODE = "{0:d8}";

        public const string FORM_GOLD_TYPE = "";

        public const int USER_PERMISTION_ADMIN = 1;
        public const string USER_PERMISTION_ADMIN_NAME = "Quản trị";

        public const int USER_PERMISTION_NORMAL = 0;
        public const string USER_PERMISTION_NORMAL_NAME = "Thường";


        public const int DATA_ACTIVE_NOT_CHECK = -1;


        public const int DATA_ACTIVE = 1;
        public const string DATA_ACTIVE_NAME = "Hoạt động";

        public const int DATA_UNACTIVE = 0;
        public const string DATA_UNACTIVE_NAME = "Không hoạt động";


        public const string DROPDOWN_DEFAULT_VALUE_KEY = "-1";
        public const string DROPDOWN_DEFAULT_VALUE_TEXT = "";
        public const string DROPDOWN_DEFAULT_VALUE_DESC = "---Chọn---";


        //Value Stalls
        public const string STALLS_01 = "1";
        public const string STALLS_01_DESC = "Quầy 1";

        public const string STALLS_02 = "2";
        public const string STALLS_02_DESC = "Quầy 2";

        public const string STALLS_03 = "3";
        public const string STALLS_03_DESC = "Quầy 3";

        public const string STALLS_04 = "4";
        public const string STALLS_04_DESC = "Quầy 4";

        public const string STALLS_05 = "5";
        public const string STALLS_05_DESC = "Quầy 5";

        public const string STALLS_06 = "6";
        public const string STALLS_06_DESC = "Quầy 6";

        public const string STALLS_07 = "7";
        public const string STALLS_07_DESC = "Quầy 7";

        public const string STALLS_08 = "8";
        public const string STALLS_08_DESC = "Quầy 8";

    }
}
