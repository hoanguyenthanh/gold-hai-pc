using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GoldManager.Views;

namespace GoldManager
{
    public static class Program
    {
        public static FrmHome frmHome;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmLogin());
        }
    }
}