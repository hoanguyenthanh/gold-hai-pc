﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;
using GoldManager.Process;
using GoldManager.Res;

namespace GoldManager.Logic
{
    public class GoldGroup_Logic : BaseLogic
    {
        public string strGroupID { get; set; }
        public string strGroupCode { get; set; }
        public string strGroupName { get; set; }
        public int intActive { get; set; }

        public int intActionName { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            object result = null;
            string sql = "";

            switch (intActionName)
            {
                case GoldGroup_Process.ACT_GET_GROUP:
                    sql = sqlGetGroup();
                    DataTable table = executeQuery(sql.ToString());
                    result = table;
                    break;

                case GoldGroup_Process.ACT_IST_GROUP:
                    sql = sqlInsert();
                    result = executeNonQuery(sql.ToString());
                    break;

                case GoldGroup_Process.ACT_UPD_GROUP:
                    sql = sqlUpdate();
                    result = executeNonQuery(sql.ToString());
                    break;

                case GoldGroup_Process.ACT_DEL_GROUP:
                    sql = sqlDelete();
                    result = executeNonQuery(sql.ToString());
                    break;

                case GoldGroup_Process.ACT_CREATE_GROUPD_ID:
                    result = createGroupID();
                    break;

                case GoldGroup_Process.ACT_EXISTS_GROUP:
                    result = checkExistGroupCode();
                    break;

                default:
                    result = 1;
                    break;
            }

            return result;
        }


        private string sqlGetGroup()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT");
            sql.AppendLine("    ID");
            sql.AppendLine("   ,GroupID");
            sql.AppendLine("   ,GroupCode");
            sql.AppendLine("   ,GroupName");
            sql.AppendLine("   ,Active");
            sql.AppendLine("   ,CASE WHEN ISNULL(Active, '0') = '1' THEN N'Hoạt động' ELSE N'Không'");
            sql.AppendLine("    END AS ActiveDesc");
            sql.AppendLine(" FROM WS_GROUP");

            if (intActive != MConts.DATA_ACTIVE_NOT_CHECK)
            {
                //sql.AppendLine(" WHERE Active = '" + intActive + "'");
            }

            return sql.ToString();
        }

        private string sqlInsert()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("INSERT INTO WS_GROUP(GroupID, GroupCode, GroupName, Active)");
            sql.AppendLine("VALUES (");
            sql.AppendLine("   '" + strGroupID + "'");
            sql.AppendLine(" ,N'" + strGroupCode + "'");
            sql.AppendLine(" ,N'" + strGroupName + "'");
            sql.AppendLine(" ,'" + intActive + "'");
            sql.AppendLine(")");

            return sql.ToString();
        }

        private string sqlUpdate()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("UPDATE WS_GROUP SET");
            sql.AppendLine("  GroupName = N'" + strGroupName + "'");
            sql.AppendLine(" ,Active = '" + intActive + "'");
            sql.AppendLine(" WHERE GroupCode = '" + strGroupCode + "'");

            return sql.ToString();
        }

        private string sqlDelete()
        {
            return "DELETE WS_GROUP WHERE GroupCode = '" + strGroupCode + "'";
        }


        private string sqlCheckExist()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT 0");
            sql.AppendLine(" FROM WS_GROUP");
            sql.AppendLine(" WHERE GroupCode = '" + strGroupCode + "'");
            return sql.ToString();
        }


        private string createGroupID()
        {
            DataTable table = executeQuery("SELECT COUNT(0) AS CountRow FROM WS_GROUP");
            Int64 count = Int64.Parse(table.Rows[0]["CountRow"].ToString());
            string groupID = "PG" + string.Format("{0:0000}", DateTime.Now.Year) 
                + string.Format("{0:00}", DateTime.Now.Month) + string.Format("{0:0000000}", count);
            return groupID;
        }

        private bool checkExistGroupCode()
        {
            string sql = sqlCheckExist();
            DataTable table = executeQuery(sql);

            return table != null && table.Rows.Count > 0;
        }
    }
}
