﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class GetOrderList_Logic : BaseLogic
    {
        public DateTime DtFromDate { get; set; }
        public DateTime DtToDate { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.AppendLine("SELECT");
            strSql.AppendLine("     ord.OrderId");
            strSql.AppendLine("     ,ord.ModUser");
            strSql.AppendLine("     ,CONVERT(varchar(10), ord.ModDt, 105)  AS DateTrans");
            strSql.AppendLine("     ,CONVERT(varchar(8), ord.ModDt, 108) AS TimeTrans");
            strSql.AppendLine("     ,ord.Status");
            strSql.AppendLine("     ,CASE WHEN ord.Status = 'new' THEN N'Mới' ELSE N'Đã thanh toán' END AS StatusName");
            strSql.AppendLine("     ,ord.TypeTrans");
            strSql.AppendLine("     ,CASE ord.TypeTrans");
            strSql.AppendLine("         WHEN 'sell' THEN N'Bán'");
            strSql.AppendLine("         WHEN 'buy' THEN N'Mua'");
            strSql.AppendLine("         WHEN 'sell_and_buy' THEN N'Mua-Bán'");
            strSql.AppendLine("      END AS TypeTransName");
            strSql.AppendLine("     ,ord.SellTotalWeight");
            strSql.AppendLine("     ,ord.BuyTotalWeight");
            strSql.AppendLine("     ,ord.TotalWeight");
            strSql.AppendLine("     ,REPLACE(CONVERT(varchar(20), (CAST(ord.PriceSell AS money)), 1), '.00', '') AS PriceSell");
            strSql.AppendLine("     ,REPLACE(CONVERT(varchar(20), (CAST(ord.PriceBuy AS money)), 1), '.00', '') AS PriceBuy");
            strSql.AppendLine("     ,REPLACE(CONVERT(varchar(20), (CAST(ord.Amount AS money)), 1), '.00', '') AS Amount");
            strSql.AppendLine("     ,REPLACE(CONVERT(varchar(20), (CAST(ord.TotalSalary AS money)), 1), '.00', '') AS TotalSalary");
            strSql.AppendLine("     ,REPLACE(CONVERT(varchar(20), (CAST(ord.TotalPayBonus AS money)), 1), '.00', '')  AS TotalPayBonus");
            strSql.AppendLine("     ,REPLACE(CONVERT(varchar(20), (CAST(ord.TotalAmount AS money)), 1), '.00', '') AS TotalAmount");
            strSql.AppendLine("FROM WS_ORDER ord ");
            strSql.AppendLine("   WHERE 1 = 1");

            if (this.DtFromDate != null)
            {
                strSql.AppendLine(" AND YEAR(ord.ModDt) >= " + this.DtFromDate.Year);
                strSql.AppendLine(" AND MONTH(ord.ModDt) >= " + this.DtFromDate.Month);
                strSql.AppendLine(" AND DAY(ord.ModDt) >= " + this.DtFromDate.Day);
            }
            if (this.DtToDate != null)
            {
                strSql.AppendLine(" AND YEAR(ord.ModDt) <= " + this.DtToDate.Year);
                strSql.AppendLine(" AND MONTH(ord.ModDt) <= " + this.DtToDate.Month);
                strSql.AppendLine(" AND DAY(ord.ModDt) <= " + this.DtToDate.Day);
            }

            strSql.AppendLine(" ORDER BY ord.ModDt DESC");

            DataTable dtResult = executeQuery(strSql.ToString());

            return dtResult;
        }
    }
}
