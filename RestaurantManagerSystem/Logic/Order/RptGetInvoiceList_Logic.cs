﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using GoldManager.Models;
using GoldManager.Logic.Common;

namespace GoldManager.Logic
{
    public class RptGetInvoiceList_Logic : BaseLogic
    {
        public long OrderID { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(" SELECT ord.OrderId");
            strSql.AppendLine("    ,ord.ModUser");
            strSql.AppendLine("    ,ord.ModDt");
            strSql.AppendLine("    ,CASE WHEN ord.SellTotalWeight > 0 THEN CONVERT(VARCHAR(20), ord.SellTotalWeight) ELSE '-'");
            strSql.AppendLine("     END AS SellTotalWeight");
            strSql.AppendLine("    ,CASE WHEN ord.SellTotalWeight_L > 0 THEN CONVERT(VARCHAR(20), ord.SellTotalWeight_L) ELSE ''");
            strSql.AppendLine("     END AS SellTotalWeight_L");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.SellTotalWeight_C > 0 THEN CONVERT(VARCHAR(20), ord.SellTotalWeight_C)");
            strSql.AppendLine("         WHEN ord.SellTotalWeight_C = 0 AND ord.SellTotalWeight_L > 0 THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS SellTotalWeight_C");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.SellTotalWeight_P > 0 THEN CONVERT(VARCHAR(20), ord.SellTotalWeight_P)");
            strSql.AppendLine("         WHEN ord.SellTotalWeight_P = 0 AND (ord.SellTotalWeight_L > 0 OR ord.SellTotalWeight_C > 0) THEN '0'");
            strSql.AppendLine("         ELSE '' ");
            strSql.AppendLine("     END AS SellTotalWeight_P");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.SellTotalWeight_Li > 0 THEN CONVERT(VARCHAR(20), ord.SellTotalWeight_Li)");
            strSql.AppendLine("         WHEN ord.SellTotalWeight_Li = 0 AND (ord.SellTotalWeight_L > 0 OR ord.SellTotalWeight_C > 0 OR ord.SellTotalWeight_P > 0)");
            strSql.AppendLine("             THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS SellTotalWeight_Li");



            strSql.AppendLine("    ,CASE WHEN ord.SellTotalWeight_L > 0 THEN 'l' ELSE '' END AS SellTotalWeightSym_L");
            strSql.AppendLine("    ,CASE WHEN ord.SellTotalWeight_C > 0 THEN 'c' ELSE '' END AS SellTotalWeightSym_C");
            strSql.AppendLine("    ,CASE WHEN ord.SellTotalWeight_P > 0 THEN 'p' ELSE '' END AS SellTotalWeightSym_P");
            strSql.AppendLine("    ,CASE WHEN ord.SellTotalWeight_Li > 0 THEN 'li' ELSE '' END AS SellTotalWeightSym_Li");

            strSql.AppendLine("    ,CASE WHEN ord.BuyTotalWeight > 0 THEN CONVERT(VARCHAR(20), ord.BuyTotalWeight) ELSE '-'");
            strSql.AppendLine("     END AS BuyTotalWeight");
            strSql.AppendLine("    ,CASE WHEN ord.BuyTotalWeight_L > 0 THEN CONVERT(VARCHAR(20), ord.BuyTotalWeight_L) ELSE ''");
            strSql.AppendLine("     END AS BuyTotalWeight_L");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.BuyTotalWeight_C > 0 THEN CONVERT(VARCHAR(20), ord.BuyTotalWeight_C)");
            strSql.AppendLine("         WHEN ord.BuyTotalWeight_C = 0 AND ord.BuyTotalWeight_L > 0 THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS BuyTotalWeight_C");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.BuyTotalWeight_P > 0 THEN CONVERT(VARCHAR(20), ord.BuyTotalWeight_P)");
            strSql.AppendLine("         WHEN ord.BuyTotalWeight_P = 0 AND (ord.BuyTotalWeight_L > 0 OR ord.BuyTotalWeight_C > 0) THEN '0'");
            strSql.AppendLine("         ELSE '' ");
            strSql.AppendLine("     END AS BuyTotalWeight_P");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.BuyTotalWeight_Li > 0 THEN CONVERT(VARCHAR(20), ord.BuyTotalWeight_Li)");
            strSql.AppendLine("         WHEN ord.BuyTotalWeight_Li = 0 AND (ord.BuyTotalWeight_L > 0 OR ord.BuyTotalWeight_C > 0 OR ord.BuyTotalWeight_P > 0)");
            strSql.AppendLine("             THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS BuyTotalWeight_Li");

            strSql.AppendLine("    ,CASE WHEN ord.BuyTotalWeight_L > 0 THEN 'l' ELSE '' END AS BuyTotalWeightSym_L");
            strSql.AppendLine("    ,CASE WHEN ord.BuyTotalWeight_C > 0 THEN 'c' ELSE '' END AS BuyTotalWeightSym_C");
            strSql.AppendLine("    ,CASE WHEN ord.BuyTotalWeight_P > 0 THEN 'p' ELSE '' END AS BuyTotalWeightSym_P");
            strSql.AppendLine("    ,CASE WHEN ord.BuyTotalWeight_Li > 0 THEN 'li' ELSE '' END AS BuyTotalWeightSym_Li");



            strSql.AppendLine("    ,CASE WHEN ord.TotalWeight > 0 THEN CONVERT(VARCHAR(20), ord.TotalWeight) ELSE '-'");
            strSql.AppendLine("     END AS TotalWeight");
            strSql.AppendLine("    ,CASE WHEN ord.TotalWeight_L > 0 THEN CONVERT(VARCHAR(20), ord.TotalWeight_L) ELSE ''");
            strSql.AppendLine("     END AS TotalWeight_L");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.TotalWeight_C > 0 THEN CONVERT(VARCHAR(20), ord.TotalWeight_C)");
            strSql.AppendLine("         WHEN ord.TotalWeight_C = 0 AND ord.TotalWeight_L > 0 THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS TotalWeight_C");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.TotalWeight_P > 0 THEN CONVERT(VARCHAR(20), ord.TotalWeight_P)");
            strSql.AppendLine("         WHEN ord.TotalWeight_P = 0 AND (ord.TotalWeight_L > 0 OR ord.TotalWeight_C > 0) THEN '0'");
            strSql.AppendLine("         ELSE '' ");
            strSql.AppendLine("     END AS TotalWeight_P");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.TotalWeight_Li > 0 THEN CONVERT(VARCHAR(20), ord.TotalWeight_Li)");
            strSql.AppendLine("         WHEN ord.TotalWeight_Li = 0 AND (ord.TotalWeight_L > 0 OR ord.TotalWeight_C > 0 OR ord.TotalWeight_P > 0)");
            strSql.AppendLine("             THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS TotalWeight_Li");

            strSql.AppendLine("    ,CASE WHEN ord.TotalWeight_L > 0 THEN 'l' ELSE '' END AS TotalWeightSym_L");
            strSql.AppendLine("    ,CASE WHEN ord.TotalWeight_C > 0 THEN 'c' ELSE '' END AS TotalWeightSym_C");
            strSql.AppendLine("    ,CASE WHEN ord.TotalWeight_P > 0 THEN 'p' ELSE '' END AS TotalWeightSym_P");
            strSql.AppendLine("    ,CASE WHEN ord.TotalWeight_Li > 0 THEN 'li' ELSE '' END AS TotalWeightSym_Li");


            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ABS(ord.PriceSell) AS money)), 1), '.00', '') AS PriceSell");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ABS(ord.PriceBuy) AS money)), 1), '.00', '') AS PriceBuy");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ABS(ord.Amount) AS money)), 1), '.00', '')  AS Amount");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ord.TotalSalary AS money)), 1), '.00', '') AS TotalSalary");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ord.TotalPayBonus AS money)), 1), '.00', '') AS TotalPayBonus");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ABS(ord.TotalAmount) AS money)), 1), '.00', '')  AS TotalAmount");
            strSql.AppendLine("    ,ordDT.ProductCode");
            strSql.AppendLine("    ,ordDT.ProductDesc");
            strSql.AppendLine("    ,ordDT.DiamondWeight");



            strSql.AppendLine("    ,CASE WHEN ordDT.TotalWeightDT > 0 THEN CONVERT(VARCHAR(20), ordDT.TotalWeightDT) ELSE '-'");
            strSql.AppendLine("     END AS TotalWeightDT");

            strSql.AppendLine("    ,CASE WHEN ordDT.TotalWeightDT_L > 0 THEN CONVERT(VARCHAR(20), ordDT.TotalWeightDT_L) ELSE ''");
            strSql.AppendLine("     END AS TotalWeightDT_L");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ordDT.TotalWeightDT_C > 0 THEN CONVERT(VARCHAR(20), ordDT.TotalWeightDT_C)");
            strSql.AppendLine("         WHEN ordDT.TotalWeightDT_C = 0 AND ordDT.TotalWeightDT_L > 0 THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS TotalWeightDT_C");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ordDT.TotalWeightDT_P > 0 THEN CONVERT(VARCHAR(20), ordDT.TotalWeightDT_P)");
            strSql.AppendLine("         WHEN ordDT.TotalWeightDT_P = 0 AND (ordDT.TotalWeightDT_L > 0 OR ordDT.TotalWeightDT_C > 0) THEN '0'");
            strSql.AppendLine("         ELSE '' ");
            strSql.AppendLine("     END AS TotalWeightDT_P");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ordDT.TotalWeightDT_Li > 0 THEN CONVERT(VARCHAR(20), ordDT.TotalWeightDT_Li)");
            strSql.AppendLine("         WHEN ordDT.TotalWeightDT_Li = 0 AND (ordDT.TotalWeightDT_L > 0 OR ordDT.TotalWeightDT_C > 0 OR ordDT.TotalWeightDT_P > 0)");
            strSql.AppendLine("             THEN '0'");
            strSql.AppendLine("         ELSE ''");
            strSql.AppendLine("     END AS TotalWeightDT_Li");

            strSql.AppendLine("    ,CASE WHEN ordDT.TotalWeightDT_L > 0 THEN 'l' ELSE '' END AS TotalWeightDTSym_L");
            strSql.AppendLine("    ,CASE WHEN ordDT.TotalWeightDT_C > 0 THEN 'c' ELSE '' END AS TotalWeightDTSym_C");
            strSql.AppendLine("    ,CASE WHEN ordDT.TotalWeightDT_P > 0 THEN 'p' ELSE '' END AS TotalWeightDTSym_P");
            strSql.AppendLine("    ,CASE WHEN ordDT.TotalWeightDT_Li > 0 THEN 'li' ELSE '' END AS TotalWeightDTSym_Li");

            strSql.AppendLine("    ,REPLACE(CONVERT(VARCHAR(20), CAST(ordDT.PriceDT AS money), 1), '.00', '') AS PriceDT");
            strSql.AppendLine("    ,REPLACE(CONVERT(VARCHAR(20), CAST(ordDT.AmountDT AS money), 1), '.00', '') AS AmountDT");
            strSql.AppendLine("    ,REPLACE(CONVERT(VARCHAR(20), CAST(SUM(ordDT.AmountDT) OVER(PARTITION BY ordDT.OrderId) AS money), 1), '.00', '') AS AmountDTSum");
            strSql.AppendLine("    ,REPLACE(CONVERT(VARCHAR(20), CAST(ordDT.PayBonus AS money), 1), '.00', '') AS PayBonus");
            strSql.AppendLine("    ,REPLACE(CONVERT(VARCHAR(20), CAST(ordDT.PayBonusAmount AS money), 1), '.00', '') AS PayBonusAmount");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST(ordDT.Salary AS money)), 1), '.00', '') AS Salary");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST((ord.TotalSalary/1000) AS money)), 1), '.00', '') AS SalarySum");
            strSql.AppendLine("    ,REPLACE(CONVERT(varchar(20), (CAST((ord.TotalPayBonus/1000) AS money)), 1), '.00', '') AS PayBonusSum");
            strSql.AppendLine("    ,ordDT.Depot");
            strSql.AppendLine("    ,ordDT.TypeTrans");
            strSql.AppendLine("    ,typ.GoldDesc");
            strSql.AppendLine("    ,grp.GroupName");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.Amount < 0 THEN N'TIỆM phải trả'");
            strSql.AppendLine("         ELSE N'KHÁCH phải trả'");
            strSql.AppendLine("     END AS LabelAmount");
            strSql.AppendLine("    ,CASE");
            strSql.AppendLine("         WHEN ord.TotalAmount < 0 THEN N'TIỆM phải trả'");
            strSql.AppendLine("         ELSE N'KHÁCH phải trả'");
            strSql.AppendLine("     END AS LabelTotalAmount");
            strSql.AppendLine(" FROM WS_ORDER ord");
	        strSql.AppendLine("  INNER JOIN WS_ORDER_DT ordDT");
		    strSql.AppendLine("      ON ord.OrderId = ordDT.OrderId");
	        strSql.AppendLine("  LEFT JOIN WS_TYPE typ");
		    strSql.AppendLine("      ON ordDT.GoldType = typ.GoldCode");
	        strSql.AppendLine("  LEFT JOIN WS_GROUP grp");
		    strSql.AppendLine("      ON ordDT.GroupID = grp.GroupID");
            strSql.AppendLine(" WHERE ord.OrderId = " + OrderID);

            DataTable dtResult = new DataTable();

            dtResult = base.executeQuery(strSql.ToString());

            return dtResult;
        }
    }
}
