﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class UpdateGoldType_Logic : BaseLogic
    {
        public string strGoldCode { get; set; }
        public string strGoldDesc { get; set; }
        public string strNote { get; set; }
        public string strOld { get; set; }
        public double PriceSell { get; set; }
        public double PriceBuy { get; set; }
        public int intActive { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("UPDATE WS_TYPE SET");
            sql.AppendLine("  GoldDesc = N'" + strGoldDesc + "'");
            sql.AppendLine(" ,Notes = N'" + strNote + "'");
            sql.AppendLine(" ,Old = '" + strOld + "'");
            sql.AppendLine(" ,PriceSell = " + PriceSell);
            sql.AppendLine(" ,PriceBuy = " + PriceBuy);
            sql.AppendLine(" ,Active = " + intActive);
            sql.AppendLine(" WHERE GoldCode = '" + strGoldCode + "'");

            return executeNonQuery(sql.ToString());
        }
    }
}
