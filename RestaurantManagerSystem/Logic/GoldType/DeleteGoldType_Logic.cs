﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class DeleteGoldType_Logic : BaseLogic
    {
        public string strGoldCode { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("DELETE WS_TYPE WHERE GoldCode = '" + strGoldCode + "'");

            return executeNonQuery(strSql.ToString());
        }
    }
}