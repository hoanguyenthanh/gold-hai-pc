﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class InsertGoldType_Logic : BaseLogic
    {
        public string strGoldCode { get; set; }
        public string strGoldDesc { get; set; }
        public string strNotes { get; set; }
        public string strOld { get; set; }
        public double douPriceSell { get; set; }
        public double douPriceBuy { get; set; }
        public int intActive { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("INSERT INTO WS_TYPE(GoldCode, GoldDesc, PriceSell, PriceBuy, Active, Notes, Old)");
            sql.AppendLine("VALUES (");
            sql.AppendLine("  N'" + strGoldCode + "'");
            sql.AppendLine(" ,N'" + strGoldDesc + "'");
            sql.AppendLine(" ," + douPriceSell + "");
            sql.AppendLine(" ," + douPriceBuy + "");
            sql.AppendLine(" ," + intActive);
            sql.AppendLine(" ,N'" + strNotes + "'");
            sql.AppendLine(" ,'" + strOld + "'");
            sql.AppendLine(")");

            return executeNonQuery(sql.ToString());
        }
    }
}
