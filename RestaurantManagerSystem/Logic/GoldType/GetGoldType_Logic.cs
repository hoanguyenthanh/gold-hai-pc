﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;
using GoldManager.Res;

namespace GoldManager.Logic
{
    class GetGoldType_Logic : BaseLogic
    {
        public int intActive { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            DataTable table = new DataTable();
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT");
            sql.AppendLine("    GoldCode");
            sql.AppendLine("   ,GoldDesc");
            sql.AppendLine("   ,Notes");
            sql.AppendLine("   ,Active");
            sql.AppendLine("   ,CASE WHEN ISNULL(Active, 0) = 1 THEN N'Hoạt động' ELSE N'Không'");
            sql.AppendLine("    END AS ActiveDesc");
            sql.AppendLine("   ,PriceSell");
            sql.AppendLine("   ,PriceBuy");
            sql.AppendLine("   ,Old");
            sql.AppendLine(" FROM WS_TYPE");
            table = executeQuery(sql.ToString());

            return table;
        }
    }
}
