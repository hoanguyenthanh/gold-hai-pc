﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class CheckExistRefe_Logic : BaseLogic
    {
        public string strGoldCode { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            DataTable table = new DataTable();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT 0");
            sql.AppendLine(" FROM WS_ORDER_DT");
            sql.AppendLine(" WHERE GoldType = '" + strGoldCode + "'");

            table = executeQuery(sql.ToString());

            return table;
        }
    }
}
