﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class CheckExistUsers_Logic : BaseLogic
    {
        public string strUserName {get; set;}

        protected override object Execute(params object[] arrObjParams)
        {
            DataTable table = new DataTable();
            
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT 0");
            sql.AppendLine(" FROM WS_USERS");
            sql.AppendLine(" WHERE UserName = '" + strUserName + "'");

            table = executeQuery(sql.ToString());
            return table;
        }
    }
}
