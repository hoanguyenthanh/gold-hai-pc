﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class GetUsers_Logic : BaseLogic
    {
        protected override object Execute(params object[] arrObjParams)
        {
            DataTable table = new DataTable();
            
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT");
            sql.AppendLine("    UserID");
            sql.AppendLine("   ,UserName");
            sql.AppendLine("   ,FullName");
            sql.AppendLine("   ,Mobile");
            sql.AppendLine("   ,ISNULL(IsAdmin, 0) AS IsAdmin");
            
            sql.AppendLine("   ,CASE WHEN ISNULL(IsAdmin, 0) = 1 THEN N'Quản trị' ELSE N'Bán hàng'");
            sql.AppendLine("    END AS IsAdminDesc");
            sql.AppendLine("   ,LastVisited");
            sql.AppendLine("   ,ISNULL(Active, 0) AS Active");
            sql.AppendLine("   ,CASE WHEN Active = 1 THEN N'Hoạt động' ELSE N'Không'");
            sql.AppendLine("    END ActiveDesc");
            sql.AppendLine(" FROM WS_USERS");

            table = executeQuery(sql.ToString());

            return table;
        }
    }
}
