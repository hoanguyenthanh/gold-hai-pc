﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class Login_Logic : BaseLogic
    {
        public string strUserName { get; set; }
        public string strPwd { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            DataTable dtResult = new DataTable();

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT");
            sql.AppendLine("    UserName");
            sql.AppendLine("   ,FullName");
            sql.AppendLine("   ,IsAdmin");
            sql.AppendLine(" FROM WS_USERS");
            sql.AppendLine(" WHERE UserName = '" + strUserName + "'");
            sql.AppendLine("    AND Password = '" + strPwd + "'");

            dtResult = executeQuery(sql.ToString());

            return dtResult;
        }
    }
}
