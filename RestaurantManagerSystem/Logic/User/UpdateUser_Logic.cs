﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class UpdateUser_Logic : BaseLogic
    {
        public string strUserName { get; set; }
        public string strFullName { get; set; }
        public string strPwd { get; set; }
        public string strMobile { get; set; }
        public int intIsAdmin { get; set; }
        public int intActive { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine(" UPDATE WS_USERS SET");
            sql.AppendLine("     FullName= N'" + strFullName + "'");
            sql.AppendLine("    ,Mobile= '" + strMobile + "'");
            sql.AppendLine("    ,IsAdmin= " + intIsAdmin);
            sql.AppendLine("    ,Active= " + intActive + "");
            if (strPwd != null && strPwd.Length > 0){
                sql.AppendLine("    ,Password= '" + strPwd + "'");
            }
            sql.AppendLine(" WHERE UserName = '" + strUserName + "'");

            return executeNonQuery(sql.ToString());
        }
    }
}