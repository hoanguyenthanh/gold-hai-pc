﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class DeleteUser_Logic : BaseLogic
    {
        public int intUserID {get; set;}

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("DELETE WS_USERS WHERE UserID = " + intUserID);

            return executeNonQuery(strSql.ToString());
        }
    }
}