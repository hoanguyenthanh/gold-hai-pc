﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    public class InsertUser_Logic : BaseLogic
    {
        public string strUserName {get; set;}
        public string strPassword {get; set;}
        public string strFullName {get; set;}
        public string strMobile { get; set; }
        public int intIsAdmin { get; set; }
        public int intActive { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("INSERT INTO WS_USERS(UserName, Password, FullName, Mobile, IsAdmin, Active)");
            sql.AppendLine("VALUES (");
            sql.AppendLine("  '" + strUserName + "'");
            sql.AppendLine(", '" + strPassword + "'");
            sql.AppendLine(", N'" + strFullName + "'");
            sql.AppendLine(", '" + strMobile + "'");
            sql.AppendLine(", " + intIsAdmin + "");
            sql.AppendLine(", " + intActive + "");
            sql.AppendLine(")");
            return executeNonQuery(sql.ToString());
        }
    }
}
