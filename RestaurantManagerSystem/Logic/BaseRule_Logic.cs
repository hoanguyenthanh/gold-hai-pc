﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;
using GoldManager.Process;
using GoldManager.Res;

namespace GoldManager.Logic
{
    public class BaseRule_Logic : BaseLogic
    {
        //Action name
        public int intActionName { get; set; }

        //Field data
        public int intBaseRuleId { get; set; }
        public string strBaseRuleName { get; set; }
        public string strBaseRuleDesc { get; set; }
        public int intActive { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            object result = null;
            string sql = "";

            switch (intActionName)
            {
                case BaseRule_Process.ACT_GET_RULE:
                    sql = sqlGetBaseRule();
                    DataTable table = executeQuery(sql.ToString());
                    result = table;
                    break;

                case BaseRule_Process.ACT_IST_RULE:
                    sql = sqlInsert();
                    result = executeNonQuery(sql.ToString());
                    break;

                case BaseRule_Process.ACT_UPD_RULE:
                    sql = sqlUpdate();
                    result = executeNonQuery(sql.ToString());
                    break;

                case BaseRule_Process.ACT_DEL_RULE:
                    sql = sqlDelete();
                    result = executeNonQuery(sql.ToString());
                    break;

                case BaseRule_Process.ACT_EXISTS_NAME:
                    result = isExistName();
                    break;

                case BaseRule_Process.ACT_DOUBLE_NAME:
                    result = checkDoubleName();
                    break;
                default:
                    result = 1;
                    break;
            }

            return result;
        }


        private string sqlGetBaseRule()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT");
            sql.AppendLine("    BaseRuleId");
            sql.AppendLine("   ,BaseRuleName");
            sql.AppendLine("   ,BaseRuleDesc");
            sql.AppendLine("   ,Active");
            sql.AppendLine("   ,CASE WHEN ISNULL(Active, 0) = 1 THEN N'Hoạt động' ELSE N'Không'");
            sql.AppendLine("    END AS ActiveDesc");
            sql.AppendLine(" FROM WS_BASE_RULE");
            if (intActive != MConts.DATA_ACTIVE_NOT_CHECK)
            {
                sql.AppendLine(" WHERE Active = " + intActive);
            }

            return sql.ToString();
        }

        private string sqlInsert()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("INSERT INTO WS_BASE_RULE(BaseRuleName, BaseRuleDesc, Active)");
            sql.AppendLine("VALUES (");
            sql.AppendLine("  N'" + strBaseRuleName + "'");
            sql.AppendLine(" ,N'" + strBaseRuleDesc + "'");
            if (intActive == MConts.DATA_ACTIVE)
            {
                sql.AppendLine(" ," + MConts.DATA_ACTIVE);
            }
            else
            {
                sql.AppendLine(" ," + MConts.DATA_UNACTIVE);
            }
            sql.AppendLine(")");

            return sql.ToString();
        }

        private string sqlUpdate()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("UPDATE WS_BASE_RULE SET");
            sql.AppendLine(" BaseRuleDesc = N'" + strBaseRuleDesc + "'");
            if (intActive == MConts.DATA_ACTIVE)
            {
                sql.AppendLine(" ,Active = " + MConts.DATA_ACTIVE);
            }
            else
            {
                sql.AppendLine(" ,Active = " + MConts.DATA_UNACTIVE);
            }

            sql.AppendLine(" WHERE BaseRuleName = '" + strBaseRuleName + "'");

            return sql.ToString();
        }

        private string sqlDelete()
        {
            return "DELETE WS_BASE_RULE WHERE BaseRuleName = '" + strBaseRuleName + "'";
        }


        private string sqlCheckDoubleName()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT 0");
            sql.AppendLine(" FROM WS_BASE_RULE");
            sql.AppendLine(" WHERE BaseRuleName = N'" + strBaseRuleName + "'");
            sql.AppendLine("    AND BaseRuleId <> " + intBaseRuleId);
            return sql.ToString();
        }


        private string sqlCheckExistName()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT 0");
            sql.AppendLine(" FROM WS_BASE_RULE");
            sql.AppendLine(" WHERE BaseRuleName = N'" + strBaseRuleName + "'");
            return sql.ToString();
        }

        /*
         * Check double base name rule when update data
         * Return: true or false
         */
        private bool checkDoubleName()
        {
            string sql = sqlCheckDoubleName();
            DataTable table = executeQuery(sql);
            if (table != null && table.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }


        /*
         * Check exist base rule name
         * Return: true or false
         */
        private bool isExistName()
        {
            string sql = sqlCheckExistName();
            DataTable table = executeQuery(sql);

            return table != null && table.Rows.Count > 0;
        }
    }
}
