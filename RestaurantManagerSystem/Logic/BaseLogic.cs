﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NTH.Framework.Log;
using System.Data;
using GoldManager.Res;

namespace GoldManager.Logic.Common
{
    public abstract class BaseLogic : IDisposable
    {
        public const int LOGIC_ERR_DATA_NOT_FOUND = -3;
        public const int LOGIC_ERR_DATAACCESS = -2;
        public const int LOGIC_ERR_UNKNOWN = -999;
        public const int LOGIC_ERR_VALIDATION = -1;
        public const int LOGIC_RETRY_TRANSACTION = -4;
        public const int LOGIC_SUCCESS = 0;
        public const int LOGIC_OK = 1;

        protected SqlConnection connect = null;
        protected SqlCommand command = null;
        protected SqlDataAdapter adapter = null;
        
        private IDelivLog m_LogOut = null;



        protected IDelivLog Log
        {
            get { return this.m_LogOut; }
            private set { this.m_LogOut = value; }
        }


        public void Dispose()
        {
            if (command != null)
            {
                command.Dispose();
            }
            if (adapter != null)
            {
                adapter.Dispose();
            }
            m_LogOut = null;
        }


        //Implement in subclass
        protected abstract object Execute(params object[] arrObjParams);


        protected DataTable executeQuery(string strSql) {
            DataTable table = new DataTable();

            command.CommandText = strSql;
            adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            adapter.Fill(table);

            return table;
        }



        //Insert và trả về số dòng đã insert
        protected int executeNonQuery(string strSql) {
            command.CommandText = strSql;
            return command.ExecuteNonQuery();
        }


        protected int executeNonQuery(string strSql, SqlTransaction trans)
        {
            if (trans != null)
            {
                command.Transaction = trans;
            }
            command.CommandText = strSql;
            return command.ExecuteNonQuery();
        }


        //Insert và trả về ID
        protected long executeScalar(string strSql, string column, SqlTransaction trans)
        {
            long id = 0;
            try
            {
                if (trans != null)
                {
                    command.Transaction = trans;
                }

                command.CommandText = strSql;
                SqlDataReader dr = command.ExecuteReader();

                if (dr != null && dr.Read())
                {
                    id = long.Parse(dr[column].ToString());
                }

                dr.Close();
                ((IDisposable)dr).Dispose();
            }
            catch (Exception ex) {
                return -1;
            }
            return id;
        }

        // Call in Process
        public object ExecuteLogic(SqlConnection dbAccess, params object[] arrObjParams)
        {
            object objResult = null;
            this.Log = LogFactory.CreateLog();
            Log.Debug(ValueConstant.LOG_LOGIC_START);

            try
            {
                connect = dbAccess;
                command = connect.CreateCommand();
                objResult = Execute(arrObjParams);
            }
            catch (SqlException exDbAccess)
            {
                Log.Error(exDbAccess.Message);
            }
            catch (Exception ex){
                Log.Error(ex.Message);
            }
            finally
            {
                Log.Debug(ValueConstant.LOG_LOGIC_END);
            }

            return objResult;
        }
    }
}
