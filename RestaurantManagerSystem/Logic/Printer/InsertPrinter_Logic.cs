﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class InsertPrinter_Logic : BaseLogic
    {
        public string printerName { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("INSERT INTO WS_PRINTER(PrintName, Device)");
            strSql.AppendLine("VALUES ('" + printerName + "', 'PRINT')");

            int iResult = executeNonQuery(strSql.ToString());
            return iResult;
        }
    }
}
