﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class DeletePrinter_Logic : BaseLogic
    {
        protected override object Execute(params object[] arrObjParams)
        {
            return executeNonQuery("DELETE WS_PRINTER WHERE Device = 'PRINT'");
        }
    }
}
