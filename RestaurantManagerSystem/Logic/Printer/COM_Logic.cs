﻿using GoldManager.Logic.Common;
using GoldManager.Process;
using System.Data;
using System.Text;

namespace GoldManager.Logic
{
    class COM_Logic : BaseLogic
    {
        public string strName { get; set; }
        public int ActionName { get; set; }

        protected override object Execute(params object[] arrObjParams)
        {
            object result = null;

            switch (ActionName)
            {
                case COM_Process.ACTION_SELECT:
                    result = (DataTable)executeQuery(sqlGet());
                    break;
                case COM_Process.ACTION_INSERT:
                    result = executeNonQuery(sqlInsert());
                    break;
                case COM_Process.ACTION_DELETE:
                    result = executeNonQuery(sqlDelete());
                    break;

            }

            return result;
        }

        private string sqlGet()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("SELECT PrintName FROM WS_PRINTER WHERE Device = 'COM'");
            return strSql.ToString();
        }

        private string sqlDelete()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(" DELETE WS_PRINTER");
            strSql.AppendLine(" WHERE Device = 'COM'");

            return strSql.ToString();
        }

        private string sqlInsert()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(" INSERT INTO WS_PRINTER(PrintName, Device) ");
            strSql.AppendLine(" VALUES ('" + strName + "', 'COM') ");

            return strSql.ToString();
        }

    }
}
