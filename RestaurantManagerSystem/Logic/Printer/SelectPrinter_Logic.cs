﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Logic.Common;
using System.Data;

namespace GoldManager.Logic
{
    class SelectPrinter_Logic : BaseLogic
    {
        protected override object Execute(params object[] arrObjParams)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.AppendLine("SELECT");
            strSql.AppendLine("     PrintID ");
            strSql.AppendLine("    ,PrintName ");
            strSql.AppendLine("    ,IsDefault ");
            strSql.AppendLine(" FROM WS_PRINTER");
            strSql.AppendLine(" WHERE Device = 'PRINT'");

            DataTable dtResult = executeQuery(strSql.ToString());

            return dtResult;
        }


    }
}
