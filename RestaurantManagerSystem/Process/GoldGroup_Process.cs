﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Controllers.Common;
using System.Data;
using GoldManager.Logic;

namespace GoldManager.Process
{
    public class GoldGroup_Process : BaseProcess
    {
        public const int ACT_GET_GROUP = 1;
        public const int ACT_DEL_GROUP = 2;
        public const int ACT_IST_GROUP = 3;
        public const int ACT_UPD_GROUP = 4;
        public const int ACT_EXISTS_GROUP = 5;
        public const int ACT_EXISTS_REFE = 6;
        public const int ACT_CREATE_GROUPD_ID = 7;

        public int intActionName {get; set;}
        
        public string strGroupID { get; set; }
        public string strGroupCode { get; set; }
        public string strGroupName { get; set; }
        public int intActive { get; set; }

        //Data result for select list group
        public DataTable DtResult { get; set; }


        //Check exist group code
        public bool isExistGroupCode { get; set; }

        protected override int Execute()
        {
            DtResult = new DataTable();

            using (GoldGroup_Logic logic = new GoldGroup_Logic())
            {
                //Action name
                logic.intActionName = intActionName;

                //Value
                logic.strGroupID = strGroupID;
                logic.strGroupCode = strGroupCode;
                logic.strGroupName = strGroupName;
                logic.intActive = intActive;

                switch (intActionName)
                {
                    case ACT_GET_GROUP:
                        DtResult = logic.ExecuteLogic(this.DbAccessObject) as DataTable;
                        break;
                    case ACT_IST_GROUP:
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_UPD_GROUP:
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_DEL_GROUP:
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_CREATE_GROUPD_ID:
                        strGroupID = (string)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_EXISTS_GROUP:
                        isExistGroupCode = (bool)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                }
            }
            return resultCode;
        }
    }
}
