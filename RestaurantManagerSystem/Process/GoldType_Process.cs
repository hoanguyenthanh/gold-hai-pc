﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Controllers.Common;
using System.Data;
using GoldManager.Logic;
using GoldManager.Models;
using System.Data.SqlClient;

namespace GoldManager.Process
{
    public class GoldType_Process : BaseProcess
    {
        public const int ACT_GET_GOLD_TYPE = 1;
        public const int ACT_IST_GOLD_TYPE = 3;
        public const int ACT_UPD_GOLD_TYPE = 4;
        public const int ACT_EXISTS_GOLD_TYPE = 5;
        public const int ACT_EXISTS_REFE = 6;

        public int intActionName {get; set;}
        public DataTable DtResult { get; set; }

        public string strGoldCode { get; set; }
        public string strGoldDesc { get; set; }
        public string strNote { get; set; }
        public string strOld { get; set; }
        public double douPriceSell { get; set; }
        public double douPriceBuy { get; set; }
        public int intActive { get; set; }

        protected override int Execute()
        {
            DtResult = new DataTable();
            switch (intActionName)
            {
                case ACT_GET_GOLD_TYPE:
                    DtResult = getGoldType();
                    break;
                case ACT_IST_GOLD_TYPE:
                    resultCode = insertGoldType();
                    break;
                case ACT_UPD_GOLD_TYPE:
                    resultCode = updateGoldType();
                    break;
                case ACT_EXISTS_GOLD_TYPE:
                    resultCode = checkExistGoldType();
                    break;
                case ACT_EXISTS_REFE:
                    resultCode = checkExistRefe();
                    break;
            }

            return resultCode;
        }

        private DataTable getGoldType(){
            DataTable table = new DataTable();
            using (GetGoldType_Logic logic = new GetGoldType_Logic())
            {
                logic.intActive = intActive;
                table = logic.ExecuteLogic(this.DbAccessObject) as DataTable;
            }
            return table;
        }


        public bool updateData(string method)
        {
            int result = -1;
            SqlTransaction transation = null;
            try{
                createConnect();
                DbAccessObject.Open();
                transation = DbAccessObject.BeginTransaction();
                if (method == "insert")
                {
                    SqlCommand cmdUpdateGoldType = DbAccessObject.CreateCommand();
                    cmdUpdateGoldType.Transaction = transation;

                    if (strGoldDesc == null) strGoldDesc = "";

                    StringBuilder sql = new StringBuilder();
                    sql.AppendLine("INSERT INTO WS_TYPE(GoldCode, GoldDesc, PriceSell, PriceBuy, Active, Notes, Old)");
                    sql.AppendLine("VALUES (");
                    sql.AppendLine("  N'" + strGoldCode + "'");
                    sql.AppendLine(" ,N'" + strGoldDesc + "'");
                    sql.AppendLine(" ," + douPriceSell + "");
                    sql.AppendLine(" ," + douPriceBuy + "");
                    sql.AppendLine(" ," + intActive);
                    sql.AppendLine(" ,N'" + strNote + "'");
                    sql.AppendLine(" ,'" + strOld + "'");
                    sql.AppendLine(")");

                    cmdUpdateGoldType.CommandText = sql.ToString();
                    result = cmdUpdateGoldType.ExecuteNonQuery();

                    if (result > 0)
                    {
                        SqlCommand cmdSelectProductBal = DbAccessObject.CreateCommand();
                        cmdSelectProductBal.Transaction = transation;
                        cmdSelectProductBal.CommandText = "SELECT null FROM ws_product_bal WHERE GoldCode = '" + strGoldCode + "'";

                        SqlDataAdapter adapter = new SqlDataAdapter();
                        adapter.SelectCommand = cmdSelectProductBal;

                        int rowAffected = adapter.Fill(new DataTable());

                        if (rowAffected == 0)
                        {
                            SqlCommand cmdInsertProductBal = DbAccessObject.CreateCommand();
                            
                        }
                    }
                    
                    
                }
                else if (method == "update")
                {

                }
            } catch (SqlException ex)
            {
                result = -1;
            } finally 
            {
                if (transation != null){
                    if (result == -1)
                    {
                        transation.Rollback();
                    } else {
                        transation.Commit();
                    }
                }

                if (DbAccessObject != null)
                {
                    DbAccessObject.Close();
                }
            }

            return true;
        }
        

        public GoldType getGoldTypeByCode(string code)
        {
            GoldType goldType = null;

            try{
                createConnect();
                DbAccessObject.Open();

                DataTable table = new DataTable();
                StringBuilder sql = new StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine("    GoldCode");
                sql.AppendLine("   ,GoldDesc");
                sql.AppendLine("   ,Notes");
                sql.AppendLine("   ,Active");
                sql.AppendLine("   ,CASE WHEN ISNULL(Active, 0) = 1 THEN N'Hoạt động' ELSE N'Không'");
                sql.AppendLine("    END AS ActiveDesc");
                sql.AppendLine("   ,PriceSell");
                sql.AppendLine("   ,PriceBuy");
                sql.AppendLine("   ,Old");
                sql.AppendLine(" FROM WS_TYPE");
                sql.AppendLine(" WHERE 1 = 1");

                if (!String.IsNullOrEmpty(code))
                {
                    sql.AppendLine(" AND GoldCode = '" + code + "'");
                }

                SqlCommand command = DbAccessObject.CreateCommand();
                command.CommandText = sql.ToString();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(table);

                if (table != null && table.Rows.Count > 0){
                    goldType = new GoldType();
                    goldType.GoldCode = table.Rows[0]["GoldCode"].ToString();
                    goldType.GoldDesc = table.Rows[0]["GoldDesc"].ToString();
                }
            } catch (Exception ex) {
                goldType = null;
            } finally {
                if (DbAccessObject != null){
                    DbAccessObject.Close();
                }
            }
            return goldType;
        }

        private int insertGoldType(){
            int iResult = 0;
            using (InsertGoldType_Logic logic = new InsertGoldType_Logic())
            {
                logic.strGoldCode = this.strGoldCode == null ? "" : this.strGoldCode;
                logic.strGoldDesc = this.strGoldDesc == null ? "" : this.strGoldDesc;
                logic.douPriceSell = this.douPriceSell;
                logic.douPriceBuy = this.douPriceBuy;
                logic.strNotes = this.strNote;
                logic.strOld = this.strOld;
                logic.intActive = this.intActive;
                
                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }

        private int updateGoldType()
        {
            int iResult = 0;
            using (UpdateGoldType_Logic logic = new UpdateGoldType_Logic())
            {
                logic.strGoldCode = this.strGoldCode == null ? "" : this.strGoldCode;
                logic.strGoldDesc = this.strGoldDesc == null ? "" : this.strGoldDesc;
                logic.PriceBuy = this.douPriceBuy;
                logic.PriceSell = this.douPriceSell;
                logic.strNote = this.strNote;
                logic.strOld = this.strOld;
                logic.intActive = this.intActive;

                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }

        private int checkExistGoldType(){
            using(CheckExistGoldType_Logic logic = new CheckExistGoldType_Logic()){
                logic.strGoldCode = this.strGoldCode;
                DataTable dt = (DataTable)logic.ExecuteLogic(this.DbAccessObject);
                if (dt != null && dt.Rows.Count > 0){
                    return 1;
                }
            }
            return 0;
        }

        private int checkExistRefe()
        {
            using (CheckExistRefe_Logic logic = new CheckExistRefe_Logic())
            {
                logic.strGoldCode = this.strGoldCode;
                DataTable dt = (DataTable)logic.ExecuteLogic(this.DbAccessObject);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return 1;
                }
            }
            return 0;
        }
    }
}
