﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Controllers.Common;
using System.Data;
using GoldManager.Logic;

namespace GoldManager.Process
{
    public class BaseRule_Process : BaseProcess
    {
        public const int ACT_GET_RULE = 1;
        public const int ACT_DEL_RULE = 2;
        public const int ACT_IST_RULE = 3;
        public const int ACT_UPD_RULE = 4;
        public const int ACT_EXISTS_NAME = 5;
        public const int ACT_EXISTS_REFE = 6;
        public const int ACT_DOUBLE_NAME = 7;

        public int intActionName {get; set;}

        public int intBaseRuleId { get; set; }
        public string strBaseRuleName { get; set; }
        public string strBaseRuleDesc { get; set; }
        public int intActive { get; set; }

        //Data result for select list group
        public DataTable DtResult { get; set; }


        //Result for check Yes/No
        public bool resultYesNo { get; set; }

        protected override int Execute()
        {
            DtResult = new DataTable();

            using (BaseRule_Logic logic = new BaseRule_Logic())
            {
                //Action name
                logic.intActionName = intActionName;


                //Value
                logic.intBaseRuleId = intBaseRuleId;
                logic.strBaseRuleName = strBaseRuleName;
                logic.strBaseRuleDesc = strBaseRuleDesc;
                logic.intActive = intActive;


                switch (intActionName)
                {
                    case ACT_GET_RULE:
                        DtResult = logic.ExecuteLogic(this.DbAccessObject) as DataTable;
                        break;
                    case ACT_IST_RULE:
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_UPD_RULE:
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_DEL_RULE:
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_EXISTS_NAME:
                        resultYesNo = (bool)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACT_DOUBLE_NAME:
                        resultYesNo = (bool)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                }
            }
            return resultCode;
        }
    }
}
