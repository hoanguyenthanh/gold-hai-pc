﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GoldManager.Logic.Common;
using System.Configuration;
using System.Data;

namespace GoldManager.Controllers.Common
{
    public abstract class BaseProcess : IDisposable
    {
        //private const string _connectionString = @"Data Source=192.168.1.96;Initial Catalog=BH_HAI; Integrated Security=True";
        private const string _connectionString = @"Data Source=127.0.0.1;Initial Catalog=HAIHOA; User Id=sa;Password=123456";

        protected int resultCode = BaseLogic.LOGIC_SUCCESS;

        protected SqlConnection DbAccessObject = null;
        
        protected abstract int Execute();

        public void createConnect()
        {
            DbAccessObject = new SqlConnection(_connectionString);
        }

        public SqlConnection getConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public SqlCommand getCammandStore()
        {
            if (DbAccessObject == null)
            {
                DbAccessObject = getConnection();
            }
            
            SqlCommand cmd = DbAccessObject.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            return cmd;
        }

        public void open()
        {
            if (this.DbAccessObject != null)
            {
                this.DbAccessObject.Open();
            }
        }

        public void close()
        {   
            if (this.DbAccessObject != null)
            {
                this.DbAccessObject.Close();
            }
        }


        public static object toDBNull(object value)
        {
            if (null != value)
                return value;
            return DBNull.Value;
        }

        public void Dispose()
        {
            DbAccessObject = null;
        }

        public int ExecuteProcess()
        {
            NTH.Framework.Log.IDelivLog logOut = NTH.Framework.Log.LogFactory.CreateLog();

            try
            {
                resultCode = BaseLogic.LOGIC_SUCCESS;
                DbAccessObject = new SqlConnection(_connectionString);

                DbAccessObject.Open();

                resultCode = this.Execute();

                DbAccessObject.Close();
            }
            catch (SqlException sqlEx)
            {
                logOut.Error(sqlEx.Message);
                resultCode = BaseLogic.LOGIC_ERR_UNKNOWN;
            }
            catch (Exception exOrther)
            {
                logOut.Error(exOrther.Message);
                resultCode = BaseLogic.LOGIC_ERR_UNKNOWN;
            }
            finally
            {
                this.DbAccessObject = null;
            }
            return resultCode;
        }
    }
}