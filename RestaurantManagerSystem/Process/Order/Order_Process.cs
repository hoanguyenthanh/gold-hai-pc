﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Controllers.Common;
using System.Data;
using GoldManager.Logic;
using System.Data.SqlClient;
using GoldManager.Models;
using GoldManager.Res;

namespace GoldManager.Process
{
    public class Order_Process : BaseProcess
    {
        public long OrderId { get; set; }
        public string TypeTrans { get; set; }


        protected override int Execute()
        {
            return 1;
        }

        public DataTable getOrders(DateTime fromDate, DateTime toDate, string typeInvoice, string typeTrans){

            DataTable table = new DataTable();
            SqlConnection myConn = null;
            SqlCommand myCommand = null;

            try
            {
                myConn = getConnection();
                myCommand = myConn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = StoreProcedureName.WS_ORDER_Get_List;

                myCommand.Parameters.Add("@p_FromDate", System.Data.SqlDbType.DateTime).Value = fromDate;
                myCommand.Parameters.Add("@p_ToDate", System.Data.SqlDbType.DateTime).Value = toDate;
                myCommand.Parameters.Add("@p_TypeInvoice", SqlDbType.VarChar, 50).Value = toDBNull(typeInvoice);
                myCommand.Parameters.Add("@p_TypeTrans", SqlDbType.VarChar, 50).Value = toDBNull(typeTrans);

                SqlDataAdapter adapter = new SqlDataAdapter(myCommand);

                myConn.Open();

                adapter.Fill(table);

            } catch (Exception e)
            {
                Console.WriteLine("Lỗi exception: e = " + e.Message);
            } finally {
                if (myCommand != null)
                {
                    myCommand.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }
            return table;
        }


        public DataTable getOrder(long _orderId)
        {
            DataTable data = new DataTable();

            SqlConnection myConn = null;
            SqlCommand myCommand = null;

            try
            {
                myConn = getConnection();
                myCommand = myConn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = StoreProcedureName.WS_ORDER_Get;

                myCommand.Parameters.Add("@p_OrderId", System.Data.SqlDbType.BigInt).Value = _orderId;

                SqlDataAdapter adapter = new SqlDataAdapter(myCommand);

                myConn.Open();
                adapter.Fill(data);
            }
            catch (Exception e)
            {
                Console.WriteLine("Lỗi exception: e = " + e.Message);
            }
            finally
            {
                if (myCommand != null)
                {
                    myCommand.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }

            return data;
        }


        public DataTable getOrderDetail(long _orderId){
            SqlConnection myConn = null;
            SqlCommand myCmd = null;
            DataTable table = new DataTable();

            try{
                StringBuilder strSql = new StringBuilder();
                strSql.AppendLine("SELECT");
                strSql.AppendLine("     ordDT.OrderDetailId");
                strSql.AppendLine("     ,ordDT.OrderId");
                strSql.AppendLine("     ,ordDT.ModUser");
                strSql.AppendLine("     ,ordDT.ModDt");
                strSql.AppendLine("     ,ordDT.Shared");
                strSql.AppendLine("     ,ordDT.TrnID");
                strSql.AppendLine("     ,ordDT.ProductCode");
                strSql.AppendLine("     ,ordDT.ProductDesc");

                strSql.AppendLine("     ,ordDT.DiamondWeight");
                strSql.AppendLine("     ,ordDT.TotalWeightDT");
                strSql.AppendLine("     ,ISNULL(ordDT.TotalWeightDT, 0) - ISNULL(ordDT.DiamondWeight, 0)  AS GoldWeightDT");
                strSql.AppendLine("     ,ordDT.TotalWeightDTDesc");
                
                strSql.AppendLine("     ,ordDT.PayBonus");
                strSql.AppendLine("     ,ordDT.PriceDT");
                strSql.AppendLine("     ,ordDT.Salary");
                strSql.AppendLine("     ,ordDT.AmountDT");

                strSql.AppendLine("     ,grp.GroupName");
                strSql.AppendLine("     ,typ.GoldDesc");
                strSql.AppendLine("     ,ordDT.Depot");
                strSql.AppendLine("     ,CASE ordDT.TypeTrans");
                strSql.AppendLine("         WHEN 'sell' THEN N'Bán'");
                strSql.AppendLine("         WHEN 'buy' THEN N'Mua'");
                strSql.AppendLine("      END AS TypeTransNameDT");

                strSql.AppendLine(" FROM WS_ORDER_DT ordDT");
                strSql.AppendLine("     LEFT JOIN WS_TYPE typ");
                strSql.AppendLine("         ON ordDT.GoldType = typ.GoldCode");
                strSql.AppendLine("     LEFT JOIN WS_GROUP grp");
                strSql.AppendLine("         ON ordDT.GroupID = grp.GroupID");
                strSql.AppendLine(" WHERE ordDT.OrderId = " + _orderId);
                strSql.AppendLine(" ORDER BY ordDT.TypeTrans");
                

                myConn = getConnection();
                myCmd = myConn.CreateCommand();
                myCmd.CommandText = strSql.ToString();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = myCmd;

                myConn.Open();
                adapter.Fill(table);

            } catch (Exception e)
            {
                table = null;
            } finally {
                if (myCmd != null)
                {
                    myCmd.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }

            return table;
        }

        public ResultObject checkPrinterWaitting()
        {
            ResultObject resultObj = new ResultObject();

            SqlConnection myConn = null;
            SqlCommand myCmd = null;

            try{
                DataTable table = new DataTable();

                myConn = getConnection();
                myCmd = myConn.CreateCommand();
                myCmd.CommandText = "SELECT OrderId, TypeTrans FROM ws_order WHERE IsPrinter = 1 AND DATEADD(SECOND, 30, SendPrintDate) > GETDATE()";
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = myCmd;

                myConn.Open();
                adapter.Fill(table);

                if (table != null && table.Rows.Count > 0){
                    ConvertDataRow cr = new ConvertDataRow(table.Rows[0]);
                    this.OrderId = cr.getLong("OrderId");
                    this.TypeTrans = cr.getString("TypeTrans");
                }
            } catch (Exception e)
            {
                resultObj.Result = -1;
                resultObj.ErrorDesc = "Lỗi Exception e = " + e.Message;
            } finally {
                if (myCmd != null)
                {
                    myCmd.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }

            return resultObj;
        }

        public ResultObject updateInvoicePrinted(long orderID)
        {
            ResultObject resultObj = new ResultObject();

            SqlConnection myConn = null;
            SqlCommand myCmd = null;

            try{
                myConn = getConnection();
                myCmd = myConn.CreateCommand();
                myCmd.CommandText = "UPDATE WS_ORDER SET IsPrinter = 0, SendPrintDate = null WHERE OrderId = " + orderID;

                myConn.Open();
                resultObj.Result = myCmd.ExecuteNonQuery();

            } catch (Exception e)
            {
                resultObj.Result = -1;
                resultObj.ErrorDesc = "Lỗi Exception e = " + e.Message;
            } finally {
                if (myCmd != null)
                {
                    myCmd.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }

            return resultObj;
        }


        public ResultObject deleteOrderDT(long orderDetailId)
        {
            ResultObject resultObj = new ResultObject();
            SqlCommand myCommand = null;
            SqlConnection myConnection = null;

            try{
                myConnection = getConnection();
                myCommand = myConnection.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = StoreProcedureName.WS_ORDER_DT_Delete;

                myCommand.Parameters.Add("@p_OrderDetailId", SqlDbType.BigInt).Value = orderDetailId;
                myCommand.Parameters.Add("@p_Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                myCommand.Parameters.Add("@p_ErrorDesc", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                myConnection.Open();

                myCommand.ExecuteNonQuery();

                ConvertParameters cp = new ConvertParameters(myCommand.Parameters);
                resultObj.Result = cp.getInt("@p_Result");
                resultObj.ErrorDesc = cp.getString("@p_ErrorDesc");

            } catch (Exception e)
            {
                resultObj.Result = -1;
                resultObj.ErrorDesc = "Lỗi Exception: e = " + e.Message;
            } finally {
                if (myCommand != null)
                {
                    myCommand.Dispose();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                    myConnection.Dispose();
                }
            }

            return resultObj;
        }


        public ResultSaleBuy updateOrderMaster(OrderData data)
        {
            ResultSaleBuy result = new ResultSaleBuy();

            SqlConnection myConnection = null;
            SqlCommand myCommand = null;

            try
            {

                myConnection = getConnection();
                myCommand = myConnection.CreateCommand();

                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = StoreProcedureName.WS_ORDER_DT_Update;

                myCommand.Parameters.Add("@p_Method", SqlDbType.VarChar, 10).Value = toDBNull(data.Method);

                myCommand.Parameters.Add("@p_OrderId", SqlDbType.BigInt).Value = toDBNull(data.OrderId);
                myCommand.Parameters.Add("@p_OrderDetailId", SqlDbType.BigInt).Value = toDBNull(data.OrderDetailId);
                myCommand.Parameters.Add("@p_ProductId", SqlDbType.BigInt).Value = toDBNull(data.ProductId);

                myCommand.Parameters.Add("@p_TypeTrans", SqlDbType.VarChar, 20).Value = toDBNull(data.TypeTrans);
                myCommand.Parameters.Add("@p_ActionTrans", SqlDbType.VarChar, 20).Value = toDBNull(data.ActionTrans);
                myCommand.Parameters.Add("@p_ModUser", SqlDbType.VarChar, 30).Value = toDBNull(data.ModUser);
                myCommand.Parameters.Add("@p_TrnID", SqlDbType.VarChar, 30).Value = toDBNull(data.TrnID);

                myCommand.Parameters.Add("@p_ProductCode", SqlDbType.VarChar, 30).Value = toDBNull(data.ProductCode);
                myCommand.Parameters.Add("@p_ProductDesc", SqlDbType.NVarChar, 500).Value = toDBNull(data.ProductDesc);

                myCommand.Parameters.Add("@p_DiamondWeight", SqlDbType.Float).Value = toDBNull(data.DiamondWeight);
                myCommand.Parameters.Add("@p_TotalWeightDT", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT);
                myCommand.Parameters.Add("@p_TotalWeightDT_L", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_L);
                myCommand.Parameters.Add("@p_TotalWeightDT_C", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_C);
                myCommand.Parameters.Add("@p_TotalWeightDT_P", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_P);
                myCommand.Parameters.Add("@p_TotalWeightDT_Li", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_Li);
                myCommand.Parameters.Add("@p_TotalWeightDTDesc", SqlDbType.VarChar, 20).Value = toDBNull(data.TotalWeightDTDesc);

                myCommand.Parameters.Add("@p_PriceDT", SqlDbType.Float).Value = toDBNull(data.PriceDT);
                myCommand.Parameters.Add("@p_PayBonus", SqlDbType.Float).Value = toDBNull(data.PayBonus);
                myCommand.Parameters.Add("@p_PayBonusAmount", SqlDbType.Float).Value = toDBNull(data.PayBonusAmount);
                myCommand.Parameters.Add("@p_Salary", SqlDbType.Float).Value = toDBNull(data.Salary);
                myCommand.Parameters.Add("@p_AmountDT", SqlDbType.Float).Value = toDBNull(data.AmountDT);

                myCommand.Parameters.Add("@p_Depot", SqlDbType.VarChar, 10).Value = toDBNull(data.Depot);
                myCommand.Parameters.Add("@p_GroupID", SqlDbType.VarChar, 15).Value = toDBNull(data.GroupID);
                myCommand.Parameters.Add("@p_GoldType", SqlDbType.VarChar, 10).Value = toDBNull(data.GoldType);


                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Result = Int32.Parse(reader["Result"].ToString());
                        result.ErrorDesc = (string)reader["ErrorDesc"];
                        result.OrderId = Int64.Parse(reader["OrderId"].ToString());
                        result.OrderDetailId = Int64.Parse(reader["OrderDetailId"].ToString());
                    }
                }

            }
            catch (Exception e)
            {
                result.Result = -1;
                result.ErrorDesc = "Lỗi Exception: e = " + e.Message;

                Console.WriteLine(e.Message);
            }
            finally
            {
                if (myCommand != null)
                {
                    myCommand.Dispose();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                    myConnection.Dispose();
                }
            }

            return result;
        }


        public ResultSaleBuy updateOrderDetail(OrderData data)
        {
            ResultSaleBuy result = new ResultSaleBuy();

            SqlConnection myConnection = null;
            SqlCommand myCommand = null;

            try{

                myConnection = getConnection();
                myCommand = myConnection.CreateCommand();
                
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = StoreProcedureName.WS_ORDER_DT_Update;

                myCommand.Parameters.Add("@p_Method", SqlDbType.VarChar, 10).Value = toDBNull(data.Method);

                myCommand.Parameters.Add("@p_OrderId", SqlDbType.BigInt).Value = toDBNull(data.OrderId);
                myCommand.Parameters.Add("@p_OrderDetailId", SqlDbType.BigInt).Value = toDBNull(data.OrderDetailId);
                myCommand.Parameters.Add("@p_ProductId", SqlDbType.BigInt).Value = toDBNull(data.ProductId);
                
                myCommand.Parameters.Add("@p_TypeTrans", SqlDbType.VarChar, 20).Value = toDBNull(data.TypeTrans);
                myCommand.Parameters.Add("@p_ActionTrans", SqlDbType.VarChar, 20).Value = toDBNull(data.ActionTrans);
                myCommand.Parameters.Add("@p_ModUser", SqlDbType.VarChar, 30).Value = toDBNull(data.ModUser);
                myCommand.Parameters.Add("@p_TrnID", SqlDbType.VarChar, 30).Value = toDBNull(data.TrnID);
                
                myCommand.Parameters.Add("@p_ProductCode", SqlDbType.VarChar, 30).Value = toDBNull(data.ProductCode);
                myCommand.Parameters.Add("@p_ProductDesc", SqlDbType.NVarChar, 500).Value = toDBNull(data.ProductDesc);

                myCommand.Parameters.Add("@p_DiamondWeight", SqlDbType.Float).Value = toDBNull(data.DiamondWeight);
                myCommand.Parameters.Add("@p_TotalWeightDT", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT);
                myCommand.Parameters.Add("@p_TotalWeightDT_L", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_L);
                myCommand.Parameters.Add("@p_TotalWeightDT_C", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_C);
                myCommand.Parameters.Add("@p_TotalWeightDT_P", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_P);
                myCommand.Parameters.Add("@p_TotalWeightDT_Li", SqlDbType.Int).Value = toDBNull(data.TotalWeightDT_Li);
                myCommand.Parameters.Add("@p_TotalWeightDTDesc", SqlDbType.VarChar, 20).Value = toDBNull(data.TotalWeightDTDesc);

                myCommand.Parameters.Add("@p_PriceDT", SqlDbType.Float).Value = toDBNull(data.PriceDT);
                myCommand.Parameters.Add("@p_PayBonus", SqlDbType.Float).Value = toDBNull(data.PayBonus);
                myCommand.Parameters.Add("@p_PayBonusAmount", SqlDbType.Float).Value = toDBNull(data.PayBonusAmount);
                myCommand.Parameters.Add("@p_Salary", SqlDbType.Float).Value = toDBNull(data.Salary);
                myCommand.Parameters.Add("@p_AmountDT", SqlDbType.Float).Value = toDBNull(data.AmountDT);

                myCommand.Parameters.Add("@p_Depot", SqlDbType.VarChar, 10).Value = toDBNull(data.Depot);
                myCommand.Parameters.Add("@p_GroupID", SqlDbType.VarChar, 15).Value = toDBNull(data.GroupID);
                myCommand.Parameters.Add("@p_GoldType", SqlDbType.VarChar, 10).Value = toDBNull(data.GoldType);


                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Result = Int32.Parse(reader["Result"].ToString());
                        result.ErrorDesc = (string)reader["ErrorDesc"];
                        result.OrderId = Int64.Parse(reader["OrderId"].ToString());
                        result.OrderDetailId = Int64.Parse(reader["OrderDetailId"].ToString());
                    }
                }

            } catch (Exception e)
            {
                result.Result = -1;
                result.ErrorDesc = "Lỗi Exception: e = " + e.Message;

                Console.WriteLine(e.Message);            
            } finally {
                if (myCommand != null)
                {
                    myCommand.Dispose();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                    myConnection.Dispose();
                }
            }

            return result;
        }

        
        public bool isProductInOrder(long orderId, string productCode)
        {
            bool isExist = false;

            SqlConnection myConn = null;
            SqlCommand myCmd = null;

            try
            {
                DataTable table = new DataTable();
                myConn = getConnection();
                myCmd = myConn.CreateCommand();
                myCmd.CommandText = "SELECT NULL FROM ws_order_dt WHERE ProductCode = '" + productCode + "' AND OrderId = " + orderId;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = myCmd;

                myConn.Open();
                adapter.Fill(table);

                if (table != null && table.Rows.Count > 0)
                {
                    isExist = true;
                }
            }
            catch (Exception e)
            {
                
            }
            finally
            {
                if (myCmd != null)
                {
                    myCmd.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }

            return isExist;
        }
    }
}
