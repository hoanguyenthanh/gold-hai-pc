﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GoldManager.Controllers.Common;
using GoldManager.Logic;

namespace GoldManager.Process
{
    class RptGetInvoiceList_Process : BaseProcess
    {
        public long OrderId { get; set; }
        public DataTable DtResult { get; set; }

        protected override int Execute()
        {
            using(RptGetInvoiceList_Logic logic = new RptGetInvoiceList_Logic()){
                logic.OrderID = this.OrderId;
                DtResult = (DataTable)logic.ExecuteLogic(DbAccessObject);
            }
            return base.resultCode;
        }
    }
}
