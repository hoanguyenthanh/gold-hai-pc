﻿using GoldManager.Controllers.Common;
using GoldManager.Logic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using GoldManager.Res;
using GoldManager.Models;

namespace GoldManager.Process
{
    public class Product_Process : BaseProcess
    {
        protected override int Execute()
        {
            return 0;
        }


        public Int64 int64ProductId { get; set; }
        public string strGoldCode { get; set; }
        public int intStallsId { get; set; }
        public string strProductDesc { get; set; }
        public string strGroupID { get; set; }
        public double douQuantity { get; set; }
        public double douGoldWeight { get; set; }
        public double douDiamondWeight { get; set; }
        public double douTotalWeight { get; set; }
        public string strKyHieu { get; set; }
        public string strNCC { get; set; }
        public string strBaseRule { get; set; }
        public double douSalary { get; set; }


        public DataRow getProduct(long id){
            DataTable table = new DataTable();

            try{
                DbAccessObject = getConnection();
                DbAccessObject.Open();

                SqlCommand sqlCammand = DbAccessObject.CreateCommand();
                sqlCammand.CommandText = "SELECT * FROM ws_product_in WHERE ProductId = " + id;
                
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCammand;
                adapter.Fill(table);

            } catch (SqlException ex){
                table = null;
            } finally {
                if (DbAccessObject != null)
                {
                    DbAccessObject.Close();
                    DbAccessObject.Dispose();
                }
            }

            if (table != null && table.Rows.Count > 0)
            {
                return table.Rows[0];
            }

            return null;
        }

        public string copyProduct(string productCode)
        {
            string errorDesc = string.Empty;

            try
            {
                SqlCommand cammand = getCammandStore();

                cammand.CommandText = "WS_PRODUCT_IN_CopyData";
                cammand.Parameters.Add("@p_ProductCode", SqlDbType.VarChar, 50).Value = toDBNull(productCode);
                
                open();

                SqlDataReader reader = cammand.ExecuteReader();
                
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int64ProductId = Int64.Parse(reader["_ID"].ToString());
                        errorDesc = (string)reader["ErrorDesc"];

                    }
                }
            }
            catch (SqlException e)
            {
                errorDesc = string.Empty;
            }
            finally
            {
                close();
            }

            return errorDesc;
        }

        public int insertProduct()
        {
            int affected = -1;

            try{
                SqlCommand cammand = getCammandStore();
                cammand.CommandText = StoreProcedureName.WS_PRODUCT_IN_Insert;

                cammand.Parameters.Add("@p_GoldCode", SqlDbType.VarChar, 50).Value =  toDBNull(strGoldCode);
                cammand.Parameters.Add("@p_StallsId", SqlDbType.Int).Value = intStallsId;
                cammand.Parameters.Add("@p_ProductDesc", SqlDbType.NVarChar, 500).Value = toDBNull(strProductDesc);
                cammand.Parameters.Add("@p_GroupID", SqlDbType.VarChar, 50).Value = toDBNull(strGroupID);
                cammand.Parameters.Add("@p_Quantity", SqlDbType.Decimal).Value = douQuantity;
                cammand.Parameters.Add("@p_GoldWeight", SqlDbType.Decimal).Value = douGoldWeight;
                cammand.Parameters.Add("@p_DiamondWeight", SqlDbType.Decimal).Value = douDiamondWeight;
                cammand.Parameters.Add("@p_TotalWeight", SqlDbType.Decimal).Value = douTotalWeight;
                cammand.Parameters.Add("@p_KyHieu", SqlDbType.VarChar, 15).Value = toDBNull(strKyHieu);
                cammand.Parameters.Add("@p_NCC", SqlDbType.NVarChar, 50).Value = toDBNull(strNCC);
                cammand.Parameters.Add("@p_BaseRule", SqlDbType.VarChar, 50).Value = toDBNull(strBaseRule);
                cammand.Parameters.Add("@p_Salary", SqlDbType.Decimal).Value = toDBNull(douSalary);

                open();
                SqlDataReader reader = cammand.ExecuteReader();
                if (reader.HasRows)
                {
                    while(reader.Read())
                    {
                        int64ProductId = (long)reader["_ID"];
                        affected = (int)reader["Result"];
                    }
                }
            } catch (SqlException e)
            {
                affected = -1;
            } finally
            {
                close();
            }

            return affected;
        }


        public int updateProduct(Int64 productID)
        {
            int affected = -1;
            try{
                SqlCommand cammand = getCammandStore();
                cammand.CommandText = StoreProcedureName.WS_PRODUCT_IN_Update;

                cammand.Parameters.Add("@p_ProductId", SqlDbType.BigInt).Value = productID;
                cammand.Parameters.Add("@p_GoldCode", SqlDbType.VarChar, 50).Value =  toDBNull(strGoldCode);
                cammand.Parameters.Add("@p_StallsId", SqlDbType.Int).Value = intStallsId;
                cammand.Parameters.Add("@p_ProductDesc", SqlDbType.NVarChar, 500).Value = toDBNull(strProductDesc);
                cammand.Parameters.Add("@p_GroupID", SqlDbType.VarChar, 50).Value = toDBNull(strGroupID);
                cammand.Parameters.Add("@p_Quantity", SqlDbType.Decimal).Value = douQuantity;
                cammand.Parameters.Add("@p_GoldWeight", SqlDbType.Decimal).Value = douGoldWeight;
                cammand.Parameters.Add("@p_DiamondWeight", SqlDbType.Decimal).Value = douDiamondWeight;
                cammand.Parameters.Add("@p_TotalWeight", SqlDbType.Decimal).Value = douTotalWeight;
                cammand.Parameters.Add("@p_KyHieu", SqlDbType.VarChar, 15).Value = toDBNull(strKyHieu);
                cammand.Parameters.Add("@p_NCC", SqlDbType.NVarChar, 50).Value = toDBNull(strNCC);
                cammand.Parameters.Add("@p_BaseRule", SqlDbType.VarChar, 50).Value = toDBNull(strBaseRule);
                cammand.Parameters.Add("@p_Salary", SqlDbType.Decimal).Value = toDBNull(douSalary);

                open();
                SqlDataReader reader = cammand.ExecuteReader();
                if (reader.HasRows)
                {
                    while(reader.Read())
                    {
                        affected = (int)reader["Result"];
                    }
                }
            } catch (SqlException e)
            {
                affected = -1;
            } finally
            {
                close();
            }

            return affected;
        }

        public int deleteProduct(Int64 id)
        {
            int affected = -1;
            try
            {
                createConnect();
                SqlCommand cmd = getCammandStore();
                cmd.CommandText = StoreProcedureName.WS_PRODUCT_IN_Delete;
                cmd.Parameters.Add("@p_ProductId", SqlDbType.BigInt).Value = id;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                open();
                cmd.ExecuteNonQuery();

                affected = (int)cmd.Parameters["@Result"].Value;
            } catch (SqlException e)
            {
                affected = -1;
            } finally
            {
                close();
            }

            return affected;
        }

        public DataTable getListProduct (int limitRow)
        {
            DataTable result = new DataTable();
            SqlConnection conn = null;
            SqlCommand comd = null;

            try
            {
                conn = getConnection();

                comd = conn.CreateCommand();
                comd.CommandText = sqlGetProducts(0, limitRow);

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = comd;
                adapter.Fill(result);

            } catch (Exception e) {
                Console.WriteLine(e.Message);
            } finally
            {
                if (comd  != null)
                {
                    comd.Dispose();
                }

                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;
        }


        public DataTable searchProduct(string goldType, string productCode, string productDesc)
        {
            DataTable table = new DataTable();

            SqlConnection connection = null;
            SqlCommand command = null;

            try{
                StringBuilder sql = new StringBuilder();

                sql.AppendLine("SELECT TOP 1000 ");
                sql.AppendLine("    prodin.ProductId");
                sql.AppendLine("   ,prodin.GoldCode");
                sql.AppendLine("   ,typ.GoldDesc");
                sql.AppendLine("   ,typ.PriceSell");
                sql.AppendLine("   ,typ.PriceBuy");
                sql.AppendLine("   ,prodin.StallsId");
                sql.AppendLine("   ,prodin.ProductDesc");
                sql.AppendLine("   ,prodin.GroupID");
                sql.AppendLine("   ,grp.GroupName");
                sql.AppendLine("   ,prodin.Quantity");
                sql.AppendLine("   ,prodin.GoldWeight");
                sql.AppendLine("   ,prodin.DiamondWeight");
                sql.AppendLine("   ,prodin.TotalWeight");
                sql.AppendLine("   ,prodin.KyHieu");
                sql.AppendLine("   ,prodin.NCC");
                sql.AppendLine("   ,CONVERT(varchar(20), prodin.ModDt, 105) AS _date");
                sql.AppendLine("   ,CONVERT(varchar(20), prodin.ModDt, 108) AS _time");
                sql.AppendLine("   ,prodin.PrintYN");
                sql.AppendLine("   ,CASE ISNULL(prodin.PrintYN, 0) WHEN 1 THEN 'Yes' ELSE 'No' END AS PrintYNDesc");
                sql.AppendLine("   ,prodin.BaseRule");
                sql.AppendLine("   ,prodin.ProductCode");

                sql.AppendLine("   ,prodin.[ProductCode_Old] ");
                sql.AppendLine("   ,prodin.[TrnID_Old] ");
                sql.AppendLine("   ,prodin.[SectionID_Old] ");
                sql.AppendLine("   ,prodin.[TotalWeight_Old] ");
                sql.AppendLine("   ,prodin.[GoldlWeight_Old] ");
                sql.AppendLine("   ,prodin.[DiamondWeight_Old] ");

                sql.AppendLine(" FROM WS_PRODUCT_IN prodin");
                sql.AppendLine("    LEFT JOIN WS_TYPE typ");
                sql.AppendLine("        ON typ.GoldCode = prodin.GoldCode");
                sql.AppendLine("    LEFT JOIN WS_GROUP grp");
                sql.AppendLine("        ON grp.GroupID = prodin.GroupID");
                sql.AppendLine(" WHERE 1 = 1 ");

                if (!string.IsNullOrEmpty(goldType))
                    sql.AppendLine("    AND prodin.GoldCode LIKE " + string.Format("'%{0}%'", goldType));

                if (!string.IsNullOrEmpty(productCode))
                    sql.AppendLine("    AND prodin.ProductCode LIKE " + string.Format("'%{0}%'", productCode));
                
                if (!string.IsNullOrEmpty(productDesc))
                    sql.AppendLine("    AND prodin.ProductDesc LIKE " + string.Format("'%{0}%'", productDesc));

                sql.AppendLine(" ORDER BY prodin.ProductCode, prodin.ProductDesc DESC");


                connection = getConnection();
                

                command = connection.CreateCommand();
                command.CommandText = sql.ToString();
                
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(table);

            } catch (Exception e)
            {
                table = null;
                Console.WriteLine(e);
            } finally 
            {
                if (command != null)
                {
                    command.Dispose();
                }

                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }

            return table;
        }

        public Product getProductByCode (string productCode)
        {
            Product product = new Product();
            SqlConnection myConn = null;
            SqlCommand myCommand = null;

            try{

                StringBuilder sql = new StringBuilder();

                sql.AppendLine("SELECT ");
                sql.AppendLine("    prodin.ProductId");
                sql.AppendLine("   ,prodin.ProductCode");
                sql.AppendLine("   ,prodin.StallsId");
                sql.AppendLine("   ,prodin.GoldCode");
                sql.AppendLine("   ,prodin.ProductDesc");
                sql.AppendLine("   ,prodin.GroupID");
                sql.AppendLine("   ,prodin.Quantity");
                
                sql.AppendLine("   ,prodin.GoldWeight");
                sql.AppendLine("   ,prodin.DiamondWeight");
                sql.AppendLine("   ,prodin.TotalWeight");
                sql.AppendLine("   ,prodin.TotalWeightRounding");
                sql.AppendLine("   ,prodin.StampWeight");
                
                sql.AppendLine("   ,prodin.KyHieu");
                sql.AppendLine("   ,prodin.NCC");

                sql.AppendLine("   ,prodin.Salary");
                sql.AppendLine("   ,prodin.BaseRule");
                

                sql.AppendLine("   ,prodin.[ProductCode_Old] ");
                sql.AppendLine("   ,prodin.[TrnID_Old] ");
                sql.AppendLine("   ,prodin.[SectionID_Old] ");
                sql.AppendLine("   ,prodin.[TotalWeight_Old] ");
                sql.AppendLine("   ,prodin.[GoldlWeight_Old] ");
                sql.AppendLine("   ,prodin.[DiamondWeight_Old] ");

                sql.AppendLine(" FROM WS_PRODUCT_IN prodin");
                sql.AppendLine(" WHERE 1 = 1 ");
                sql.AppendLine("    AND prodin.ProductCode = " + string.Format("'{0}'", productCode));

                myConn = getConnection();

                myCommand = myConn.CreateCommand();
                myCommand.CommandText = sql.ToString();

                myConn.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = myCommand;


                DataTable table = new DataTable();
                adapter.Fill(table);

                if (table != null && table.Rows.Count > 0)
                {
                    DataRow row = table.Rows[0];
                    ConvertDataRow convert = new ConvertDataRow(row);

                    product.ProductId = convert.getLong("ProductId");
                    product.ProductCode = convert.getString("ProductCode");
                    product.StallsId = convert.getInt("StallsId");
                    product.GoldCode = convert.getString("GoldCode");
                    product.ProductDesc = convert.getString("ProductDesc");
                    product.GroupID = convert.getString("GroupID");
                    product.Quantity = convert.getDecimal("Quantity");
                    product.GoldWeight = convert.getDecimal("GoldWeight");
                    product.DiamondWeight = convert.getDecimal("DiamondWeight");
                    product.TotalWeight = convert.getDecimal("TotalWeight");
                    product.TotalWeightRounding = convert.getInt("TotalWeightRounding");
                    product.StampWeight = convert.getDecimal("StampWeight");

                    product.KyHieu = convert.getString("KyHieu");
                    product.NCC = convert.getString("NCC");
                    product.Salary = convert.getLong("Salary");
                    product.BaseRule = convert.getString("BaseRule");
                    
                    product.ProductCode_Old = convert.getString("ProductCode_Old");
                    product.TrnID_Old = convert.getString("TrnID_Old");
                    product.SectionID_Old = convert.getString("SectionID_Old");
                    product.TotalWeight_Old = convert.getDecimal("TotalWeight_Old");
                    product.GoldlWeight_Old = convert.getDecimal("GoldlWeight_Old");
                    product.DiamondWeight_Old = convert.getDecimal("DiamondWeight_Old");
                }

            } catch (Exception e)
            {
                product = null;
                Console.WriteLine(e.Message);
            } finally {
                if (myCommand != null)
                {
                    myCommand.Dispose();
                }

                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
            }


            return product;
        }

        private string sqlGetProducts(Int64 productID, int limit)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT ");
            if (limit > 0 && productID == 0)
            {
                sql.AppendLine(" TOP " + limit + " ");
            }
            sql.AppendLine("    prodin.ProductId");
            sql.AppendLine("   ,prodin.GoldCode");
            sql.AppendLine("   ,typ.GoldDesc");
            sql.AppendLine("   ,prodin.StallsId");
            sql.AppendLine("   ,CASE ISNULL(prodin.StallsId, 0)");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_01) + " THEN N'" + MConts.STALLS_01_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_02) + " THEN N'" + MConts.STALLS_02_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_03) + " THEN N'" + MConts.STALLS_03_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_04) + " THEN N'" + MConts.STALLS_04_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_05) + " THEN N'" + MConts.STALLS_05_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_06) + " THEN N'" + MConts.STALLS_06_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_07) + " THEN N'" + MConts.STALLS_07_DESC + "'");
            sql.AppendLine("        WHEN " + int.Parse(MConts.STALLS_08) + " THEN N'" + MConts.STALLS_08_DESC + "'");
            sql.AppendLine("        ELSE ''");
            sql.AppendLine("    END AS StallsDesc");
            sql.AppendLine("   ,prodin.ProductDesc");
            sql.AppendLine("   ,prodin.GroupID");
            sql.AppendLine("   ,grp.GroupName");
            sql.AppendLine("   ,prodin.Quantity");
            sql.AppendLine("   ,prodin.GoldWeight");
            sql.AppendLine("   ,prodin.DiamondWeight");
            sql.AppendLine("   ,prodin.TotalWeight");
            sql.AppendLine("   ,prodin.Salary");
            sql.AppendLine("   ,prodin.KyHieu");
            sql.AppendLine("   ,prodin.NCC");
            sql.AppendLine("   ,CONVERT(varchar(20), prodin.ModDt, 105) AS _date");
            sql.AppendLine("   ,CONVERT(varchar(20), prodin.ModDt, 108) AS _time");
            sql.AppendLine("   ,prodin.PrintYN");
            sql.AppendLine("   ,CASE ISNULL(prodin.PrintYN, 0) WHEN 1 THEN 'Yes' ELSE 'No' END AS PrintYNDesc");
            sql.AppendLine("   ,prodin.BaseRule");
            sql.AppendLine("   ,prodin.ProductCode");

            sql.AppendLine("   ,prodin.[ProductCode_Old] ");
            sql.AppendLine("   ,prodin.[TrnID_Old] ");
            sql.AppendLine("   ,prodin.[SectionID_Old] ");
            sql.AppendLine("   ,prodin.[TotalWeight_Old] ");
            sql.AppendLine("   ,prodin.[GoldlWeight_Old] ");
            sql.AppendLine("   ,prodin.[DiamondWeight_Old] ");

            sql.AppendLine(" FROM WS_PRODUCT_IN prodin");
            sql.AppendLine("    LEFT JOIN WS_TYPE typ");
            sql.AppendLine("        ON typ.GoldCode = prodin.GoldCode");
            sql.AppendLine("    LEFT JOIN WS_GROUP grp");
            sql.AppendLine("        ON grp.GroupID = prodin.GroupID");

            if (productID > 0)
            {
                sql.AppendLine(" WHERE prodin.ProductId = " + productID);
            }
            else
            {
                sql.AppendLine(" ORDER BY prodin.ProductId DESC");
            }

            return sql.ToString();
        }


        public int updatePrinting(string productCode)
        {
            int affected = -1;
            SqlConnection conn = null;
            try
            {
                conn = getConnection();
                SqlCommand comd = conn.CreateCommand();
                comd.CommandText = "UPDATE ws_product_in SET Printing = 1 WHERE ProductCode = '" + productCode + "'";

                conn.Open();
                affected = comd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                affected = - 1;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return affected;
        }
    }
}
