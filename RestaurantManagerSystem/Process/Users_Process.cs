﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Controllers.Common;
using System.Data;
using GoldManager.Logic;

namespace GoldManager.Process
{
    public class Users_Process : BaseProcess
    {
        public const int ACT_GET_USER = 1;
        public const int ACT_DEL_USER = 2;
        public const int ACT_IST_USER = 3;
        public const int ACT_UPD_USER = 4;
        public const int ACT_LOGIN = 5;
        public const int ACT_CHECK_EXIST_USER = 6;

        public int intActionName {get; set;}
        public DataTable DtResult { get; set; }

        public int intUserId { get; set; }
        public string strUserName { get; set; }
        public string strPassword { get; set; }
        public string strFullName { get; set; }
        public string strMobile { get; set; }
        public int intIsAdmin { get; set; }
        public int intActive { get; set; }

        protected override int Execute()
        {
            DtResult = new DataTable();
            switch (intActionName)
            {
                case ACT_GET_USER:
                    DtResult = this.getUsers();
                    break;
                case ACT_DEL_USER:
                    resultCode = deleteUser(this.intUserId);
                    break;
                case ACT_IST_USER:
                    resultCode = insertUser();
                    break;
                case ACT_UPD_USER:
                    resultCode = updateUser();
                    break;
                case ACT_LOGIN:
                    DtResult = getUsers(strUserName, strPassword);
                    break;
                case ACT_CHECK_EXIST_USER:
                    resultCode = checkExistUser();
                    break;
            }

            return resultCode;
        }

        private DataTable getUsers(){
            DataTable table = null;
            using (GetUsers_Logic logic = new GetUsers_Logic())
            {
                table = logic.ExecuteLogic(this.DbAccessObject) as DataTable;
            }
            return table;
        }

        private int deleteUser(int userId){
            int iResult = 0;
            using (DeleteUser_Logic logic = new DeleteUser_Logic())
            {
                logic.intUserID = userId;
                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }

        private int insertUser(){
            int iResult = 0;
            using (InsertUser_Logic logic = new InsertUser_Logic())
            {
                logic.strUserName = this.strUserName == null ? "" : this.strUserName;
                logic.strPassword = this.strPassword == null ? "" : this.strPassword;
                logic.strFullName = this.strFullName == null ? "" : this.strFullName;
                logic.strMobile = this.strMobile == null ? "" : this.strMobile;
                logic.intActive = this.intActive;
                logic.intIsAdmin = this.intIsAdmin;

                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }

        private int updateUser()
        {
            int iResult = 0;
            using (UpdateUser_Logic logic = new UpdateUser_Logic())
            {
                logic.strUserName = this.strUserName;

                logic.strFullName = this.strFullName == null ? "" : this.strFullName;
                logic.strMobile = this.strMobile == null ? "" : this.strMobile;
                logic.strPwd = this.strPassword == null ? "" : this.strPassword;
                logic.intIsAdmin = this.intIsAdmin;
                logic.intActive = this.intActive;

                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }

        private DataTable getUsers(string userName, string pwd){
            DataTable table = new DataTable();
            using(Login_Logic logic = new Login_Logic()){
                logic.strUserName = userName;
                logic.strPwd = pwd;
                table = (DataTable)logic.ExecuteLogic(this.DbAccessObject);
            }
            return table;
        }

        private int checkExistUser(){
            using(CheckExistUsers_Logic logic = new CheckExistUsers_Logic()){
                logic.strUserName = this.strUserName;
                DataTable dt = (DataTable)logic.ExecuteLogic(this.DbAccessObject);
                if (dt != null && dt.Rows.Count > 0){
                    return 1;
                }
            }
            return 0;
        }
    }
}
