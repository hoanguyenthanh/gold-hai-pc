﻿using GoldManager.Controllers.Common;
using GoldManager.Logic;
using System.Data;

namespace GoldManager.Process
{
    public class Printer_Process : BaseProcess
    {
        public const int ACTION_DEL = 1;
        public const int ACTION_IST = 2;
        public const int ACTION_SEL = 3;

        public int ActionName { get; set; }
        public DataTable DtResult { get; set; }
        public string strPrintName { get; set; }

        protected override int Execute()
        {
            switch(ActionName)
            {
                case ACTION_SEL:
                    DtResult = this.getPrinters();
                    break;
                case ACTION_IST:
                    resultCode = this.setPrinterDetault(this.strPrintName);
                    break;
                case ACTION_DEL:
                    resultCode = this.deletePrinter();
                    break;
            }
            return resultCode;
        }

        private DataTable getPrinters(){
            DataTable dtResult = new DataTable();
            using (SelectPrinter_Logic logic = new SelectPrinter_Logic()){
                dtResult = (DataTable)logic.ExecuteLogic(this.DbAccessObject);
            }
            return dtResult;
        }

        private int setPrinterDetault(string prName){
            int iResult = 0;
            using(InsertPrinter_Logic logic = new InsertPrinter_Logic()){
                logic.printerName = prName;
                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }

        private int deletePrinter(){
            int iResult = 0;
            using(DeletePrinter_Logic logic = new DeletePrinter_Logic()){
                iResult = (int)logic.ExecuteLogic(this.DbAccessObject);
            }
            return iResult;
        }
    }
}
