﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Controllers.Common;
using GoldManager.Logic;
using System.Data;

namespace GoldManager.Process
{
    public class COM_Process : BaseProcess
    {
        public const int ACTION_INSERT = 1;
        public const int ACTION_SELECT = 2;
        public const int ACTION_DELETE = 3;

        public int ActionName { get; set; }
        public DataTable DtResult { get; set; }
        public string strName { get; set; }

        protected override int Execute()
        {
            using (COM_Logic logic = new COM_Logic())
            {
                logic.strName = strName;

                switch (ActionName)
                {
                    case ACTION_SELECT:
                        logic.ActionName = ACTION_SELECT;
                        DtResult = (DataTable)logic.ExecuteLogic(this.DbAccessObject);
                        resultCode = DtResult.Rows.Count;
                        break;
                    case ACTION_INSERT:
                        logic.ActionName = ACTION_INSERT;
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                    case ACTION_DELETE:
                        logic.ActionName = ACTION_DELETE;
                        resultCode = (int)logic.ExecuteLogic(this.DbAccessObject);
                        break;
                }
            }
            return resultCode;
        }
    }
}
