﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoldManager.Models;
using System.Data;
using GoldManager.Controllers.Common;
using System.Data.SqlClient;

namespace GoldManager.Process
{
    class Customer_Process : BaseProcess
    {
        protected SqlCommand command;
        protected SqlDataAdapter adapter;

        public DataTable getCustomers()
        {
            DataTable data = new DataTable();
            
            try{
                createConnect();

                command = DbAccessObject.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT CusId, CusCode, CusName, Phone, [Address], CMND, Note  FROM WS_CUSTOMER";
                
                adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                
                adapter.Fill(data);

            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            } finally 
            {
                Dispose();
            }           
            return data;
        }

        public ResultObject updateCus(string method, Customer cus)
        {
            ResultObject result = new ResultObject();

            try
            {
                createConnect();
                DbAccessObject.Open();

                command = DbAccessObject.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "WS_CUSTOMER_Update";

                command.Parameters.Add("@p_Method", SqlDbType.NVarChar, 10).Value = method;
                command.Parameters.Add("@p_CusId",SqlDbType.Int).Value = cus.CusId;
                command.Parameters.Add("@p_CusCode", SqlDbType.NVarChar, 10).Value = toDBNull(cus.CusCode);
                command.Parameters.Add("@p_CusName", SqlDbType.NVarChar, 50).Value = toDBNull(cus.CusName);
                command.Parameters.Add("@p_Address", SqlDbType.NVarChar, 100).Value = toDBNull(cus.Address);
                command.Parameters.Add("@p_Phone", SqlDbType.VarChar, 15).Value = toDBNull(cus.Phone);
                command.Parameters.Add("@p_CMND", SqlDbType.VarChar, 15).Value = toDBNull(cus.CMND);
                command.Parameters.Add("@p_Note", SqlDbType.NVarChar, 500).Value = toDBNull(cus.Note);

                command.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                command.Parameters.Add("@ErrorDesc", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

                command.ExecuteNonQuery();

                result.Result = (int)command.Parameters["@Result"].Value;
                result.ErrorDesc = (string)command.Parameters["@ErrorDesc"].Value;
            }
            catch (Exception e)
            {
                result.Result = -1;
                result.ErrorDesc = "Lỗi exception: e = " + e.Message; 
                Console.WriteLine(e.Message);
            }
            finally
            {
                Dispose();
            }

            return result;
        }


        

        public void Dispose()
        {
            if (command != null)
            {
                command.Dispose();
            }
            
            if (adapter != null)
            {
                adapter.Dispose();
            }

            if (DbAccessObject != null)
            {
                DbAccessObject.Dispose();
            }
        }

        protected override int Execute()
        {
            return 1;
        }
    }   
}