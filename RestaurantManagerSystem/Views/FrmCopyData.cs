﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Process;

namespace GoldManager.Views
{
    public partial class FrmCopyData : Form
    {
        public FrmProductInput.ReloadProduct reloadProduct;
        
        public FrmCopyData()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtBarcode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string strCurrentString = txtBarcode.Text.Trim().ToString();

                if (strCurrentString != "")
                {
                    Console.WriteLine("txtBarcode Value: " + strCurrentString);

                    string errroDesc = string.Empty;
                    using(Product_Process process = new Product_Process())
                    {
                        errroDesc = process.copyProduct(strCurrentString);
                    }

                    if (string.IsNullOrEmpty(errroDesc))
                    {
                        lblStatus.Text = "Sao chép thành công!";
                    } else {
                        lblStatus.Text = errroDesc.Equals("ExistsData") ? "Sản phẩm đã tồn tại" : errroDesc;
                    }

                    if (reloadProduct != null && string.IsNullOrEmpty(errroDesc))
                    {
                        reloadProduct.Invoke(0);   
                    }

                    txtBarcode.Text = "";
                }

                txtBarcode.Focus();
            } 
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
