﻿namespace GoldManager.Views
{
    partial class FrmGoldType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGoldType));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dgvGoldType = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.txtAgeGold = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtNote = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.txtPriceBuy = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.txtPriceSell = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnUpdate = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.btnAdd = new DevComponents.DotNetBar.ButtonX();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.txtGoldDesc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtGoldCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.GoldCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceSell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiveDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoldType)).BeginInit();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            this.labelX1.Location = new System.Drawing.Point(430, 126);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(122, 38);
            this.labelX1.TabIndex = 14;
            this.labelX1.Text = "Tuổi vàng";
            // 
            // dgvGoldType
            // 
            this.dgvGoldType.AllowUserToAddRows = false;
            this.dgvGoldType.AllowUserToDeleteRows = false;
            this.dgvGoldType.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvGoldType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGoldType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGoldType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvGoldType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGoldType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GoldCode,
            this.GoldDesc,
            this.PriceSell,
            this.PriceBuy,
            this.Old,
            this.Notes,
            this.ActiveDesc,
            this.Active});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGoldType.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvGoldType.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvGoldType.Location = new System.Drawing.Point(12, 271);
            this.dgvGoldType.MultiSelect = false;
            this.dgvGoldType.Name = "dgvGoldType";
            this.dgvGoldType.ReadOnly = true;
            this.dgvGoldType.RowHeadersVisible = false;
            this.dgvGoldType.RowTemplate.Height = 45;
            this.dgvGoldType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGoldType.Size = new System.Drawing.Size(1226, 356);
            this.dgvGoldType.TabIndex = 0;
            this.dgvGoldType.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGoldType_CellClick);
            this.dgvGoldType.SelectionChanged += new System.EventHandler(this.dgvGoldType_SelectionChanged);
            // 
            // txtAgeGold
            // 
            // 
            // 
            // 
            this.txtAgeGold.Border.Class = "TextBoxBorder";
            this.txtAgeGold.Location = new System.Drawing.Point(593, 126);
            this.txtAgeGold.Name = "txtAgeGold";
            this.txtAgeGold.Size = new System.Drawing.Size(233, 38);
            this.txtAgeGold.TabIndex = 13;
            this.txtAgeGold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeric_KeyPress);
            // 
            // txtNote
            // 
            // 
            // 
            // 
            this.txtNote.Border.Class = "TextBoxBorder";
            this.txtNote.Location = new System.Drawing.Point(158, 127);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(228, 38);
            this.txtNote.TabIndex = 12;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(1089, 216);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(147, 39);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Bỏ qua";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtPriceBuy
            // 
            // 
            // 
            // 
            this.txtPriceBuy.Border.Class = "TextBoxBorder";
            this.txtPriceBuy.Location = new System.Drawing.Point(593, 72);
            this.txtPriceBuy.Name = "txtPriceBuy";
            this.txtPriceBuy.Size = new System.Drawing.Size(233, 38);
            this.txtPriceBuy.TabIndex = 11;
            this.txtPriceBuy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeric_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(919, 216);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 39);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPriceSell
            // 
            // 
            // 
            // 
            this.txtPriceSell.Border.Class = "TextBoxBorder";
            this.txtPriceSell.Location = new System.Drawing.Point(158, 73);
            this.txtPriceSell.Name = "txtPriceSell";
            this.txtPriceSell.Size = new System.Drawing.Size(228, 38);
            this.txtPriceSell.TabIndex = 10;
            this.txtPriceSell.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeric_KeyPress);
            // 
            // btnUpdate
            // 
            this.btnUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(747, 216);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(147, 39);
            this.btnUpdate.TabIndex = 15;
            this.btnUpdate.Text = "Sửa";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // labelX3
            // 
            this.labelX3.Location = new System.Drawing.Point(10, 126);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(132, 38);
            this.labelX3.TabIndex = 9;
            this.labelX3.Text = "Ghi chú";
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(557, 216);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(164, 39);
            this.btnAdd.TabIndex = 16;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Location = new System.Drawing.Point(856, 12);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(158, 35);
            this.chkActive.TabIndex = 8;
            this.chkActive.Text = "Hoạt động";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.Visible = false;
            // 
            // txtGoldDesc
            // 
            // 
            // 
            // 
            this.txtGoldDesc.Border.Class = "TextBoxBorder";
            this.txtGoldDesc.Location = new System.Drawing.Point(593, 12);
            this.txtGoldDesc.Name = "txtGoldDesc";
            this.txtGoldDesc.Size = new System.Drawing.Size(233, 38);
            this.txtGoldDesc.TabIndex = 0;
            // 
            // labelX7
            // 
            this.labelX7.Location = new System.Drawing.Point(12, 13);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(140, 35);
            this.labelX7.TabIndex = 0;
            this.labelX7.Text = "Mã vàng";
            // 
            // labelX4
            // 
            this.labelX4.Location = new System.Drawing.Point(10, 71);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(142, 38);
            this.labelX4.TabIndex = 0;
            this.labelX4.Text = "Giá bán ra";
            // 
            // labelX8
            // 
            this.labelX8.Location = new System.Drawing.Point(430, 72);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(157, 37);
            this.labelX8.TabIndex = 0;
            this.labelX8.Text = "Giá mua vào";
            // 
            // labelX6
            // 
            this.labelX6.Location = new System.Drawing.Point(433, 12);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(128, 38);
            this.labelX6.TabIndex = 0;
            this.labelX6.Text = "Diễn giải";
            // 
            // txtGoldCode
            // 
            // 
            // 
            // 
            this.txtGoldCode.Border.Class = "TextBoxBorder";
            this.txtGoldCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGoldCode.Location = new System.Drawing.Point(158, 12);
            this.txtGoldCode.Name = "txtGoldCode";
            this.txtGoldCode.Size = new System.Drawing.Size(226, 38);
            this.txtGoldCode.TabIndex = 1;
            // 
            // GoldCode
            // 
            this.GoldCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GoldCode.DataPropertyName = "GoldCode";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GoldCode.DefaultCellStyle = dataGridViewCellStyle3;
            this.GoldCode.FillWeight = 78.17259F;
            this.GoldCode.HeaderText = "Mã vàng";
            this.GoldCode.Name = "GoldCode";
            this.GoldCode.ReadOnly = true;
            // 
            // GoldDesc
            // 
            this.GoldDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GoldDesc.DataPropertyName = "GoldDesc";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GoldDesc.DefaultCellStyle = dataGridViewCellStyle4;
            this.GoldDesc.FillWeight = 121.8274F;
            this.GoldDesc.HeaderText = "Diễn giải";
            this.GoldDesc.Name = "GoldDesc";
            this.GoldDesc.ReadOnly = true;
            // 
            // PriceSell
            // 
            this.PriceSell.DataPropertyName = "PriceSell";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.Format = "###,###,###";
            dataGridViewCellStyle5.NullValue = "0";
            this.PriceSell.DefaultCellStyle = dataGridViewCellStyle5;
            this.PriceSell.HeaderText = "Giá bán ra";
            this.PriceSell.Name = "PriceSell";
            this.PriceSell.ReadOnly = true;
            this.PriceSell.Width = 203;
            // 
            // PriceBuy
            // 
            this.PriceBuy.DataPropertyName = "PriceBuy";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.Format = "###,###,###";
            dataGridViewCellStyle6.NullValue = "0";
            this.PriceBuy.DefaultCellStyle = dataGridViewCellStyle6;
            this.PriceBuy.HeaderText = "Giá mua vào";
            this.PriceBuy.Name = "PriceBuy";
            this.PriceBuy.ReadOnly = true;
            this.PriceBuy.Width = 204;
            // 
            // Old
            // 
            this.Old.DataPropertyName = "Old";
            this.Old.HeaderText = "Tuổi";
            this.Old.Name = "Old";
            this.Old.ReadOnly = true;
            // 
            // Notes
            // 
            this.Notes.DataPropertyName = "Notes";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Notes.DefaultCellStyle = dataGridViewCellStyle7;
            this.Notes.HeaderText = "Ghi chú";
            this.Notes.Name = "Notes";
            this.Notes.ReadOnly = true;
            this.Notes.Width = 204;
            // 
            // ActiveDesc
            // 
            this.ActiveDesc.DataPropertyName = "ActiveDesc";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ActiveDesc.DefaultCellStyle = dataGridViewCellStyle8;
            this.ActiveDesc.HeaderText = "Hoạt động";
            this.ActiveDesc.Name = "ActiveDesc";
            this.ActiveDesc.ReadOnly = true;
            this.ActiveDesc.Width = 204;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Visible = false;
            // 
            // FrmGoldType
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1250, 639);
            this.ControlBox = true;
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.dgvGoldType);
            this.Controls.Add(this.txtAgeGold);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtPriceBuy);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPriceSell);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.chkActive);
            this.Controls.Add(this.txtGoldDesc);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.txtGoldCode);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FrmGoldType";
            this.Text = "Loại vàng và giá";
            this.Load += new System.EventHandler(this.FrmGoldType_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoldType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected DevComponents.DotNetBar.Controls.DataGridViewX dgvGoldType;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.ButtonX btnUpdate;
        private DevComponents.DotNetBar.ButtonX btnAdd;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNote;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPriceBuy;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPriceSell;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.CheckBox chkActive;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGoldDesc;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGoldCode;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAgeGold;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceSell;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Old;
        private System.Windows.Forms.DataGridViewTextBoxColumn Notes;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActiveDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Active;
    }
}