﻿using DevComponents.DotNetBar.Controls;
using GoldManager.Process;
using GoldManager.Views.UserControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Printing;

namespace GoldManager.Views
{
    public partial class FrmPrinter : FrmSimple
    {
        public FrmPrinter()
        {
            InitializeComponent();
        }

        private void FrmPrinter_Load(object sender, EventArgs e)
        {
            loadPrintList();
            loadCOM();
        }

        protected override void setStatusItems(string status)
        {

        }

        #region PRINTER
        private void loadPrintList()
        {
            String pkInstalledPrinters;
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                dropPrintList.Items.Add(pkInstalledPrinters);
            }

            if (!String.IsNullOrEmpty(Properties.Settings.Default.printer))
            {
                dropPrintList.SelectedItem = Properties.Settings.Default.printer;
            }
        }

        private void dropPrintList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = ((ComboBoxEx)sender).SelectedItem.ToString();
            if (String.IsNullOrEmpty(value)){
                showWarning("Không thể cài đặt máy in");
            } else {
                Properties.Settings.Default.printer = value;
                Properties.Settings.Default.Save();
            }
        }
        #endregion PRINTER


        #region PORT COM
        private void dropCOMList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = ((ComboBoxEx)sender).SelectedItem.ToString();
            if (String.IsNullOrEmpty(value)){
                showWarning("Không lấy được tên cân");
            } else {
                Properties.Settings.Default.scalePortCOM = value;
                Properties.Settings.Default.Save();
            }
        }


        public List<string> getAllPorts()
        {
            List<String> allPorts = new List<String>();
            foreach (String portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                allPorts.Add(portName);
            }
            dropCOMList.Items.AddRange(allPorts.ToArray());
            return allPorts;
        }



        public void loadCOM()
        {
            getAllPorts();
            if (!String.IsNullOrEmpty(Properties.Settings.Default.scalePortCOM)){
                dropCOMList.SelectedItem = Properties.Settings.Default.scalePortCOM;
            }
        }
        #endregion PORT COM
    }
}
