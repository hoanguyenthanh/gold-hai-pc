﻿namespace GoldManager.Views
{
    partial class FrmInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvoice));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnViewDetail = new System.Windows.Forms.Button();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.combTypeTrans = new System.Windows.Forms.ComboBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.combTypeInvoice = new System.Windows.Forms.ComboBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtToDate = new System.Windows.Forms.DateTimePicker();
            this.dtFromDate = new System.Windows.Forms.DateTimePicker();
            this.grdOrder = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.labelX64 = new DevComponents.DotNetBar.LabelX();
            this.grdOrderDetail = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeTransNameDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeightDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayBonus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Depot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelX65 = new DevComponents.DotNetBar.LabelX();
            this.btnRefreshInvoice = new DevComponents.DotNetBar.ButtonX();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.OrderCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeTransName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTrans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeTrans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SellTotalWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyTotalWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceSell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalSalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPayBonus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CusName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // btnViewDetail
            // 
            this.btnViewDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewDetail.Location = new System.Drawing.Point(12, 53);
            this.btnViewDetail.Name = "btnViewDetail";
            this.btnViewDetail.Size = new System.Drawing.Size(117, 23);
            this.btnViewDetail.TabIndex = 17;
            this.btnViewDetail.Text = "Xem chi tiết";
            this.btnViewDetail.UseVisualStyleBackColor = true;
            this.btnViewDetail.Click += new System.EventHandler(this.btnViewDetail_Click);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX3.Location = new System.Drawing.Point(550, 53);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(344, 23);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "DANH SÁCH HÓA ĐƠN";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // combTypeTrans
            // 
            this.combTypeTrans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combTypeTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combTypeTrans.FormattingEnabled = true;
            this.combTypeTrans.Location = new System.Drawing.Point(876, 7);
            this.combTypeTrans.Name = "combTypeTrans";
            this.combTypeTrans.Size = new System.Drawing.Size(116, 26);
            this.combTypeTrans.TabIndex = 15;
            this.combTypeTrans.SelectedIndexChanged += new System.EventHandler(this.combTypeTrans_SelectedIndexChanged);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX2.Location = new System.Drawing.Point(770, 10);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(100, 23);
            this.labelX2.TabIndex = 14;
            this.labelX2.Text = "Loại giao dịch";
            // 
            // combTypeInvoice
            // 
            this.combTypeInvoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combTypeInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combTypeInvoice.FormattingEnabled = true;
            this.combTypeInvoice.Location = new System.Drawing.Point(636, 7);
            this.combTypeInvoice.Name = "combTypeInvoice";
            this.combTypeInvoice.Size = new System.Drawing.Size(101, 26);
            this.combTypeInvoice.TabIndex = 13;
            this.combTypeInvoice.SelectedIndexChanged += new System.EventHandler(this.combTypeInvoice_SelectedIndexChanged);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX1.Location = new System.Drawing.Point(572, 10);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(68, 23);
            this.labelX1.TabIndex = 12;
            this.labelX1.Text = "Loại HĐ";
            // 
            // dtToDate
            // 
            this.dtToDate.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtToDate.CustomFormat = "dd-MM-yyyy";
            this.dtToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToDate.Location = new System.Drawing.Point(316, 10);
            this.dtToDate.Name = "dtToDate";
            this.dtToDate.Size = new System.Drawing.Size(117, 24);
            this.dtToDate.TabIndex = 11;
            // 
            // dtFromDate
            // 
            this.dtFromDate.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFromDate.CustomFormat = "dd-MM-yyyy";
            this.dtFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromDate.Location = new System.Drawing.Point(85, 11);
            this.dtFromDate.Name = "dtFromDate";
            this.dtFromDate.Size = new System.Drawing.Size(117, 24);
            this.dtFromDate.TabIndex = 10;
            // 
            // grdOrder
            // 
            this.grdOrder.AllowUserToAddRows = false;
            this.grdOrder.AllowUserToDeleteRows = false;
            this.grdOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrder.BackgroundColor = System.Drawing.Color.White;
            this.grdOrder.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdOrder.ColumnHeadersHeight = 40;
            this.grdOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OrderCode,
            this.TypeTransName,
            this.DateTrans,
            this.TimeTrans,
            this.SellTotalWeight,
            this.BuyTotalWeight,
            this.TotalWeight,
            this.PriceSell,
            this.PriceBuy,
            this.Amount,
            this.TotalSalary,
            this.TotalPayBonus,
            this.TotalAmount,
            this.ModUser,
            this.Status,
            this.CusName,
            this.Address,
            this.OrderId});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdOrder.DefaultCellStyle = dataGridViewCellStyle14;
            this.grdOrder.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.grdOrder.Location = new System.Drawing.Point(12, 82);
            this.grdOrder.MultiSelect = false;
            this.grdOrder.Name = "grdOrder";
            this.grdOrder.ReadOnly = true;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdOrder.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.grdOrder.RowTemplate.Height = 30;
            this.grdOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdOrder.Size = new System.Drawing.Size(1438, 353);
            this.grdOrder.TabIndex = 0;
            this.grdOrder.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrder_CellContentClick);
            this.grdOrder.SelectionChanged += new System.EventHandler(this.grdOrder_SelectionChanged);
            // 
            // labelX64
            // 
            this.labelX64.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.labelX64.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX64.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX64.Location = new System.Drawing.Point(234, 11);
            this.labelX64.Name = "labelX64";
            this.labelX64.Size = new System.Drawing.Size(76, 23);
            this.labelX64.TabIndex = 0;
            this.labelX64.Text = "Đến ngày";
            // 
            // grdOrderDetail
            // 
            this.grdOrderDetail.AllowUserToAddRows = false;
            this.grdOrderDetail.AllowUserToResizeRows = false;
            this.grdOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrderDetail.BackgroundColor = System.Drawing.Color.White;
            this.grdOrderDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdOrderDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.grdOrderDetail.ColumnHeadersHeight = 30;
            this.grdOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdOrderDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductCode,
            this.TypeTransNameDT,
            this.GoldDesc,
            this.GroupName,
            this.ProductDesc,
            this.TotalWeightDT,
            this.Salary,
            this.PayBonus,
            this.Depot});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdOrderDetail.DefaultCellStyle = dataGridViewCellStyle26;
            this.grdOrderDetail.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.grdOrderDetail.Location = new System.Drawing.Point(12, 478);
            this.grdOrderDetail.MultiSelect = false;
            this.grdOrderDetail.Name = "grdOrderDetail";
            this.grdOrderDetail.ReadOnly = true;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdOrderDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.grdOrderDetail.RowHeadersVisible = false;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdOrderDetail.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.grdOrderDetail.RowTemplate.Height = 30;
            this.grdOrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdOrderDetail.Size = new System.Drawing.Size(1438, 157);
            this.grdOrderDetail.TabIndex = 0;
            // 
            // ProductCode
            // 
            this.ProductCode.DataPropertyName = "ProductCode";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ProductCode.DefaultCellStyle = dataGridViewCellStyle17;
            this.ProductCode.HeaderText = "Mã hàng";
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.ReadOnly = true;
            this.ProductCode.Width = 95;
            // 
            // TypeTransNameDT
            // 
            this.TypeTransNameDT.DataPropertyName = "TypeTransNameDT";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TypeTransNameDT.DefaultCellStyle = dataGridViewCellStyle18;
            this.TypeTransNameDT.HeaderText = "Loại nhập";
            this.TypeTransNameDT.Name = "TypeTransNameDT";
            this.TypeTransNameDT.ReadOnly = true;
            this.TypeTransNameDT.Width = 102;
            // 
            // GoldDesc
            // 
            this.GoldDesc.DataPropertyName = "GoldDesc";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GoldDesc.DefaultCellStyle = dataGridViewCellStyle19;
            this.GoldDesc.HeaderText = "Loại vàng";
            this.GoldDesc.Name = "GoldDesc";
            this.GoldDesc.ReadOnly = true;
            this.GoldDesc.Width = 104;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "GroupName";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GroupName.DefaultCellStyle = dataGridViewCellStyle20;
            this.GroupName.HeaderText = "Nhóm vàng";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            this.GroupName.Width = 114;
            // 
            // ProductDesc
            // 
            this.ProductDesc.DataPropertyName = "ProductDesc";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ProductDesc.DefaultCellStyle = dataGridViewCellStyle21;
            this.ProductDesc.HeaderText = "Diễn giải";
            this.ProductDesc.Name = "ProductDesc";
            this.ProductDesc.ReadOnly = true;
            this.ProductDesc.Width = 250;
            // 
            // TotalWeightDT
            // 
            this.TotalWeightDT.DataPropertyName = "TotalWeightDT";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TotalWeightDT.DefaultCellStyle = dataGridViewCellStyle22;
            this.TotalWeightDT.HeaderText = "Khối lượng";
            this.TotalWeightDT.Name = "TotalWeightDT";
            this.TotalWeightDT.ReadOnly = true;
            this.TotalWeightDT.Width = 110;
            // 
            // Salary
            // 
            this.Salary.DataPropertyName = "Salary";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Salary.DefaultCellStyle = dataGridViewCellStyle23;
            this.Salary.HeaderText = "Tiền công";
            this.Salary.Name = "Salary";
            this.Salary.ReadOnly = true;
            this.Salary.Width = 105;
            // 
            // PayBonus
            // 
            this.PayBonus.DataPropertyName = "PayBonus";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PayBonus.DefaultCellStyle = dataGridViewCellStyle24;
            this.PayBonus.HeaderText = "Khách trả thêm";
            this.PayBonus.Name = "PayBonus";
            this.PayBonus.ReadOnly = true;
            this.PayBonus.Width = 143;
            // 
            // Depot
            // 
            this.Depot.DataPropertyName = "Depot";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Depot.DefaultCellStyle = dataGridViewCellStyle25;
            this.Depot.HeaderText = "Kệ";
            this.Depot.Name = "Depot";
            this.Depot.ReadOnly = true;
            this.Depot.Width = 52;
            // 
            // labelX65
            // 
            this.labelX65.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.labelX65.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX65.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX65.Location = new System.Drawing.Point(12, 12);
            this.labelX65.Name = "labelX65";
            this.labelX65.Size = new System.Drawing.Size(67, 23);
            this.labelX65.TabIndex = 0;
            this.labelX65.Text = "Từ ngày";
            // 
            // btnRefreshInvoice
            // 
            this.btnRefreshInvoice.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefreshInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshInvoice.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefreshInvoice.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshInvoice.Image")));
            this.btnRefreshInvoice.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnRefreshInvoice.Location = new System.Drawing.Point(1319, 12);
            this.btnRefreshInvoice.Name = "btnRefreshInvoice";
            this.btnRefreshInvoice.Size = new System.Drawing.Size(131, 25);
            this.btnRefreshInvoice.TabIndex = 9;
            this.btnRefreshInvoice.Text = "Tải lại hóa đơn";
            this.btnRefreshInvoice.Click += new System.EventHandler(this.btnRefreshInvoice_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageFixedSize = new System.Drawing.Size(15, 15);
            this.btnSearch.Location = new System.Drawing.Point(439, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(104, 26);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Lọc";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // OrderCode
            // 
            this.OrderCode.DataPropertyName = "OrderCode";
            this.OrderCode.HeaderText = "Mã hóa đơn";
            this.OrderCode.Name = "OrderCode";
            this.OrderCode.ReadOnly = true;
            // 
            // TypeTransName
            // 
            this.TypeTransName.DataPropertyName = "TypeTransName";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TypeTransName.DefaultCellStyle = dataGridViewCellStyle2;
            this.TypeTransName.HeaderText = "Giao dịch";
            this.TypeTransName.Name = "TypeTransName";
            this.TypeTransName.ReadOnly = true;
            this.TypeTransName.Width = 102;
            // 
            // DateTrans
            // 
            this.DateTrans.DataPropertyName = "DateTrans";
            this.DateTrans.HeaderText = "Ngày giao dịch";
            this.DateTrans.Name = "DateTrans";
            this.DateTrans.ReadOnly = true;
            // 
            // TimeTrans
            // 
            this.TimeTrans.DataPropertyName = "TimeTrans";
            this.TimeTrans.HeaderText = "Giờ giao dịch";
            this.TimeTrans.Name = "TimeTrans";
            this.TimeTrans.ReadOnly = true;
            this.TimeTrans.Width = 80;
            // 
            // SellTotalWeight
            // 
            this.SellTotalWeight.DataPropertyName = "SellTotalWeight";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = "0";
            this.SellTotalWeight.DefaultCellStyle = dataGridViewCellStyle3;
            this.SellTotalWeight.HeaderText = "Tổng vàng bán";
            this.SellTotalWeight.Name = "SellTotalWeight";
            this.SellTotalWeight.ReadOnly = true;
            this.SellTotalWeight.Width = 96;
            // 
            // BuyTotalWeight
            // 
            this.BuyTotalWeight.DataPropertyName = "BuyTotalWeight";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N3";
            dataGridViewCellStyle4.NullValue = "0";
            this.BuyTotalWeight.DefaultCellStyle = dataGridViewCellStyle4;
            this.BuyTotalWeight.HeaderText = "Tổng vàng mua";
            this.BuyTotalWeight.Name = "BuyTotalWeight";
            this.BuyTotalWeight.ReadOnly = true;
            this.BuyTotalWeight.Width = 96;
            // 
            // TotalWeight
            // 
            this.TotalWeight.DataPropertyName = "TotalWeight";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N3";
            dataGridViewCellStyle5.NullValue = "0";
            this.TotalWeight.DefaultCellStyle = dataGridViewCellStyle5;
            this.TotalWeight.HeaderText = "Tổng vàng thanh toán";
            this.TotalWeight.Name = "TotalWeight";
            this.TotalWeight.ReadOnly = true;
            this.TotalWeight.Width = 132;
            // 
            // PriceSell
            // 
            this.PriceSell.DataPropertyName = "PriceSell";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PriceSell.DefaultCellStyle = dataGridViewCellStyle6;
            this.PriceSell.HeaderText = "Giá bán";
            this.PriceSell.Name = "PriceSell";
            this.PriceSell.ReadOnly = true;
            this.PriceSell.Width = 70;
            // 
            // PriceBuy
            // 
            this.PriceBuy.DataPropertyName = "PriceBuy";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PriceBuy.DefaultCellStyle = dataGridViewCellStyle7;
            this.PriceBuy.HeaderText = "Giá mua";
            this.PriceBuy.Name = "PriceBuy";
            this.PriceBuy.ReadOnly = true;
            this.PriceBuy.Width = 79;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle8;
            this.Amount.HeaderText = "Tiền vàng";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Width = 148;
            // 
            // TotalSalary
            // 
            this.TotalSalary.DataPropertyName = "TotalSalary";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TotalSalary.DefaultCellStyle = dataGridViewCellStyle9;
            this.TotalSalary.HeaderText = "Tiền công";
            this.TotalSalary.Name = "TotalSalary";
            this.TotalSalary.ReadOnly = true;
            this.TotalSalary.Width = 105;
            // 
            // TotalPayBonus
            // 
            this.TotalPayBonus.DataPropertyName = "TotalPayBonus";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TotalPayBonus.DefaultCellStyle = dataGridViewCellStyle10;
            this.TotalPayBonus.HeaderText = "Phải trả thêm";
            this.TotalPayBonus.Name = "TotalPayBonus";
            this.TotalPayBonus.ReadOnly = true;
            this.TotalPayBonus.Width = 130;
            // 
            // TotalAmount
            // 
            this.TotalAmount.DataPropertyName = "TotalAmount";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TotalAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.TotalAmount.HeaderText = "Tổng thanh toán";
            this.TotalAmount.Name = "TotalAmount";
            this.TotalAmount.ReadOnly = true;
            this.TotalAmount.Width = 153;
            // 
            // ModUser
            // 
            this.ModUser.DataPropertyName = "ModUser";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ModUser.DefaultCellStyle = dataGridViewCellStyle12;
            this.ModUser.HeaderText = "Nhân viên";
            this.ModUser.Name = "ModUser";
            this.ModUser.ReadOnly = true;
            this.ModUser.Width = 106;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "StatusName";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Status.DefaultCellStyle = dataGridViewCellStyle13;
            this.Status.HeaderText = "Trạng thái";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 108;
            // 
            // CusName
            // 
            this.CusName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CusName.DataPropertyName = "CusName";
            this.CusName.HeaderText = "Khách hàng";
            this.CusName.Name = "CusName";
            this.CusName.ReadOnly = true;
            // 
            // Address
            // 
            this.Address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Address.DataPropertyName = "Address";
            this.Address.HeaderText = "Địa chỉ";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            this.Address.Width = 54;
            // 
            // OrderId
            // 
            this.OrderId.DataPropertyName = "OrderId";
            this.OrderId.HeaderText = "OrderId";
            this.OrderId.Name = "OrderId";
            this.OrderId.ReadOnly = true;
            this.OrderId.Visible = false;
            // 
            // FrmInvoice
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1462, 647);
            this.ControlBox = true;
            this.Controls.Add(this.btnViewDetail);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.combTypeTrans);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.combTypeInvoice);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.dtToDate);
            this.Controls.Add(this.dtFromDate);
            this.Controls.Add(this.grdOrder);
            this.Controls.Add(this.labelX64);
            this.Controls.Add(this.grdOrderDetail);
            this.Controls.Add(this.labelX65);
            this.Controls.Add(this.btnRefreshInvoice);
            this.Controls.Add(this.btnSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmInvoice";
            this.Text = "DS hóa đơn";
            this.Load += new System.EventHandler(this.FrmInvoice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnRefreshInvoice;
        private DevComponents.DotNetBar.LabelX labelX64;
        private DevComponents.DotNetBar.LabelX labelX65;
        private DevComponents.DotNetBar.ButtonX btnSearch;
        protected DevComponents.DotNetBar.Controls.DataGridViewX grdOrderDetail;
        private DevComponents.DotNetBar.Controls.DataGridViewX grdOrder;
        private System.Windows.Forms.DateTimePicker dtFromDate;
        private System.Windows.Forms.DateTimePicker dtToDate;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.ComboBox combTypeInvoice;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.ComboBox combTypeTrans;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.Button btnViewDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeTransNameDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWeightDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayBonus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Depot;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeTransName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTrans;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeTrans;
        private System.Windows.Forms.DataGridViewTextBoxColumn SellTotalWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyTotalWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceSell;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalSalary;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPayBonus;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn CusName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderId;

    }
}