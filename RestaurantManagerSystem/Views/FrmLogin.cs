﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using GoldManager.Models;
using GoldManager.Process;
using System.IO;

namespace GoldManager.Views
{
    public partial class FrmLogin : Office2007Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            tbTenDN.Text = "";
            tbMatKhau.Text = "";
        }

        public void DangNhap()
        {
            if (String.IsNullOrEmpty(tbTenDN.Text) || String.IsNullOrEmpty(tbMatKhau.Text))
            {
                MessageBoxEx.Show("Vui lòng nhập đầy đủ Tên đăng nhập và Mật khẩu!");
            }
            else
            {
                DataTable dtUserInfo = null;
                using(Users_Process process = new Users_Process()){
                    process.intActionName = Users_Process.ACT_LOGIN;
                    process.strUserName = tbTenDN.Text;
                    process.strPassword = tbMatKhau.Text;

                    process.ExecuteProcess();
                    dtUserInfo = process.DtResult;
                }

                if (dtUserInfo != null && dtUserInfo.Rows.Count > 0){
                    string userName = dtUserInfo.Rows[0]["UserName"].ToString();
                    string fullName = dtUserInfo.Rows[0]["FullName"].ToString();
                    int isAdmin = (Int32)dtUserInfo.Rows[0]["IsAdmin"];

                    UserDto user = new UserDto();
                    user.UserName = userName;
                    user.FullName = fullName;
                    user.IsAdmin = isAdmin;

                    Properties.Settings.Default.userName = tbTenDN.Text;
                    Properties.Settings.Default.password = tbMatKhau.Text;

                    Properties.Settings.Default.Save();

                    GoldManager.Program.frmHome = new FrmHome();
                    GoldManager.Program.frmHome.setUserInfo(user);
                    GoldManager.Program.frmHome.Show();

                    this.Hide();
                } 
                else 
                {
                    MessageBoxEx.Show("Tên đăng nhập hoặc mật khẩu không chính xác!");
                    tbMatKhau.Text = "";
                }
            }
        }
        private void btDangNhap_Click(object sender, EventArgs e)
        {
            DangNhap();
        }

        private void tbMatKhau_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DangNhap();
            }
        }

        private void tbTenDN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tbMatKhau.Focus();
            }
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            getConfigInfo();

            String value = Properties.Settings.Default.userName;
            if (!String.IsNullOrEmpty(Properties.Settings.Default.userName) && !String.IsNullOrEmpty(Properties.Settings.Default.password))
            {
                cbxSavePwd.Checked = true;
                tbMatKhau.Text = Properties.Settings.Default.password;
                tbTenDN.Text = Properties.Settings.Default.userName;
            }
        }

        private void getConfigInfo()
        {
            //string text = null;
            //var fileStream = new FileStream(@"" + Environment.CurrentDirectory + "\\config.txt", FileMode.Open, FileAccess.Read);
            //using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            //{
            //    text = streamReader.ReadToEnd();
            //}
        }
    }
}