﻿namespace GoldManager.Views
{
    partial class FrmPrinter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dropPrintList = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dropCOMList = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.SuspendLayout();
            // 
            // dropPrintList
            // 
            this.dropPrintList.DisplayMember = "Text";
            this.dropPrintList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.dropPrintList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropPrintList.FormattingEnabled = true;
            this.dropPrintList.ItemHeight = 20;
            this.dropPrintList.Location = new System.Drawing.Point(86, 24);
            this.dropPrintList.Name = "dropPrintList";
            this.dropPrintList.Size = new System.Drawing.Size(197, 26);
            this.dropPrintList.TabIndex = 5;
            this.dropPrintList.SelectedIndexChanged += new System.EventHandler(this.dropPrintList_SelectedIndexChanged);
            // 
            // labelX2
            // 
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(30, 24);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(50, 26);
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "Máy in";
            // 
            // labelX1
            // 
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(322, 24);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(38, 26);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "Cân";
            // 
            // dropCOMList
            // 
            this.dropCOMList.DisplayMember = "Text";
            this.dropCOMList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.dropCOMList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropCOMList.FormattingEnabled = true;
            this.dropCOMList.ItemHeight = 20;
            this.dropCOMList.Location = new System.Drawing.Point(366, 24);
            this.dropCOMList.Name = "dropCOMList";
            this.dropCOMList.Size = new System.Drawing.Size(197, 26);
            this.dropCOMList.TabIndex = 14;
            this.dropCOMList.SelectedIndexChanged += new System.EventHandler(this.dropCOMList_SelectedIndexChanged);
            // 
            // FrmPrinter
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(735, 308);
            this.ControlBox = true;
            this.Controls.Add(this.dropCOMList);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.dropPrintList);
            this.Controls.Add(this.labelX2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmPrinter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            this.Text = "Thiết bị";
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.FrmPrinter_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.ComboBoxEx dropPrintList;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx dropCOMList;
    }
}