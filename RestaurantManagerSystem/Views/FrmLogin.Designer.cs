﻿namespace GoldManager.Views
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.tbMatKhau = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tbTenDN = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btDangNhap = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.cbxSavePwd = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbMatKhau
            // 
            // 
            // 
            // 
            this.tbMatKhau.Border.Class = "TextBoxBorder";
            this.tbMatKhau.Location = new System.Drawing.Point(170, 129);
            this.tbMatKhau.Name = "tbMatKhau";
            this.tbMatKhau.PasswordChar = '*';
            this.tbMatKhau.Size = new System.Drawing.Size(167, 20);
            this.tbMatKhau.TabIndex = 1;
            this.tbMatKhau.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMatKhau_KeyDown);
            // 
            // tbTenDN
            // 
            // 
            // 
            // 
            this.tbTenDN.Border.Class = "TextBoxBorder";
            this.tbTenDN.Location = new System.Drawing.Point(170, 93);
            this.tbTenDN.Name = "tbTenDN";
            this.tbTenDN.Size = new System.Drawing.Size(167, 20);
            this.tbTenDN.TabIndex = 0;
            this.tbTenDN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTenDN_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.Image = ((System.Drawing.Image)(resources.GetObject("labelX2.Image")));
            this.labelX2.Location = new System.Drawing.Point(35, 126);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(129, 21);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "Mật Khẩu:";
            // 
            // labelX1
            // 
            this.labelX1.Image = ((System.Drawing.Image)(resources.GetObject("labelX1.Image")));
            this.labelX1.Location = new System.Drawing.Point(35, 90);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(129, 21);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "Tên Đăng Nhập:";
            // 
            // btDangNhap
            // 
            this.btDangNhap.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btDangNhap.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btDangNhap.Image = ((System.Drawing.Image)(resources.GetObject("btDangNhap.Image")));
            this.btDangNhap.Location = new System.Drawing.Point(170, 206);
            this.btDangNhap.Name = "btDangNhap";
            this.btDangNhap.Size = new System.Drawing.Size(98, 28);
            this.btDangNhap.TabIndex = 2;
            this.btDangNhap.Text = "Đăng Nhập";
            this.btDangNhap.Click += new System.EventHandler(this.btDangNhap_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.buttonX2.Image = ((System.Drawing.Image)(resources.GetObject("buttonX2.Image")));
            this.buttonX2.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonX2.Location = new System.Drawing.Point(284, 206);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(98, 28);
            this.buttonX2.TabIndex = 4;
            this.buttonX2.Text = "Đóng";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // cbxSavePwd
            // 
            this.cbxSavePwd.AutoSize = true;
            this.cbxSavePwd.Checked = true;
            this.cbxSavePwd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxSavePwd.Location = new System.Drawing.Point(250, 165);
            this.cbxSavePwd.Name = "cbxSavePwd";
            this.cbxSavePwd.Size = new System.Drawing.Size(91, 17);
            this.cbxSavePwd.TabIndex = 5;
            this.cbxSavePwd.Text = "Lưu mật khẩu";
            this.cbxSavePwd.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(93, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 37);
            this.label1.TabIndex = 6;
            this.label1.Text = "ĐĂNG NHẬP";
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(394, 244);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxSavePwd);
            this.Controls.Add(this.tbMatKhau);
            this.Controls.Add(this.tbTenDN);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.btDangNhap);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmLogin";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PHẦN MỀM VÀNG - TIỆM VÀNG HẢI";
            this.Load += new System.EventHandler(this.frmDangNhap_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Controls.TextBoxX tbMatKhau;
        private DevComponents.DotNetBar.Controls.TextBoxX tbTenDN;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btDangNhap;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private System.Windows.Forms.CheckBox cbxSavePwd;
        private System.Windows.Forms.Label label1;
    }
}