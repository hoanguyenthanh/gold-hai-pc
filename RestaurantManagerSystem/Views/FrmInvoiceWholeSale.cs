﻿using GoldManager.Res.Reports;
using GoldManager.Process;
using GoldManager.Views.UserControl;
using System;
using System.Data;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Collections.Generic;
using GoldManager.Res;
using GoldManager.Models;
using System.Globalization;

namespace GoldManager.Views
{
    /*
     * Hóa đơn bán sỉ - Toa hàng
     * 
     **/
    public partial class FrmInvoiceWholeSale : FrmSimple
    {
        const string KEY_GOLD_TYPE_DEFAULT = "---Chọn loại vàng---";

        private DataTable mDtOrderDetailList;

        public long mOrderId;

        private PageSettings mPageSetting;
        private PrinterSettings mPrintSetting;


        private List<GoldType> goldTypeList = new List<GoldType>();
        
        private Customer mCustomer;

        public FrmInvoiceWholeSale()
        {
            InitializeComponent();
        }

        protected override void setStatusItems(string status)
        {

        }

        private void FrmInvoiceWholeSale_Load(object sender, EventArgs e)
        {
            this.dtCreateDate.Value = DateTime.Today;

            //Customer
            getCustomer();

            //Gold type
            getGoldType();

            //Get order list
            getOrders();
        }       

        private void getCustomer()
        {
            using (Customer_Process cus = new Customer_Process())
            {
                DataTable cusTable = cus.getCustomers();

                if (cusTable != null && cusTable.Rows.Count > 0)
                {
                    AutoCompleteStringCollection ac = new AutoCompleteStringCollection();

                    int countRow = cusTable.Rows.Count;

                    for (int i = 0; i < countRow; i++)
                    {
                        DataRow row = cusTable.Rows[i];
                        string code = Utilities.DbNullToEmpty(row["CusCode"].ToString()) + " - " + Utilities.DbNullToEmpty(row["CusName"].ToString());
                        ac.Add(code);
                    }

                    txtCusCode.AutoCompleteCustomSource = ac;
                }
            }
        }

        private void getGoldType()
        {
            using (GoldType_Process process = new GoldType_Process())
            {
                process.intActionName = GoldType_Process.ACT_GET_GOLD_TYPE;
                process.intActive = MConts.DATA_ACTIVE;
                process.ExecuteProcess();

                DataTable tableGoldType = process.DtResult;

                Dictionary<string, string> items = new Dictionary<string, string>();
                items.Add(MConts.DROPDOWN_DEFAULT_VALUE_KEY, "-");

                goldTypeList.Clear();

                if (tableGoldType != null && tableGoldType.Rows.Count > 0)
                {
                    foreach (DataRow row in tableGoldType.Rows)
                    {
                        items.Add(row["GoldCode"].ToString(), row["GoldCode"].ToString());

                        GoldType goldType = new GoldType();
                        goldType.GoldCode = row["GoldCode"].ToString();
                        goldType.GoldDesc = row["GoldDesc"].ToString();
                        goldType.Notes = row["Notes"].ToString();
                        goldType.Old = row["Old"].ToString();

                        goldType.PriceSell = (decimal)row["PriceSell"];
                        goldType.PriceBuy = (decimal)row["PriceSell"];

                        goldTypeList.Add(goldType);
                    }
                }

                combGoldType.DataSource = new BindingSource(items, null);
                combGoldType.DisplayMember = "Value";
                combGoldType.ValueMember = "Key";

                combGoldType.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;

                txtProductCodeS.Enabled = false;
                txtProductDescS.Enabled = false;
                btnSearch.Enabled = false;
                btnPush.Enabled = false;
            }
        }

        private void getOrders()
        {
            if (mOrderId > 0)
            {
                using (Order_Process op = new Order_Process())
                {
                    DataTable header = op.getOrder(mOrderId);

                    if (header != null && header.Rows.Count > 0)
                    {
                        ConvertDataRow cr = new ConvertDataRow(header.Rows[0]);

                        combGoldType.SelectedValue = cr.getString("GoldTypeExchange");
                        txtOrderCode.Text = cr.getString("OrderCode");
                        
                        int _cusId = cr.getInt("CusId");
                        if (_cusId > 0)
                        {
                            mCustomer = new Customer();
                            mCustomer.CusId = _cusId;
                            mCustomer.CusCode = cr.getString("CusCode");
                            mCustomer.CusName = cr.getString("CusName");
                            mCustomer.Address = cr.getString("Address");
                            mCustomer.CMND = cr.getString("CMND");

                            txtCusCode.Text = mCustomer.CusCode;
                            txtCusName.Text = mCustomer.CusName;
                            txtCusAddress.Text = mCustomer.Address;
                        }


                        using(Order_Process opd = new Order_Process())
                        {
                            DataTable detail = opd.getOrderDetail(mOrderId);
                            dgvOrderDetail.AutoGenerateColumns = false;
                            dgvOrderDetail.DataSource = detail;
                        }
                    }
                }
            }
        }

        private void getOrderDetail()
        {
            using (Order_Process process = new Order_Process())
            {
                DataTable table = process.getOrderDetail(this.mOrderId);

                dgvOrderDetail.AutoGenerateColumns = false;
                dgvOrderDetail.DataSource = table;
            }
        }

        private void callTotal(DataGridView gridView)
        {
            if (gridView != null && gridView.Rows.Count > 0)
            {
                txtTotalCountRows.Text = Convert.ToString(gridView.Rows.Count);

                decimal totalGoldWeight = 0;
                decimal TotalDiamandWeight = 0;
                decimal TotalWeight = 0;
                decimal TotalSalary = 0;
                decimal TotalAmount = 0;

                foreach (DataGridViewRow row in gridView.Rows)
                {
                    ConvertGridViewRow cr = new ConvertGridViewRow(row);
                    totalGoldWeight += cr.getDecimal("GoldWeightDT");
                    TotalDiamandWeight += cr.getDecimal("DiamondWeight");
                    TotalWeight += cr.getDecimal("TotalWeightDT");
                    TotalSalary += cr.getDecimal("Salary");
                    TotalAmount += cr.getDecimal("AmountDT");
                }

                txtTotalGoldWeight.Text = Convert.ToString(totalGoldWeight);
                txtTotalDiamandWeight.Text = Convert.ToString(TotalDiamandWeight);
                txtTotalWeight.Text = Convert.ToString(TotalWeight);
                txtTotalSalary.Text = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", TotalSalary);
                txtTotalAmount.Text = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", TotalAmount);
            }
            else
            {
                txtTotalCountRows.Text = "";
                txtTotalGoldWeight.Text = "";
                txtTotalDiamandWeight.Text = "";
                txtTotalWeight.Text = "";
                txtTotalSalary.Text = "";
                txtTotalAmount.Text = "";
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            using (Product_Process pp = new Product_Process())
            {
                DataTable table = pp.searchProduct(combGoldType.SelectedValue.ToString(), txtProductCodeS.Text, txtProductDescS.Text);
                dgvProducts.AutoGenerateColumns = false;
                dgvProducts.DataSource = table;
            }
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            using (PopupSearchCus popup = new PopupSearchCus())
            {
                popup.FormClosing += new FormClosingEventHandler(cus_FormClosing);
                popup.ShowDialog();
            }
        }

        protected void cus_FormClosing(object sender, EventArgs e)
        {
            PopupSearchCus popup = sender as PopupSearchCus;
            
            if (popup.customer != null && popup.customer.CusId > 0)
            {
                mCustomer = popup.customer;
                txtCusCode.Text = popup.customer.CusCode;
                txtCusName.Text = popup.customer.CusName;
                txtCusAddress.Text = popup.customer.Address;

                txtProductCodeS.Enabled = true;
                txtProductDescS.Enabled = true;
                btnPush.Enabled = true;
                btnSearch.Enabled = true;
            }
            else if (mCustomer != null)
            {
                txtCusCode.Text = mCustomer.CusCode;
                txtCusName.Text = mCustomer.CusName;
                txtCusAddress.Text = mCustomer.Address;

                txtProductCodeS.Enabled = true;
                txtProductDescS.Enabled = true;
                btnPush.Enabled = true;
                btnSearch.Enabled = true;
            } else {
                txtCusCode.Text = "";
                txtCusName.Text = "";
                txtCusAddress.Text = "";

                txtProductCodeS.Enabled = false;
                txtProductDescS.Enabled = false;
                btnPush.Enabled = false;
                btnSearch.Enabled = false;
            }

            if (combGoldType.SelectedValue != null) {
                
                string value = combGoldType.SelectedValue.ToString();
                
                if (MConts.DROPDOWN_DEFAULT_VALUE_KEY.Equals(value))
                {
                    txtProductCodeS.Enabled = false;
                    txtProductDescS.Enabled = false;
                    btnSearch.Enabled = false;
                    btnPush.Enabled = false;
                } else {

                    if (dgvOrderDetail.Rows.Count > 0)
                    {
                        combGoldType.Enabled = false;
                    } else {
                        combGoldType.Enabled = true;
                    }
                }
            }
        }

        private void combGoldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( (sender as ComboBox).SelectedValue == null){
                return;
            }

            string value = (sender as ComboBox).SelectedValue.ToString();
            foreach (GoldType type in goldTypeList)
            {
                if (type.GoldCode.Equals(value))
                {
                    txtPriceBuy.Text = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", type.PriceBuy);
                    txtPriceSale.Text = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", type.PriceSell);
                    txtOldGold.Text = type.Old;
                    break;
                } else {
                    txtPriceBuy.Text = txtPriceSale.Text = "";
                    txtOldGold.Text = "";
                }
            }

            if (MConts.DROPDOWN_DEFAULT_VALUE_KEY.Equals(value) || string.IsNullOrEmpty(txtCusCode.Text))
            {
                txtProductCodeS.Enabled = false;
                txtProductDescS.Enabled = false;
                btnSearch.Enabled = false;
                btnPush.Enabled = false;
            } else {
                txtProductCodeS.Enabled = true;
                txtProductDescS.Enabled = true;
                btnSearch.Enabled = true;
                btnPush.Enabled = true;
            }
        }

        private void btPush_Click(object sender, EventArgs e)
        {
            if (dgvProducts.CurrentRow != null && dgvProducts.CurrentRow.Index != -1)
            {
                ConvertGridViewRow cr = new ConvertGridViewRow(dgvProducts.CurrentRow);
                string productCode = cr.getString("ProductCodeList");
                if (!string.IsNullOrEmpty(productCode))
                {
                    insertProductToOrder(productCode);
                }
            }
        }

        private void txtProductCodeS_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string strProductCode = txtProductCodeS.Text.Trim().ToString();

                if (!string.IsNullOrEmpty(strProductCode))
                {
                    Console.WriteLine("txtBarcode Value: " + strProductCode);
                    txtProductCodeS.Text = string.Empty;
                    insertProductToOrder(strProductCode);
                }

                txtProductCodeS.Focus();
            }
        }

        private void insertProductToOrder(string productCode)
        {
            if (this.mOrderId > 0)
            {
                using (Order_Process op = new Order_Process())
                {
                    if (op.isProductInOrder(this.mOrderId, productCode))
                    {
                        MessageBox.Show(productCode + " đã tồn tại trong hóa đơn này", "Tồn tại dữ liệu", MessageBoxButtons.OK);

                        return;
                    }
                }
            }

            using(Product_Process pp = new Product_Process())
            {
                Product product = pp.getProductByCode(productCode);
                if (product != null && product.ProductId > 0)
                {
                    OrderData data = new OrderData();
                    data.Method = "insert";

                    data.OrderId = this.mOrderId;
                    data.OrderDetailId = 0;
                    data.ProductId = product.ProductId;

                    data.TypeTrans = "sell";
                    data.ActionTrans = "sell";

                    data.ModUser = Properties.Settings.Default.userName;
                    data.TrnID = product.TrnID_Old;
                    data.ProductCode = product.ProductCode;
                    data.ProductDesc = product.ProductDesc;


                    data.DiamondWeight = product.DiamondWeight;
                    data.TotalWeightDT = product.TotalWeight;
                    data.TotalWeightDTDesc = "";
                    data.PriceDT = Convert.ToDecimal(txtPriceSale.Text);

                    data.GoldType = combGoldType.SelectedValue.ToString();
                    data.GroupID = product.GroupID;
                    data.Depot = product.StallsId;
                    data.Salary = product.Salary;
                    data.CusId = this.mCustomer.CusId;

                    //Get price sale
                    foreach(GoldType gt in goldTypeList)
                    {
                        if (gt.GoldCode.Equals(combGoldType.SelectedValue))
                        {
                            data.PriceDT = gt.PriceSell;
                            break;
                        }
                    }

                    data.AmountDT = product.GoldWeight * data.PriceDT;

                    using (Order_Process op = new Order_Process())
                    {
                        ResultSaleBuy result = op.updateOrderDetail(data);
                        if (result.Result == -1)
                        {
                            MessageBox.Show(result.ErrorDesc, "Lỗi", MessageBoxButtons.OK);
                        }
                        else
                        {
                            this.mOrderId = result.OrderId;
                            getOrderDetail();
                        }
                    }
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnComplete_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDeleteProductSelected_Click(object sender, EventArgs e)
        {
            if (showConfirmDelete())
            {
                bool hasDelete = false;
                for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
                {
                    DataGridViewRow row = dgvOrderDetail.Rows[i];
                    ConvertGridViewRow cr = new ConvertGridViewRow(row);

                    bool isCheck = cr.getBoolean("Check");
                    long orderDetailId = cr.getLong("OrderDetailId");

                    if (isCheck){
                        using (Order_Process op = new Order_Process())
                        {
                            ResultObject rs = op.deleteOrderDT(orderDetailId);
                            if (rs.Result == -1)
                            {
                                MessageBox.Show(rs.ErrorDesc, "Lỗi", MessageBoxButtons.OK);
                            } else {
                                hasDelete = true;
                            }
                        }
                    }
                }

                if (hasDelete)
                {
                    getOrderDetail();
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            mCustomer = null;
            txtCusCode.Text = "";
            txtCusName.Text = "";
            txtCusAddress.Text = "";

            combGoldType.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
            btnPush.Enabled = false;

            txtProductCodeS.Text = "";
            txtProductDescS.Text = "";
        }

        private void dgvOrderDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvOrderDetail.CurrentRow != null && dgvOrderDetail.CurrentRow.Cells["Check"].ColumnIndex == e.ColumnIndex)
            {
                ConvertGridViewRow cr = new ConvertGridViewRow(dgvOrderDetail.CurrentRow);
                bool check = cr.getBoolean("Check");

                bool column = dgvOrderDetail.CurrentRow.Cells.Equals("Check");

                dgvOrderDetail.CurrentRow.Cells["Check"].Value = !check;
            }
        }

        private void txtMoneyDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtMoneyExtand_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtSalaryDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtSalaryRemaining_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtOldGold_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtWeightAll_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtWeightStamp_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtWeightGoldReal_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void dgvOrderDetail_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            callTotal(grid);

            if (grid != null && grid.Rows.Count > 0)
            {
                this.combGoldType.Enabled = false;
            } else {
                this.combGoldType.Enabled = true;
            }
        }
    }
}
