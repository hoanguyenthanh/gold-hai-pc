﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Models;
using GoldManager.Process;
using GoldManager.Res;
using System.Timers;
using System.IO;
using System.Globalization;

namespace GoldManager.Views
{
    public partial class FrmProductInDetail : Form
    {
        public GoldType goldTypeDTO;
        public long productId;
        private DataRow productRow;
        private DataTable DtGoldGroup;
        private DataTable DtBaseRule;
        public FrmProductInput.ReloadProduct reloadParent;
        public OnCloseForm onCloseForm;


        public System.Timers.Timer timerWeight = new System.Timers.Timer();
        public bool isRecieveData = false;

        public delegate void OnCloseForm();



        public FrmProductInDetail()
        {
            InitializeComponent();
        }

        public FrmProductInDetail (long productId, DataRow prodRow, GoldType typeDTO)
        {
            InitializeComponent();

            this.productId = productId;
            this.productRow = prodRow;
            this.goldTypeDTO = typeDTO;
        }

        private void FrmProductInDetail_Load(object sender, EventArgs e)
        {
            if (this.productId > 0)
            {
                this.loadProductData();
            }
            else
            {
                if (this.goldTypeDTO != null)
                {
                    this.txtGoldType.Text = String.IsNullOrEmpty(this.goldTypeDTO.GoldCode) ? "" : this.goldTypeDTO.GoldCode;
                    this.txtGoldTypeName.Text = String.IsNullOrEmpty(this.goldTypeDTO.GoldDesc) ? "" : this.goldTypeDTO.GoldDesc;
                }
                this.txtProductDesc.Text = "";
                this.txtWeight.Text = "0";
                this.txtWeightDiamand.Text = "0";
                this.txtWeightTotal.Text = "0";
            }

            loadStalls(); //Quầy hàng
            loadGoldGroup();
            loadBaseRule();

            if (!String.IsNullOrEmpty(Properties.Settings.Default.scalePortCOM))
            {
                portWeight.PortName = Properties.Settings.Default.scalePortCOM;
            }

            //#region
            // value weight: 0.05789Tl4
            //string w = "0.05749";
            //if (this.txtWeight.Text != null && w != null && !w.Equals(this.txtWeight.Text))
            //{
            //    string weightToRound = (Double.Parse(w) * 1000).ToString();
            //    if (weightToRound.IndexOf('.') != -1)
            //    {
            //        weightToRound = weightToRound.Substring(0, weightToRound.IndexOf('.') + 2);
            //        long lw = (long)Math.Round(Convert.ToDecimal(weightToRound), MidpointRounding.AwayFromZero);
            //        this.txtWeight.Text = string.Format("{0: 0.#}", lw);
            //    }
            //}
            //#endregion

            


            //this.txtWeight.GotFocus += OnFocus;
            //this.txtWeight.LostFocus += OnDefocus;
        }

        /*
       * Validation only input text number
       */
        protected void txtNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.');
        }


        private void loadProductData()
        {
            if (productId == -1)
            {
                return;
            }

            using(Product_Process get = new Product_Process())
            {
                get.int64ProductId = this.productId;

                DataRow row = get.getProduct(this.productId);
                if (row != null)
                {
                    ConvertDataRow cr = new ConvertDataRow(row);

                    this.txtGoldType.Text = cr.getString("GoldCode");
                    this.txtProductDesc.Text = cr.getString("ProductDesc");
                    this.txtWeight.Text = string.Format("{0}", cr.getDecimal("GoldWeight"));
                    this.txtWeightDiamand.Text = string.Format("{0}", cr.getDecimal("DiamondWeight"));
                    this.txtWeightTotal.Text = string.Format("{0}", cr.getDecimal("TotalWeight"));
                    this.txtSalary.Text = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", cr.getDecimal("Salary"));
                }
            }

            if (!String.IsNullOrEmpty(this.txtGoldType.Text))
            {
                using(GoldType_Process type = new GoldType_Process())
                {
                    this.goldTypeDTO = type.getGoldTypeByCode(this.txtGoldType.Text);
                    if (goldTypeDTO != null && goldTypeDTO.GoldDesc != null)
                    {
                        this.txtGoldTypeName.Text = goldTypeDTO.GoldDesc;
                    }
                }
            }
        }

        //Quầy hàng
        protected void loadStalls()
        {
            Dictionary<string, string> items = new Dictionary<string, string>();

            items.Add(MConts.DROPDOWN_DEFAULT_VALUE_KEY, MConts.DROPDOWN_DEFAULT_VALUE_DESC);
            items.Add(MConts.STALLS_01, MConts.STALLS_01_DESC);
            items.Add(MConts.STALLS_02, MConts.STALLS_02_DESC);
            items.Add(MConts.STALLS_03, MConts.STALLS_03_DESC);
            items.Add(MConts.STALLS_04, MConts.STALLS_04_DESC);
            items.Add(MConts.STALLS_05, MConts.STALLS_05_DESC);
            items.Add(MConts.STALLS_06, MConts.STALLS_06_DESC);
            items.Add(MConts.STALLS_07, MConts.STALLS_07_DESC);
            items.Add(MConts.STALLS_08, MConts.STALLS_08_DESC);


            cboxStalls.DataSource = new BindingSource(items, null);
            cboxStalls.DisplayMember = "Value";
            cboxStalls.ValueMember = "Key";

            cboxStalls.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
            cboxStalls.SelectedText = MConts.DROPDOWN_DEFAULT_VALUE_DESC;

            if (this.productRow != null && this.productId > 0)
            {
                cboxStalls.SelectedValue = this.productRow["StallsId"].ToString();
            }
        }


        //Tiêu chuẩn cơ sở
        protected void loadBaseRule()
        {
            using (BaseRule_Process process = new BaseRule_Process())
            {
                process.intActionName = BaseRule_Process.ACT_GET_RULE;
                process.intActive = MConts.DATA_ACTIVE;
                process.ExecuteProcess();

                DtBaseRule = process.DtResult;

                Dictionary<string, string> items = new Dictionary<string, string>();
                items.Add(MConts.DROPDOWN_DEFAULT_VALUE_KEY, MConts.DROPDOWN_DEFAULT_VALUE_DESC);

                if (DtBaseRule != null && DtBaseRule.Rows.Count > 0)
                {
                    foreach (DataRow row in DtBaseRule.Rows)
                    {
                        items.Add(row["BaseRuleName"].ToString(), row["BaseRuleName"].ToString());
                    }
                }

                cboxBaseRule.DataSource = new BindingSource(items, null);
                cboxBaseRule.DisplayMember = "Value";
                cboxBaseRule.ValueMember = "Key";
                cboxBaseRule.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
                cboxBaseRule.SelectedText = MConts.DROPDOWN_DEFAULT_VALUE_DESC;

                if (this.productRow != null && this.productId >0)
                {
                    cboxBaseRule.SelectedValue = this.productRow["BaseRule"].ToString();
                }
            }
        }


        //Nhóm vàng
        protected void loadGoldGroup()
        {
            using (GoldGroup_Process process = new GoldGroup_Process())
            {
                process.intActionName = GoldGroup_Process.ACT_GET_GROUP;
                process.intActive = MConts.DATA_ACTIVE;
                process.ExecuteProcess();
                DtGoldGroup = process.DtResult;

                Dictionary<string, string> items = new Dictionary<string, string>();
                items.Add(MConts.DROPDOWN_DEFAULT_VALUE_KEY, MConts.DROPDOWN_DEFAULT_VALUE_DESC);

                if (DtGoldGroup != null && DtGoldGroup.Rows.Count > 0)
                {
                    foreach (DataRow row in DtGoldGroup.Rows)
                    {
                        items.Add(row["GroupID"].ToString(), row["GroupName"].ToString());
                    }
                }
                cboxGoldGroup.DataSource = new BindingSource(items, null);
                cboxGoldGroup.DisplayMember = "Value";
                cboxGoldGroup.ValueMember = "Key";

                cboxGoldGroup.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
                cboxGoldGroup.SelectedText = MConts.DROPDOWN_DEFAULT_VALUE_DESC;

                if (this.productRow != null && this.productId > 0)
                {
                    cboxGoldGroup.SelectedValue = this.productRow["GroupID"].ToString();
                }
            }
        }


        /*
         * Get list lable name to show warning
         */
        private string getFieldNameRequire()
        {
            StringBuilder builder = new StringBuilder();

            if (cboxStalls.SelectedValue == null || cboxStalls.SelectedValue.ToString() == MConts.DROPDOWN_DEFAULT_VALUE_KEY)
            {
                builder.Append("\"" + lblStalls.Text + "\"");
            }

            if (String.IsNullOrEmpty(txtGoldType.Text))
            {
                builder.Append(getComma(builder.ToString()) + " \"" + lblGoldType.Text + "\"");
            }

            if (cboxGoldGroup.SelectedValue == null || cboxGoldGroup.SelectedValue.ToString() == MConts.DROPDOWN_DEFAULT_VALUE_KEY)
            {
                builder.Append(getComma(builder.ToString()) + " \"" + lblGoldGroup.Text + "\"");
            }

            if (cboxBaseRule.SelectedValue == null || cboxBaseRule.SelectedValue.ToString() == MConts.DROPDOWN_DEFAULT_VALUE_KEY)
            {
                builder.Append(getComma(builder.ToString()) + " \"" + lblBaseRule.Text + "\"");
            }

            if (builder.Length > 0)
            {
                return string.Format("Các trường sau phải nhập liệu: {0}", builder.ToString());
            }

            return "";
        }

        private string getComma(string text)
        {
            return text.Length > 0 ? "," : "";
        }

        /*
         * Check validation data before insert or update
         */
        private bool validationOk()
        {
            string str = getFieldNameRequire();
            if (str.Length > 0)
            {
                showWarning(str);
                return false;
            }

            return true;
        }

        private bool saveData()
        {
            int result = 0;
            using (Product_Process process = new Product_Process())
            {
                process.int64ProductId = this.productId;

                process.intStallsId = int.Parse(cboxStalls.SelectedValue.ToString());
                process.strProductDesc = txtProductDesc.Text;
                process.strGoldCode = txtGoldType.Text;
                process.strGroupID = cboxGoldGroup.SelectedValue.ToString();
                process.strBaseRule = cboxBaseRule.SelectedValue.ToString();

                process.douQuantity = 1;

                double goldW = 0;
                double diamandW = 0;
                double salary = 0;

                double.TryParse(txtWeight.Text, out goldW);
                double.TryParse(txtWeightDiamand.Text, out diamandW);
                double.TryParse(txtSalary.Text, out salary);

                process.douGoldWeight = goldW;
                process.douDiamondWeight = diamandW;
                process.douTotalWeight = goldW + diamandW;

                process.douSalary = salary;
                

                if (this.productId > 0)
                {
                    result = process.updateProduct(this.productId);
                }
                else
                {
                    result = process.insertProduct();
                    this.productId = process.int64ProductId;
                }
            }
            return result > 0;
        }

        

        private void txtWeight_TextChanged(object sender, EventArgs e)
        {


            callTotalWeight(txtWeight.Text, txtWeightDiamand.Text);
        }



        private void txtWeightDiamand_TextChanged(object sender, EventArgs e)
        {
            callTotalWeight(txtWeight.Text, txtWeightDiamand.Text);
        }

        private void callTotalWeight(string strGoldWeight, string strDiamandWeight)
        {
            double goldWeight = 0, diamandWeight = 0;
            if (!string.IsNullOrEmpty(strGoldWeight))
            {

                if (!double.TryParse(strGoldWeight, out goldWeight))
                {
                    showWarning("\"Trọng lượng vàng\" có giá trị không hợp lệ");
                    return;
                }
            }

            if (!string.IsNullOrEmpty(strDiamandWeight))
            {
                if (!double.TryParse(strDiamandWeight, out diamandWeight))
                {
                    showWarning("\"Trọng lượng đá\" có giá trị không hợp lệ");
                    return;
                }
            }

            txtWeightTotal.Text = string.Format("{0}", goldWeight + diamandWeight);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!validationOk())
            {
                return;
            }

            if (!saveData())
            {
                showInfomation("Không thể lưu dữ liệu");
            } else {
                if (reloadParent != null && this.productId > 0)
                {
                    reloadParent.Invoke(this.productId);
                }

                Close();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            using (FrmGoldTypeDialog dialogGoldType = new FrmGoldTypeDialog())
            {
                dialogGoldType.FormClosed += new FormClosedEventHandler(dialogGoldType_Closed);
                dialogGoldType.ShowDialog(this);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dialogGoldType_Closed(object sender, EventArgs e)
        {
            FrmGoldTypeDialog dialog = sender as FrmGoldTypeDialog;
            if (!String.IsNullOrEmpty(dialog.typeName))
            {
                this.txtGoldTypeName.Text = dialog.typeName;
            }

            if (!String.IsNullOrEmpty(dialog.typeCode))
            {
                this.txtGoldType.Text = dialog.typeCode;
                this.resetData();
            }
        }

        private void dialogGoldTypeBrower_Closed(object sender, EventArgs e)
        {
            FrmGoldTypeDialog dialog = sender as FrmGoldTypeDialog;
            if (!String.IsNullOrEmpty(dialog.typeName))
            {
                this.txtGoldTypeName.Text = dialog.typeName;
            }

            if (!String.IsNullOrEmpty(dialog.typeCode))
            {
                this.txtGoldType.Text = dialog.typeCode;
            }

        }

        private void resetData()
        {
            this.productId = -1;

            cboxStalls.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
            cboxGoldGroup.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
            cboxBaseRule.SelectedValue = MConts.DROPDOWN_DEFAULT_VALUE_KEY;
            txtWeight.Text = "0";
            txtWeightDiamand.Text = "0";
            txtWeightTotal.Text = "0";
            txtProductDesc.Text = "";
        }

        protected void showInfomation(string msg)
        {
            MessageBox.Show(msg, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /*
         * Show warning
         */
        protected void showWarning(string msg)
        {
            MessageBox.Show(msg, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        //private void OnFocus(object sender, EventArgs e)
        //{
        //    this.startTimer();
        //}

        //private void OnDefocus(object sender, EventArgs e)
        //{
        //    this.stopTimer();
        //}


        private void checkReadWeight_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox check = sender as CheckBox;
            
            if (check.Checked)
            {
                startTimer();
            } else 
            {
                stopTimer();
            }
        }

        private void txtWeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtWeightDiamand_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        private void txtSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }

        #region PORT COM http://codesamplez.com/programming/serial-port-communication-c-sharp
        //private SerialPort mScalePort;
        private void openPort()
        {
            try
            {
                if (portWeight != null && !portWeight.IsOpen) //if not open, open the port
                {
                    portWeight.Open();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void closePort()
        {
            try
            {
                if (portWeight != null && portWeight.IsOpen)
                {
                    portWeight.Close();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void startTimer()
        {
            timerWeight.AutoReset = true;
            timerWeight.Enabled = true;
            timerWeight.Interval = 1000; //default to 1 second
            timerWeight.Elapsed += new ElapsedEventHandler(OnTimedCommEvent);
        }

        public void stopTimer()
        {
            isRecieveData = false;

            timerWeight.Stop();
            timerWeight.AutoReset = false;
            timerWeight.Enabled = false;
        }

        public void OnTimedCommEvent(object source, ElapsedEventArgs e)
        {
            if (!isRecieveData)
            {
                isRecieveData = true;
                Console.WriteLine("OnTimedCommEvent called: " + DateTime.Now.ToString("yyyy-MM-dd mm:ss.fff"));

                //Whenever you need update a UI component from a method in a separate thread, instead of simply writing
                //textBox2.Invoke((Action)delegate
                //{
                //    string w3 = " " + this.textBox2.Text + "Tl2" + System.Environment.NewLine;
                //    textBox1.Text = convertWeight(w3);

                //});

                isRecieveData = false;
            }
            //SendReceiveData();
        }

        public void SendReceiveData()
        {
            openPort();

            if (!portWeight.IsOpen)
            {
                return;
            }

            byte[] cmdByteArray = new byte[1];
            portWeight.DiscardInBuffer();
            portWeight.DiscardOutBuffer();

            //send         
            cmdByteArray[0] = 0x7a;
            portWeight.Write(cmdByteArray, 0, 1);

            //while (portWeight.IsOpen && portWeight.BytesToRead == 0)
            //{
            //    //Console.WriteLine("SendReceiveData, portWeight.BytesToRead = " + portWeight.BytesToRead + ", " + DateTime.Today.ToString());
            //    Application.DoEvents();
            //}

            if (portWeight.IsOpen && portWeight.BytesToRead > 0)
            {
                string value = portWeight.ReadExisting();
                string valueConvert = convertWeight(value);

                txtWeight.Invoke((Action)delegate
                {
                    string currentWeight = txtWeight.Text;

                    if (!string.IsNullOrEmpty(currentWeight) 
                        && !string.IsNullOrEmpty(valueConvert) && valueConvert.Equals(currentWeight))
                    {
                        txtWeight.Text = valueConvert;
                    }
                });
                // 0.483603Tl2
            }

            if (portWeight.IsOpen)
            {
                portWeight.DiscardInBuffer();
                portWeight.DiscardOutBuffer();
                portWeight.Close();
            }

            isRecieveData = false;
        }

        private string convertWeight(string value)
        {
            string result = string.Empty;

            if (string.IsNullOrEmpty(value))
            {
                return result;
            }

            try
            {
                int lastIndexEnd = value.LastIndexOf("Tl2\r\n");

                if (lastIndexEnd != -1)
                {
                    int lastIndexSpace = value.LastIndexOf(" ");

                    if (lastIndexSpace != -1 && lastIndexEnd > lastIndexSpace)
                    {
                        string strWeight = value.Substring(lastIndexSpace, lastIndexEnd - lastIndexSpace);

                        if (!String.IsNullOrEmpty(strWeight))
                        {
                            string strWeightToRound = (Double.Parse(strWeight) * 1000).ToString();

                            if (strWeightToRound.IndexOf('.') != -1)
                            {
                                strWeightToRound = strWeightToRound.Substring(0, strWeightToRound.IndexOf('.') + 2);
                                long lw = (long)Math.Round(Convert.ToDecimal(strWeightToRound), MidpointRounding.AwayFromZero);
                                result = string.Format("{0: 0.#}", lw);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }

        #endregion COM

    }
}