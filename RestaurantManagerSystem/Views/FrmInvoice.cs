﻿using GoldManager.Res.Reports;
using GoldManager.Process;
using GoldManager.Views.UserControl;
using System;
using System.Data;
using System.Drawing.Printing;
using System.Windows.Forms;
using GoldManager.Models;
using GoldManager.Res;

namespace GoldManager.Views
{
    public partial class FrmInvoice : FrmSimple
    {
        public FrmInvoice()
        {
            InitializeComponent();
        }

        private void FrmInvoice_Load(object sender, EventArgs e)
        {
            IsFirstLoad = true;

            //Timer printer
            timer.Enabled = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(printInvoice);
            timer.AutoReset = true;

            //Date
            this.dtFromDate.Value = DateTime.Today.AddDays(-2);
            this.dtToDate.Value = DateTime.Today;


            //Combobox type invoice
            TypeInvoice[] list = new TypeInvoice[] { 
                new TypeInvoice("-1", "Tất cả"), 
                new TypeInvoice("le", "Bán lẻ"), 
                new TypeInvoice("si", "Bán sỉ") };
            combTypeInvoice.DataSource = list;
            combTypeInvoice.DisplayMember = "name";
            combTypeInvoice.ValueMember = "type";

            combTypeInvoice.SelectedIndex = 0;

            //Combobox type transaction
            TypeTrans[] trans = new TypeTrans[] {
                new TypeTrans("-1", "Tất cả"), 
                new TypeTrans("sell", "Bán ra"), 
                new TypeTrans("buy", "Mua vào"),
                new TypeTrans("sell_and_buy", "Bán-Mua")};
            combTypeTrans.DataSource = trans;
            combTypeTrans.DisplayMember = "Name";
            combTypeTrans.ValueMember = "Trans";

            combTypeTrans.SelectedIndex = 0;


            initPagePrint();


            //Get list order
            getOrders();
        }

        protected override void setStatusItems(string status)
        {

        }

        private void getOrders()
        {
            if (combTypeInvoice.SelectedValue == null 
                || combTypeTrans.SelectedValue == null
                || dtFromDate.Value == null
                || dtToDate.Value == null)
            {
                return;
            }

            using (Order_Process process = new Order_Process())
            {
                string typeInvoice = combTypeInvoice.SelectedValue.ToString();
                string typeTrans = combTypeTrans.SelectedValue.ToString();

                DataTable table = process.getOrders(dtFromDate.Value, dtToDate.Value, typeInvoice, typeTrans);

                grdOrder.AutoGenerateColumns = false;
                grdOrder.DataSource = table;
            }
        }

        private void loadDetail()
        {
            if (grdOrder.CurrentRow != null && grdOrder.CurrentRow.Index != -1)
            {
                long orderId = long.Parse(grdOrder.CurrentRow.Cells["OrderId"].ToString());

                grdOrderDetail.AutoGenerateColumns = false;
                grdOrderDetail.DataSource = getOrderDetail(orderId);
            }
            else
            {
                grdOrderDetail.AutoGenerateColumns = false;
                grdOrderDetail.DataSource = null;
            }
        }

        private DataTable getOrderDetail(long orderId)
        {
            DataTable result = new DataTable();
            using (Order_Process process = new Order_Process())
            {
                result = process.getOrderDetail(orderId);
            }

            return result == null ? new DataTable() : result;
        }

        private void btnRefreshInvoice_Click(object sender, EventArgs e)
        {
            getOrders();
            loadDetail();
        }

        private void grdOrder_SelectionChanged(object sender, EventArgs e)
        {
            if (grdOrder.CurrentRow != null && grdOrder.CurrentRow.Index != -1)
            {
                ConvertGridViewRow cr = new ConvertGridViewRow(grdOrder.CurrentRow);

                long orderId = cr.getLong("OrderId");

                grdOrderDetail.AutoGenerateColumns = false;
                grdOrderDetail.DataSource = getOrderDetail(orderId);
            }
        }

        private void grdOrder_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void combTypeInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            getOrders();
        }

        private void combTypeTrans_SelectedIndexChanged(object sender, EventArgs e)
        {
            getOrders();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (dtFromDate.Value <= dtToDate.Value)
            {
                getOrders();
            } else {
                MessageBox.Show("Ngày bắt đầu không được lớn hơn ngày kết thúc", "Ngày không hợp lệ", MessageBoxButtons.OK);
            }
        }

        private void btnViewDetail_Click(object sender, EventArgs e)
        {
            if (grdOrder.CurrentRow != null)
            {
                ConvertGridViewRow cr = new ConvertGridViewRow(grdOrder.CurrentRow);
                long _id = cr.getLong("OrderId");

                if (_id > 0){
                    using (FrmInvoiceWholeSale frm = new FrmInvoiceWholeSale())
                    {
                        frm.mOrderId = _id;
                        frm.ShowDialog();
                    }
                }
            }
        }
    }
}
