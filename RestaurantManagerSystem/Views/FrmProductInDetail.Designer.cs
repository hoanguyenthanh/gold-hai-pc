﻿namespace GoldManager.Views
{
    partial class FrmProductInDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductInDetail));
            this.txtGoldType = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblGoldType = new DevComponents.DotNetBar.LabelX();
            this.txtWeightTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.lblWeightTotal = new DevComponents.DotNetBar.LabelX();
            this.txtProductDesc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtWeight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnSaveClose = new DevComponents.DotNetBar.ButtonX();
            this.txtWeightDiamand = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboxStalls = new System.Windows.Forms.ComboBox();
            this.cboxBaseRule = new System.Windows.Forms.ComboBox();
            this.lblProductDesc = new DevComponents.DotNetBar.LabelX();
            this.lblStalls = new DevComponents.DotNetBar.LabelX();
            this.lblGoldGroup = new DevComponents.DotNetBar.LabelX();
            this.cboxGoldGroup = new System.Windows.Forms.ComboBox();
            this.lblBaseRule = new DevComponents.DotNetBar.LabelX();
            this.lblWeight = new DevComponents.DotNetBar.LabelX();
            this.lblWeightDiamand = new DevComponents.DotNetBar.LabelX();
            this.portWeight = new System.IO.Ports.SerialPort(this.components);
            this.btnSaveNew = new DevComponents.DotNetBar.ButtonX();
            this.txtGoldTypeName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkReadWeight = new System.Windows.Forms.CheckBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtSalary = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // txtGoldType
            // 
            this.txtGoldType.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtGoldType.Border.Class = "TextBoxBorder";
            this.txtGoldType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoldType.Location = new System.Drawing.Point(155, 41);
            this.txtGoldType.Name = "txtGoldType";
            this.txtGoldType.ReadOnly = true;
            this.txtGoldType.Size = new System.Drawing.Size(160, 23);
            this.txtGoldType.TabIndex = 46;
            // 
            // lblGoldType
            // 
            this.lblGoldType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldType.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblGoldType.Location = new System.Drawing.Point(71, 43);
            this.lblGoldType.Name = "lblGoldType";
            this.lblGoldType.Size = new System.Drawing.Size(62, 21);
            this.lblGoldType.TabIndex = 27;
            this.lblGoldType.Text = "Loại vàng";
            // 
            // txtWeightTotal
            // 
            // 
            // 
            // 
            this.txtWeightTotal.Border.Class = "TextBoxBorder";
            this.txtWeightTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightTotal.Location = new System.Drawing.Point(464, 210);
            this.txtWeightTotal.Name = "txtWeightTotal";
            this.txtWeightTotal.ReadOnly = true;
            this.txtWeightTotal.Size = new System.Drawing.Size(198, 23);
            this.txtWeightTotal.TabIndex = 25;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(650, 365);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 33);
            this.btnCancel.TabIndex = 44;
            this.btnCancel.Text = "Đóng";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblWeightTotal
            // 
            this.lblWeightTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeightTotal.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblWeightTotal.Location = new System.Drawing.Point(371, 212);
            this.lblWeightTotal.Name = "lblWeightTotal";
            this.lblWeightTotal.Size = new System.Drawing.Size(62, 21);
            this.lblWeightTotal.TabIndex = 30;
            this.lblWeightTotal.Text = "Tổng TLV";
            // 
            // txtProductDesc
            // 
            // 
            // 
            // 
            this.txtProductDesc.Border.Class = "TextBoxBorder";
            this.txtProductDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductDesc.Location = new System.Drawing.Point(155, 214);
            this.txtProductDesc.Name = "txtProductDesc";
            this.txtProductDesc.Size = new System.Drawing.Size(160, 23);
            this.txtProductDesc.TabIndex = 39;
            // 
            // txtWeight
            // 
            // 
            // 
            // 
            this.txtWeight.Border.Class = "TextBoxBorder";
            this.txtWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(464, 123);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(198, 23);
            this.txtWeight.TabIndex = 38;
            this.txtWeight.TextChanged += new System.EventHandler(this.txtWeight_TextChanged);
            this.txtWeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWeight_KeyPress);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSaveClose.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveClose.Image")));
            this.btnSaveClose.Location = new System.Drawing.Point(538, 365);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(106, 33);
            this.btnSaveClose.TabIndex = 40;
            this.btnSaveClose.Text = "Lưu";
            this.btnSaveClose.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtWeightDiamand
            // 
            // 
            // 
            // 
            this.txtWeightDiamand.Border.Class = "TextBoxBorder";
            this.txtWeightDiamand.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightDiamand.Location = new System.Drawing.Point(464, 167);
            this.txtWeightDiamand.Name = "txtWeightDiamand";
            this.txtWeightDiamand.Size = new System.Drawing.Size(198, 23);
            this.txtWeightDiamand.TabIndex = 37;
            this.txtWeightDiamand.TextChanged += new System.EventHandler(this.txtWeightDiamand_TextChanged);
            this.txtWeightDiamand.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWeightDiamand_KeyPress);
            // 
            // cboxStalls
            // 
            this.cboxStalls.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxStalls.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxStalls.FormattingEnabled = true;
            this.cboxStalls.ItemHeight = 16;
            this.cboxStalls.Location = new System.Drawing.Point(155, 126);
            this.cboxStalls.Name = "cboxStalls";
            this.cboxStalls.Size = new System.Drawing.Size(160, 24);
            this.cboxStalls.TabIndex = 34;
            // 
            // cboxBaseRule
            // 
            this.cboxBaseRule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxBaseRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxBaseRule.FormattingEnabled = true;
            this.cboxBaseRule.Location = new System.Drawing.Point(155, 170);
            this.cboxBaseRule.Name = "cboxBaseRule";
            this.cboxBaseRule.Size = new System.Drawing.Size(160, 24);
            this.cboxBaseRule.TabIndex = 36;
            // 
            // lblProductDesc
            // 
            this.lblProductDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductDesc.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblProductDesc.Location = new System.Drawing.Point(71, 216);
            this.lblProductDesc.Name = "lblProductDesc";
            this.lblProductDesc.Size = new System.Drawing.Size(62, 21);
            this.lblProductDesc.TabIndex = 32;
            this.lblProductDesc.Text = "Mô tả";
            // 
            // lblStalls
            // 
            this.lblStalls.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStalls.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStalls.Location = new System.Drawing.Point(71, 128);
            this.lblStalls.Name = "lblStalls";
            this.lblStalls.Size = new System.Drawing.Size(53, 21);
            this.lblStalls.TabIndex = 26;
            this.lblStalls.Text = "Quầy";
            // 
            // lblGoldGroup
            // 
            this.lblGoldGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldGroup.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblGoldGroup.Location = new System.Drawing.Point(71, 87);
            this.lblGoldGroup.Name = "lblGoldGroup";
            this.lblGoldGroup.Size = new System.Drawing.Size(78, 21);
            this.lblGoldGroup.TabIndex = 31;
            this.lblGoldGroup.Text = "Nhóm vàng";
            // 
            // cboxGoldGroup
            // 
            this.cboxGoldGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxGoldGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxGoldGroup.FormattingEnabled = true;
            this.cboxGoldGroup.ItemHeight = 16;
            this.cboxGoldGroup.Location = new System.Drawing.Point(155, 84);
            this.cboxGoldGroup.Name = "cboxGoldGroup";
            this.cboxGoldGroup.Size = new System.Drawing.Size(160, 24);
            this.cboxGoldGroup.TabIndex = 35;
            // 
            // lblBaseRule
            // 
            this.lblBaseRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBaseRule.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblBaseRule.Location = new System.Drawing.Point(71, 173);
            this.lblBaseRule.Name = "lblBaseRule";
            this.lblBaseRule.Size = new System.Drawing.Size(53, 21);
            this.lblBaseRule.TabIndex = 33;
            this.lblBaseRule.Text = "TCCS";
            // 
            // lblWeight
            // 
            this.lblWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblWeight.Location = new System.Drawing.Point(371, 125);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(87, 21);
            this.lblWeight.TabIndex = 28;
            this.lblWeight.Text = "T.Lượng vàng";
            // 
            // lblWeightDiamand
            // 
            this.lblWeightDiamand.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeightDiamand.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblWeightDiamand.Location = new System.Drawing.Point(371, 167);
            this.lblWeightDiamand.Name = "lblWeightDiamand";
            this.lblWeightDiamand.Size = new System.Drawing.Size(87, 21);
            this.lblWeightDiamand.TabIndex = 29;
            this.lblWeightDiamand.Text = "T.Lượng đá";
            // 
            // portWeight
            // 
            this.portWeight.BaudRate = 1200;
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveNew.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSaveNew.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveNew.Image")));
            this.btnSaveNew.Location = new System.Drawing.Point(12, 365);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(113, 33);
            this.btnSaveNew.TabIndex = 47;
            this.btnSaveNew.Text = "Thêm mới";
            this.btnSaveNew.Visible = false;
            this.btnSaveNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtGoldTypeName
            // 
            this.txtGoldTypeName.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtGoldTypeName.Border.Class = "TextBoxBorder";
            this.txtGoldTypeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoldTypeName.Location = new System.Drawing.Point(465, 41);
            this.txtGoldTypeName.Name = "txtGoldTypeName";
            this.txtGoldTypeName.ReadOnly = true;
            this.txtGoldTypeName.Size = new System.Drawing.Size(197, 23);
            this.txtGoldTypeName.TabIndex = 48;
            // 
            // labelX1
            // 
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX1.Location = new System.Drawing.Point(371, 43);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(88, 21);
            this.labelX1.TabIndex = 49;
            this.labelX1.Text = "Tên loại vàng";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.ForeColor = System.Drawing.SystemColors.Window;
            this.panel1.Location = new System.Drawing.Point(-2, 349);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(771, 1);
            this.panel1.TabIndex = 50;
            // 
            // checkReadWeight
            // 
            this.checkReadWeight.AutoSize = true;
            this.checkReadWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkReadWeight.Location = new System.Drawing.Point(568, 239);
            this.checkReadWeight.Name = "checkReadWeight";
            this.checkReadWeight.Size = new System.Drawing.Size(94, 24);
            this.checkReadWeight.TabIndex = 54;
            this.checkReadWeight.Text = "Đọc cân";
            this.checkReadWeight.UseVisualStyleBackColor = true;
            this.checkReadWeight.CheckedChanged += new System.EventHandler(this.checkReadWeight_CheckedChanged);
            // 
            // labelX2
            // 
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX2.Location = new System.Drawing.Point(371, 85);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(62, 21);
            this.labelX2.TabIndex = 55;
            this.labelX2.Text = "Tiền công";
            // 
            // txtSalary
            // 
            // 
            // 
            // 
            this.txtSalary.Border.Class = "TextBoxBorder";
            this.txtSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalary.Location = new System.Drawing.Point(465, 84);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(118, 23);
            this.txtSalary.TabIndex = 56;
            this.txtSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalary_KeyPress);
            // 
            // labelX3
            // 
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX3.Location = new System.Drawing.Point(589, 85);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(73, 21);
            this.labelX3.TabIndex = 57;
            this.labelX3.Text = "x 1000VNĐ";
            // 
            // labelX4
            // 
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX4.Location = new System.Drawing.Point(668, 123);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(31, 21);
            this.labelX4.TabIndex = 58;
            this.labelX4.Text = "Chỉ";
            // 
            // labelX5
            // 
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX5.Location = new System.Drawing.Point(668, 167);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(31, 21);
            this.labelX5.TabIndex = 59;
            this.labelX5.Text = "Chỉ";
            // 
            // labelX6
            // 
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelX6.Location = new System.Drawing.Point(668, 212);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(31, 21);
            this.labelX6.TabIndex = 60;
            this.labelX6.Text = "Chỉ";
            // 
            // FrmProductInDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 410);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.txtSalary);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.checkReadWeight);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.txtGoldTypeName);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.txtGoldType);
            this.Controls.Add(this.lblGoldType);
            this.Controls.Add(this.txtWeightTotal);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblWeightTotal);
            this.Controls.Add(this.txtProductDesc);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.btnSaveClose);
            this.Controls.Add(this.txtWeightDiamand);
            this.Controls.Add(this.cboxStalls);
            this.Controls.Add(this.cboxBaseRule);
            this.Controls.Add(this.lblProductDesc);
            this.Controls.Add(this.lblStalls);
            this.Controls.Add(this.lblGoldGroup);
            this.Controls.Add(this.cboxGoldGroup);
            this.Controls.Add(this.lblBaseRule);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblWeightDiamand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmProductInDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHI TIẾT HÀNG NHẬP";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FrmProductInDetail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Controls.TextBoxX txtGoldType;
        private DevComponents.DotNetBar.LabelX lblGoldType;
        private DevComponents.DotNetBar.Controls.TextBoxX txtWeightTotal;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.LabelX lblWeightTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProductDesc;
        private DevComponents.DotNetBar.Controls.TextBoxX txtWeight;
        private DevComponents.DotNetBar.ButtonX btnSaveClose;
        private DevComponents.DotNetBar.Controls.TextBoxX txtWeightDiamand;
        private System.Windows.Forms.ComboBox cboxStalls;
        private System.Windows.Forms.ComboBox cboxBaseRule;
        private DevComponents.DotNetBar.LabelX lblProductDesc;
        private DevComponents.DotNetBar.LabelX lblStalls;
        private DevComponents.DotNetBar.LabelX lblGoldGroup;
        private System.Windows.Forms.ComboBox cboxGoldGroup;
        private DevComponents.DotNetBar.LabelX lblBaseRule;
        private DevComponents.DotNetBar.LabelX lblWeight;
        private DevComponents.DotNetBar.LabelX lblWeightDiamand;
        private System.IO.Ports.SerialPort portWeight;
        private DevComponents.DotNetBar.ButtonX btnSaveNew;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGoldTypeName;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkReadWeight;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSalary;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;

    }
}