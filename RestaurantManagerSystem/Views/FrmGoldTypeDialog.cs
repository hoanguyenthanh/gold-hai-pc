﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Process;
using GoldManager.Res;

namespace GoldManager.Views
{
    public partial class FrmGoldTypeDialog : Form
    {
        private DataTable DtGoldType;
        private int rowIndexSelected = -1;

        public string typeCode;
        public string typeName;

        public FrmGoldTypeDialog()
        {
            InitializeComponent();
        }

        private void FrmGoldTypeDialog_Load(object sender, EventArgs e)
        {
            this.dgvGoldType.AutoGenerateColumns = false;
            loadGoldType();
        }


        //Loại vàng
        protected void loadGoldType()
        {
            using (GoldType_Process process = new GoldType_Process())
            {
                process.intActionName = GoldType_Process.ACT_GET_GOLD_TYPE;
                process.intActive = MConts.DATA_ACTIVE;
                process.ExecuteProcess();

                DtGoldType = process.DtResult;

                if (DtGoldType != null) {
                    this.dgvGoldType.DataSource = DtGoldType;
                }
            }
        }

        private void dgvGoldType_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            rowIndexSelected = e.RowIndex;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (dgvGoldType != null && dgvGoldType.RowCount > 0){
                if (rowIndexSelected >= 0)
                {
                    DataGridViewRow dataRowSelect = dgvGoldType.Rows[rowIndexSelected];
                    if (dataRowSelect != null)
                    {
                        this.typeCode = dataRowSelect.Cells["GoldCode"].Value.ToString();
                        this.typeName = dataRowSelect.Cells["GoldDesc"].Value.ToString();
                    }
                }
            } else {
                typeCode = "";
                typeName = "";
            }

            this.Close();
        }

        private void dgvGoldType_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            if (gridView != null && gridView.CurrentRow != null)
            {
                rowIndexSelected = gridView.CurrentRow.Index;
            } else {
                rowIndexSelected = -1;
            }
        }

        private void dgvGoldType_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvGoldType != null && dgvGoldType.RowCount > 0 && dgvGoldType.CurrentRow != null)
            {
                DataGridViewRow dataRowSelect = dgvGoldType.CurrentRow;
                this.typeCode = dataRowSelect.Cells["GoldCode"].Value.ToString();
                this.typeName = dataRowSelect.Cells["GoldDesc"].Value.ToString();
            }
            else
            {
                typeCode = "";
                typeName = "";
            }

            this.Close();
        }
    }
}
