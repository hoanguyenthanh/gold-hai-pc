﻿using GoldManager.Process;
using GoldManager.Views.UserControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using GoldManager.Res;
using GoldManager.Models;

namespace GoldManager.Views
{
    public partial class FrmProductInput : FrmSimple
    {
        private long _productId;
        private DataTable DtProducts;
        private GoldType goldType;

        public FrmGoldTypeDialog goldTypeDialog;
        public FrmProductInDetail productDetail;

        public delegate void ReloadProduct(Int64 prodId);

        private BarTender.Application btApp;
        private BarTender.Format btFormat;
        private BarTender.QueryPrompt btQueryPrompt;

        public FrmProductInput()
        {
            InitializeComponent();
        }


        private void FrmProductIn_Load(object sender, EventArgs e)
        {
            btnPrintBarcode.Enabled = false;
            btApp = new BarTender.Application();

            loadProducts();
        }


        //Hàng nhập
        private void loadProducts()
        {
            using (Product_Process load = new Product_Process())
            {
                DtProducts = load.getListProduct(2000);
                if (DtProducts != null)
                {
                    grdProducts.AutoGenerateColumns = false;
                    grdProducts.DataSource = DtProducts;

                    this.selectRow(this._productId);
                }
                else
                {
                    btnModify.Enabled = false;
                }
            }
        }

        private void reloadProduct_event(long prodId)
        {
            this._productId = prodId;
            this.loadProducts();
        }


        private void selectRow(long prodId)
        {
            if (grdProducts != null && grdProducts.RowCount > 0 && prodId > 0)
            {
                foreach (DataGridViewRow rows in grdProducts.Rows)
                {
                    string _id = rows.Cells["ProductId"].Value.ToString();
                    if (_id.Equals(prodId.ToString()))
                    {
                        if (rows.Cells != null && rows.Cells[1] != null)
                        {
                            rows.Cells[1].Selected = true;
                        }
                        break;
                    }
                }
            }
        }

        protected override void disable()
        {
            base.disable();
            btnAdd.Enabled = false;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;
        }


        /// <summary>
        /// Sét trạng thái màn hình nhập liệu
        /// </summary>
        /// <param name="status">Trạng thái màn hình hiện tại</param>
        protected override void setStatusItems(string status)
        {
            base.setStatusItems(status);

            switch (status)
            {
                case STATUS_ITEMS_ADD:
                    btnAdd.Enabled = false;
                    btnModify.Enabled = false;
                    btnDelete.Enabled = false;
                    break;

                case STATUS_ITEMS_MOD:
                    btnAdd.Enabled = false;
                    btnModify.Enabled = false;
                    btnDelete.Enabled = false;
                    break;
                case STATUS_ITEMS_SAVE:
                case STATUS_ITEMS_CEL:
                case STATUS_ITEMS_NORMAL:
                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnDelete.Enabled = false;
                    break;
            }
        }

        /*
         * Get list ids product selected
         */
        private List<Int64> getListProductIdSelected()
        {
            List<Int64> ids = new List<Int64>();
            if (grdProducts != null)
            {
                foreach(DataGridViewRow row in grdProducts.Rows)
                {
                    bool chk = (row.Cells["chk"].Value != null && (bool)row.Cells["chk"].Value) ? true : false;
                    if (chk)
                    {
                        ids.Add((Int64)row.Cells["ProductId"].Value);
                    }
                }
            }

            return ids;
        }


        private void resetTextItem() {
            this._productId = -1;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            disable();
            using (FrmGoldTypeDialog goldTypeDialog = new FrmGoldTypeDialog())
            {
                goldTypeDialog.FormClosed += new FormClosedEventHandler(goldTypeDialog_FormClosing);
                goldTypeDialog.ShowDialog();
            }
        }

        void goldTypeDialog_FormClosing(object sender, EventArgs e){
            FrmGoldTypeDialog dialog = sender as FrmGoldTypeDialog;

            if (goldType == null){
                goldType = new GoldType();
            }
            this.goldType.clear();
            this.goldType.GoldCode = dialog.typeCode;
            this.goldType.GoldDesc = dialog.typeName;

            if (!String.IsNullOrEmpty(goldType.GoldCode)){
                resetTextItem();
                this._productId = -1;

                //Show detail
                productDetail = new FrmProductInDetail();
                productDetail.goldTypeDTO = goldType;
                productDetail.reloadParent += new ReloadProduct(reloadProduct_event);
                productDetail.FormClosed += new FormClosedEventHandler(productDetail_ClosedEvent);
                productDetail.Show();
            } else {
                setStatusItems(STATUS_ITEMS_NORMAL);
            }
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            this._productId = getProductIdClicked();
            if (this._productId == -1 || this.DtProducts == null || this.DtProducts.Rows.Count == 0)
            {
                setStatusItems(STATUS_ITEMS_CEL);
                return;
            }

            DataRow[] curRow = this.DtProducts.Select("ProductId = " + this._productId);
            if (curRow != null && curRow.Length == 1)
            {
                disable();
                 
                productDetail = new FrmProductInDetail(this._productId, curRow[0], null);
                productDetail.FormClosed += new FormClosedEventHandler(productDetail_ClosedEvent);
                productDetail.reloadParent += new ReloadProduct(reloadProduct_event);
                productDetail.Show();
            }
        }

        private void productDetail_ClosedEvent(object sender, EventArgs e)
        {
            this.loadProducts();   
            setStatusItems(STATUS_ITEMS_NORMAL);
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<Int64> listProductId = new List<Int64>();
            listProductId = getListProductIdSelected();

            if (listProductId.Count > 0)
            {
                if (showConfirmYesNo(string.Format("Bạn có chắc muốn xóa vĩnh viễn {0} sản phẩm đã chọn", listProductId.Count), "Xác nhận xóa"))
                {
                    int countDel = 0;
                    using (Product_Process process = new Product_Process())
                    {
                        foreach (Int64 id in listProductId)
                        {
                            if (process.deleteProduct(id) > 0)
                            {
                                countDel++;
                            }
                        }
                    }

                    if (countDel > 0)
                    {
                        showInfomation(string.Format("Đã xóa thành công {0} sản phẩm", countDel));
                        loadProducts();
                        setStatusItems(STATUS_ITEMS_NORMAL);
                    }
                }
            }
            else
            {
                showInfomation("Vui lòng chọn sản phẩm để xóa");
            }

            listProductId = null;
        }


        private void cancelUpdate()
        {
            setStatusItems(STATUS_ITEMS_CEL);
        }

        /// <summary>
        /// Click chọn checkbox đầu dòng
        /// </summary>
        private void grdProducts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex != -1 && e.ColumnIndex > -1)
            {
                DataGridView grid = sender as DataGridView;
                DataGridViewRow row = grid.Rows[e.RowIndex];
                int column = row.Cells["chk"].ColumnIndex;

                if (column == e.ColumnIndex)
                {
                    object value = row.Cells["chk"].Value;
                    row.Cells["chk"].Value = (value == null || (bool)value == false);

                    bool isCheck = false;   
                    foreach (DataGridViewRow _row in grid.Rows)
                    {
                        if (_row.Cells["chk"].Value != null && (bool)_row.Cells["chk"].Value)
                        {
                            isCheck = true;
                        }
                    }
                    btnDelete.Enabled = isCheck;
                    btnPrintBarcode.Enabled = isCheck;
                }
            }
        }

        private long getProductIdClicked()
        {
            if (grdProducts == null || grdProducts.CurrentRow == null)
            { 
                return -1;
            }

            Int64 id = 0;
            try
            {
                int index = grdProducts.CurrentRow.Index;
                id = Int64.Parse(grdProducts.Rows[index].Cells["ProductId"].Value.ToString());
            }
            catch (Exception ex)
            {
                showWarning("Không thể lấy ID sản phẩm đã chọn\n. Lỗi: " + ex.Message);
                id = -1;
            }

            return id;
        }


        //http://help.seagullscientific.com/2016/ja/Subsystems/ActiveX/Content/inputdatafilesetup.htm
        private void btnPrintBarcode_Click(object sender, EventArgs e)
        {
            if (this.grdProducts == null && this.grdProducts.Rows.Count == 0)
            {
                return;
            }

            String printer = Properties.Settings.Default.printer;
            String fileTem = "D:\\HaiGold Data\\CodeDev\\gold-hai-pc\\RestaurantManagerSystem\\Print\\btPrintStampHTN_L.btw";

            if (String.IsNullOrEmpty(printer))
            {
                showWarning("Bạn vui lòng cài đặt máy in");
                return;
            }

            if (!printer.Contains("BIXOLON"))
            {
                showInfomation("Vui lòng cài đặt máy in Tem BIXOLON trước khi in");
            }

            if (!File.Exists(fileTem))
            {
                showWarning("Tập tin mẫu tem không tồn tại");
                return;
            }

            foreach(DataGridViewRow row in grdProducts.Rows)
            {
                bool isCheck = row.Cells["chk"].Value == null ? false : (bool)row.Cells["chk"].Value;
                if (isCheck)
                {
                    string productCode = (string)row.Cells["ProductCode"].Value;
                    Int64 productId = (Int64)row.Cells["ProductId"].Value;
                    int result = -1;

                    using(Product_Process proPrinting = new Product_Process())
                    {
                        result = proPrinting.updatePrinting(productCode);
                    }

                    if (result > 0)
                    {
                        try
                        {
                            btFormat = btApp.Formats.Open(fileTem, false, printer);
                            
                            // Select the query prompt
                            btQueryPrompt = btFormat.Databases.QueryPrompts.GetQueryPrompt(1);

                            // Set the value of the query prompt
                            btQueryPrompt.Value = Convert.ToString(productId);

                            btFormat.PrintOut(true, false);
                        }
                        catch (Exception ex)
                        {
                            showWarning(ex.ToString());
                        }
                        finally
                        {
                            btFormat.Close (BarTender.BtSaveOptions.btDoNotSaveChanges);
                        }
                    }
                }
            }
        }


        private void FrmProductInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);
        }


        private void btnCopy_Click(object sender, EventArgs e)
        {
            FrmCopyData frmCopy = new FrmCopyData();
            frmCopy.reloadProduct += new ReloadProduct(reloadProduct_event);
            frmCopy.Show();
        }
    }
}
