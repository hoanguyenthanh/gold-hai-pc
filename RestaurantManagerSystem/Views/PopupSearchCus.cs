﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Process;
using GoldManager.Res;
using GoldManager.Models;

namespace GoldManager.Views
{
    public partial class PopupSearchCus : Form
    {
        DataTable tbCus = new DataTable();

        public Customer customer;



        public PopupSearchCus()
        {
            InitializeComponent();
        }



        private void PopupSearchCus_Load(object sender, EventArgs e)
        {
            using(Customer_Process ps = new Customer_Process())
            {
                tbCus = ps.getCustomers();
                dgvCus.AutoGenerateColumns = false;
                dgvCus.DataSource = tbCus;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            this.customer = this.getCusFromRow();
            Close();
        }


        private Customer getCusFromRow()
        {
            Customer cus = null;

            if (dgvCus.CurrentRow != null && dgvCus.CurrentRow.Index != -1)
            {
                cus = new Customer();
                cus.CusId = (int)dgvCus.CurrentRow.Cells["CusId"].Value;
                cus.CusCode = Utilities.DbNullToEmpty(dgvCus.CurrentRow.Cells["CusCode"].Value);
                cus.CusName = Utilities.DbNullToEmpty(dgvCus.CurrentRow.Cells["CusName"].Value);
                cus.Address = Utilities.DbNullToEmpty(dgvCus.CurrentRow.Cells["Address"].Value);
                cus.Phone = Utilities.DbNullToEmpty(dgvCus.CurrentRow.Cells["Phone"].Value);
                cus.CMND = Utilities.DbNullToEmpty(dgvCus.CurrentRow.Cells["CMND"].Value);
            }

            return cus;
        }

        private void txtCusName_TextChanged(object sender, EventArgs e)
        {
            DataView dv = tbCus.DefaultView;
            dv.RowFilter = string.Format("CusName like '%{0}%'", txtCusName.Text);
            dgvCus.DataSource = dv.ToTable();
        }

        private void dgvCus_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1){
                this.customer = this.getCusFromRow();
                Close();
            }
        }
    }
}
