﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Views.UserControl;
using GoldManager.Process;
using GoldManager.Res;

namespace GoldManager.Views
{
    public partial class FrmUser : FrmSimple
    {
        private string userNameCurrent;

        public FrmUser()
        {
            InitializeComponent();
        }

        private void FrmUser_Load(object sender, EventArgs e)
        {
            loadDropRole();
            loadUsers();
            setStatusItems(STATUS_ITEMS_NORMAL);
        }

        private void loadDropRole()
        {
            Dictionary<int, string> items = new Dictionary<int, string>();
            items.Add(MConts.USER_PERMISTION_NORMAL, MConts.USER_PERMISTION_NORMAL_NAME);
            items.Add(MConts.USER_PERMISTION_ADMIN, MConts.USER_PERMISTION_ADMIN_NAME);

            dropRole.DataSource = new BindingSource(items, null);
            dropRole.ValueMember = "Key";
            dropRole.DisplayMember = "Value";
        }

        private void loadUsers()
        {
            using (Users_Process proGet = new Users_Process())
            {
                proGet.intActionName = Users_Process.ACT_GET_USER;
                proGet.ExecuteProcess();

                this.dgvUser.AutoGenerateColumns = false;
                this.dgvUser.DataSource = proGet.DtResult;

                selectRow();
            }
        }

        private void selectRow()
        {
            if (!string.IsNullOrEmpty(userNameCurrent) && this.dgvUser != null && this.dgvUser.RowCount > 0)
            {
                this.dgvUser.ClearSelection();

                foreach (DataGridViewRow row in this.dgvUser.Rows)
                {
                    string _user = row.Cells["UserName"].Value.ToString();
                    if (_user.Equals(userNameCurrent))
                    {
                        row.Selected = true;
                    }
                }
            }
        }

        private DataGridViewRow getDataRow(string user)
        {
            if (!isNull(user) && this.dgvUser != null && this.dgvUser.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvUser.Rows)
                {
                    string _user = row.Cells["UserName"].Value.ToString();
                    if (_user.Equals(user))
                    {
                        return row;
                    }
                }
            }
            return null;
        }

        private void dgvUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvUsers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid != null && grid.CurrentRow != null && grid.CurrentRow.Index > -1)
            {
                bindDataRow2TextBox(grid.CurrentRow);   
            }
        }

        private void dgvUser_Sorted(object sender, EventArgs e)
        {
            selectRow();
        }

        private void bindDataRow2TextBox(DataGridViewRow row)
        {
            if (row != null)
            {
                this.userNameCurrent = row.Cells["UserName"].Value.ToString();

                this.txtUserName.Text = row.Cells["UserName"].Value.ToString();
                this.txtFullName.Text = row.Cells["FullName"].Value.ToString();
                this.txtPhone.Text = row.Cells["Mobile"].Value.ToString();
                this.dropRole.SelectedIndex = (int)row.Cells["IsAdmin"].Value;

                int active = (int)row.Cells["Active"].Value;
                this.chkActive.Checked = active == 1 ? true : false;

                this.txtUserName.Enabled = isNull(txtUserName.Text);
            }
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            setStatusItems(STATUS_ITEMS_ADD);
            userNameCurrent = "";
            txtUserName.Text = "";
            txtFullName.Text = "";
            txtPhone.Text = "";
            txtPassword.Text = "";
            txtPassword2.Text = "";
            chkActive.Checked = true;
            dropRole.SelectedIndex = 0;

            enableInputItem(true);
        }

        private void btnModUser_Click(object sender, EventArgs e)
        {
            if (!isNull(txtUserName.Text)){
                setStatusItems(STATUS_ITEMS_MOD);
                enableInputItem(true);
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
                txtPassword2.Enabled = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int iEffected = -1;
            if (string.IsNullOrEmpty(userNameCurrent))
            {
                if (isValidInsert())
                {
                    using (Users_Process insert = new Users_Process())
                    {
                        insert.intActionName = Users_Process.ACT_IST_USER;

                        insert.strUserName = this.txtUserName.Text;
                        insert.strFullName = this.txtFullName.Text;
                        insert.strPassword = this.txtPassword.Text;
                        insert.strMobile = this.txtPhone.Text;
                        insert.intActive = this.chkActive.Checked ? MConts.DATA_ACTIVE : MConts.DATA_UNACTIVE;

                        string select = this.dropRole.SelectedValue.ToString();
                        string cont = MConts.USER_PERMISTION_ADMIN.ToString();
                        insert.intIsAdmin = select.Equals(cont) ? MConts.USER_PERMISTION_ADMIN : MConts.USER_PERMISTION_NORMAL;

                        iEffected = insert.ExecuteProcess();
                    }
                }
            }
            else if (!string.IsNullOrEmpty(userNameCurrent))
            {
                if (isValidUpdate())
                {
                    using (Users_Process update = new Users_Process())
                    {
                        update.intActionName = Users_Process.ACT_UPD_USER;

                        update.strUserName = this.txtUserName.Text;
                        update.strFullName = this.txtFullName.Text;
                        update.strMobile = this.txtPhone.Text;
                        update.intActive = this.chkActive.Checked ? MConts.DATA_ACTIVE : MConts.DATA_UNACTIVE;

                        string select = this.dropRole.SelectedValue.ToString();
                        string cont = MConts.USER_PERMISTION_ADMIN.ToString();
                        update.intIsAdmin = select.Equals(cont) ? MConts.USER_PERMISTION_ADMIN : MConts.USER_PERMISTION_NORMAL;

                        iEffected = update.ExecuteProcess();
                    }
                }
            }

            if (iEffected > 0)
            {
                userNameCurrent = this.txtUserName.Text;
                setStatusItems(STATUS_ITEMS_NORMAL);
                loadUsers();
            }
            else if (iEffected == 0)
            {
                showWarning("Không thể thêm mới hoặc cập nhật! Vui lòng kiểm tra lại thông tin nhập");
            }
        }

        

        private bool isValidInsert()
        {
            if (isNull(txtFullName.Text)
                || isNull(txtUserName.Text)
                || isNull(txtPassword.Text)
                || isNull(txtPassword2.Text)
                || isNull(txtPhone.Text))
            {
                showWarning("Vui lòng nhập đầy đủ các trường");
                return false;
            }

            if (isExistUser(this.txtUserName.Text))
            {
                showWarning(string.Format("Tên đăng nhập \"{0}\" đã tồn tại, vui lòng kiểm tra lại", txtUserName.Text));
                return false;
            }

            if (!this.txtPassword.Text.Equals(this.txtPassword2.Text))
            {
                showWarning("Vui lòng nhập 2 mặt khẩu khớp nhau!");
                return false;
            }
            return true;
        }

        private bool isValidUpdate()
        {
            if (isNull(txtFullName.Text)
                || isNull(txtUserName.Text)
                || isNull(txtPhone.Text))
            {
                showWarning("Vui lòng nhập các trường: \"Họ và tên\", \"Quyền\", \"Số điện thoại\"");
                return false;
            }
            return true;
        }

        private bool isExistUser(string userName)
        {
            using (Users_Process process = new Users_Process())
            {
                process.intActionName = Users_Process.ACT_CHECK_EXIST_USER;
                process.strUserName = userName;
                int iResult = process.ExecuteProcess();
               
                return iResult > 0;  
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            setStatusItems(STATUS_ITEMS_NORMAL);
            if (isNull(this.userNameCurrent))
            {
                txtUserName.Text = "";
                txtFullName.Text = "";
                txtPhone.Text = "";
                txtPassword.Text = "";
                txtPassword2.Text = "";
                chkActive.Checked = true;
                dropRole.SelectedIndex = 0;
            }
            else 
            {
                DataGridViewRow row = getDataRow(this.userNameCurrent);
                if (row != null)
                {
                    bindDataRow2TextBox(row);
                }
            }
        }

        protected void txtNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || e.KeyChar == 8);
        }

        protected override void setStatusItems(string status)
        {
            switch(status)
            {
                case STATUS_ITEMS_NORMAL:
                    btnAdd.Enabled = true;
                    btnUpdate.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;

                    enableInputItem(false);
                    break;
                case STATUS_ITEMS_ADD:
                case STATUS_ITEMS_MOD:
                    btnAdd.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void enableInputItem(bool enable)
        {
            txtUserName.Enabled = enable;
            txtFullName.Enabled = enable;
            txtPhone.Enabled = enable;
            txtPassword.Enabled = enable;
            txtPassword2.Enabled = enable;
            chkActive.Enabled = enable;
            dropRole.Enabled = enable;
        }

        private bool isNull(string value)
        {
            return string.IsNullOrEmpty(value);
        }
    }
}
