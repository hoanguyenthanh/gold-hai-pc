﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Process;
using GoldManager.Views.UserControl;
using GoldManager.Res;
using GoldManager.Models;

namespace GoldManager.Views
{
    public partial class FrmCustomer : FrmSimple
    {
        private int _cusId;
        
        public FrmCustomer()
        {
            InitializeComponent();
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            getCustomers();
        }


        private int getCustomers()
        {
            using(Customer_Process process = new Customer_Process())
            {
                DataTable data = process.getCustomers();

                dgvCustomer.AutoGenerateColumns = false;  
                dgvCustomer.DataSource = data;
            }

            return dgvCustomer.RowCount;
        }

   
        private void dgvCustomer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvCustomer.CurrentRow != null && e.RowIndex != -1)
            {
                bindDataRowToText(dgvCustomer.CurrentRow);
            }
            else
            {
                _cusId = -1;
            }
        }

        private void bindDataRowToText(DataGridViewRow row)
        {
            if (row != null && row.Index != -1 && !string.IsNullOrEmpty(row.Cells["CusId"].Value.ToString()))
            {
                ConvertGridViewRow cv = new ConvertGridViewRow(row);

                _cusId = cv.getInt("CusId");


                txtCusCode.Text = cv.getString("CusCode");
                txtFullName.Text = cv.getString("CusName");
                txtAddress.Text = cv.getString("Address");
                txtCMND.Text = cv.getString("CMND");
                txtPhone.Text = cv.getString("Phone");
                txtNote.Text = cv.getString("Note");

                btnSave.Enabled = true;
            } else {
                _cusId = -1;
                txtCusCode.Text = "";
                txtFullName.Text = "";
                txtAddress.Text = "";
                txtCMND.Text = "";
                txtPhone.Text = "";
                txtNote.Text = "";

                btnSave.Enabled = false;
            }
        }

        private void txtCMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar)) || txtCMND.Text.ToString().Length > 12;
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar)) || txtPhone.Text.ToString().Length > 12;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            this._cusId = -1;

            txtCusCode.Text = "";
            txtFullName.Text = "";
            txtAddress.Text = "";
            txtCMND.Text = "";
            txtPhone.Text = "";
            txtNote.Text = "";

            btnSave.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_cusId == -1)
            {
                showInfomation("Vui lòng chọn lại 1 dòng để xóa");
                return;
            }
                

            if (showConfirmYesNo("Bạn có chắc muốn xóa?", "Xác nhận xóa"))
            {
                using (Customer_Process process = new Customer_Process())
                {
                    Customer cus = new Customer();
                    cus.CusId = _cusId;

                    ResultObject result = process.updateCus("delete", cus);

                    if (result.Result == -1)
                    {
                        MessageBox.Show(result.ErrorDesc, "Lỗi xóa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    } 
                    else 
                    {
                        getCustomers();
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (Customer_Process process = new Customer_Process())
            {
                Customer cus = new Customer();

                cus.CusId = this._cusId;
                cus.CusCode = txtCusCode.Text;
                cus.CusName = txtFullName.Text;
                cus.Address = txtAddress.Text;
                cus.CMND = txtCMND.Text;
                cus.Phone = txtPhone.Text;
                cus.Note = txtNote.Text;

                string method = this._cusId > 0 ? "update" : "insert";

                ResultObject result = process.updateCus(method, cus);

                if (result.Result == -1)
                {
                    showWarning("Không thể thêm mới hoặc cập nhật. " + result.ErrorDesc);
                } else {
                    getCustomers();
                }
            }
        }

        private void btnIgnore_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = this.dgvCustomer.CurrentRow;
            bindDataRowToText(row);
            btnSave.Enabled = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        
    }
}
