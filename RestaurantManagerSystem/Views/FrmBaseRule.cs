﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Views.UserControl;
using GoldManager.Res;
using GoldManager.Process;

namespace GoldManager.Views
{
    public partial class FrmBaseRule : FrmSimple
    {
        private string ruleNameCurrent;

        public FrmBaseRule()
        {
            InitializeComponent();
        }

        private void FrmBaseRule_Load(object sender, EventArgs e)
        {
            loadBaseRule();
        }

        private void loadBaseRule()
        {
            using (BaseRule_Process proGet = new BaseRule_Process())
            {
                proGet.intActionName = BaseRule_Process.ACT_GET_RULE;
                proGet.intActive = MConts.DATA_ACTIVE_NOT_CHECK;
                proGet.ExecuteProcess();

                grdBaseRule.AutoGenerateColumns = false;
                grdBaseRule.DataSource = proGet.DtResult;

                selectRow();
            }
        }

        private void selectRow()
        {
            if (!string.IsNullOrEmpty(ruleNameCurrent) && grdBaseRule != null && grdBaseRule.RowCount > 0)
            {
                grdBaseRule.ClearSelection();

                foreach (DataGridViewRow row in grdBaseRule.Rows)
                {
                    string ruleName = row.Cells["BaseRuleName"].Value.ToString();

                    if (ruleName.Equals(ruleNameCurrent))
                    {
                        row.Selected = true;
                    }
                }
            }
        }

        private void bindDataRow2TextBox(DataGridViewRow row)
        {
            ruleNameCurrent = row.Cells["BaseRuleName"].Value.ToString();

            txtBaseRuleName.Text = row.Cells["BaseRuleName"].Value.ToString();
            txtBaseRuleDesc.Text = row.Cells["BaseRuleDesc"].Value.ToString();
            
            string active = row.Cells["Active"].Value.ToString();
            cbxActive.Checked = active.Equals(MConts.DATA_ACTIVE.ToString()) ? true : false;
        }

        private void grdBaseRule_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdBaseRule_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid != null && grid.CurrentRow != null && grid.CurrentRow.Index > -1)
            {
                bindDataRow2TextBox(grid.CurrentRow);
            }
        }

        private void grdBaseRule_Sorted(object sender, EventArgs e)
        {
            selectRow();
        }


        private bool isValidInsert()
        {
            if (isExistRuleName())
            {
                showWarning(string.Format("Tên TCCS \"{0}\" đã tồn tại, vui lòng kiểm tra lại", txtBaseRuleName.Text));
                return false;
            }
            return true;
        }

        private bool isValidUpdate()
        {
            if (string.IsNullOrEmpty(txtBaseRuleDesc.Text))
            {
                showWarning("Vui lòng nhập \"Mô tả\"");
                return false;
            }
            return true;
        }

        private bool isExistRuleName()
        {
            using (BaseRule_Process process = new BaseRule_Process())
            {
                process.intActionName = BaseRule_Process.ACT_EXISTS_NAME;
                process.strBaseRuleName = txtBaseRuleName.Text;
                process.ExecuteProcess();

                return process.resultYesNo;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtBaseRuleDesc.Text = "";
            txtBaseRuleName.Text = "";
            cbxActive.Checked = true;
            setStatusItems(STATUS_ITEMS_ADD);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.ruleNameCurrent))
            {
                setStatusItems(STATUS_ITEMS_MOD);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int iResult = -1;
            
            if (string.IsNullOrEmpty(this.ruleNameCurrent))
            {
                if (isValidInsert())
                {
                    using(BaseRule_Process proIst = new BaseRule_Process())
                    {
                        proIst.intActionName = BaseRule_Process.ACT_IST_RULE;
                        proIst.strBaseRuleName = txtBaseRuleName.Text;
                        proIst.strBaseRuleDesc = txtBaseRuleDesc.Text;
                        proIst.intActive = cbxActive.Checked ? MConts.DATA_ACTIVE : MConts.DATA_UNACTIVE;

                        iResult = proIst.ExecuteProcess();
                    }
                }
            }
            else if (!string.IsNullOrEmpty(this.ruleNameCurrent))
            {
                if (isValidUpdate())
                {
                    using (BaseRule_Process proUpd = new BaseRule_Process())
                    {
                        proUpd.intActionName = BaseRule_Process.ACT_UPD_RULE;
                        proUpd.strBaseRuleName = txtBaseRuleName.Text;
                        proUpd.strBaseRuleDesc = txtBaseRuleDesc.Text;
                        proUpd.intActive = cbxActive.Checked ? MConts.DATA_ACTIVE : MConts.DATA_UNACTIVE;

                        iResult = proUpd.ExecuteProcess();
                    }
                }
            }

            if (iResult > 0)
            {
                this.ruleNameCurrent = txtBaseRuleName.Text;
                setStatusItems(STATUS_ITEMS_SAVE);
                loadBaseRule();
            } 
            else if (iResult == 0)
            {
                showWarning("Không thể thêm hoặc cập nhật!");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            setStatusItems(STATUS_ITEMS_CEL);
        }


        protected override void setStatusItems(string status)
        {
            switch (status)
            {
                case STATUS_ITEMS_ADD:
                    txtBaseRuleName.Enabled = true;
                    txtBaseRuleDesc.Enabled = true;
                    cbxActive.Enabled = true;

                    btnAdd.Enabled = false;
                    btnModify.Enabled = false;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    break;

                case STATUS_ITEMS_MOD:
                    txtBaseRuleName.Enabled = false;
                    txtBaseRuleDesc.Enabled = true;
                    cbxActive.Enabled = true;

                    btnAdd.Enabled = false;
                    btnModify.Enabled = false;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    break;
                case STATUS_ITEMS_SAVE:
                    txtBaseRuleName.Enabled = false;
                    txtBaseRuleDesc.Enabled = false;
                    cbxActive.Enabled = false;

                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    break;
                case STATUS_ITEMS_CEL:
                    txtBaseRuleName.Enabled = false;
                    txtBaseRuleDesc.Enabled = false;
                    cbxActive.Enabled = false;

                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    break;

                case STATUS_ITEMS_NORMAL:
                    txtBaseRuleName.Enabled = false;
                    txtBaseRuleDesc.Enabled = false;
                    cbxActive.Enabled = false;

                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    break;
            }
        }
    }
}
