﻿namespace GoldManager.Views.UserControl
{
    partial class FrmSimple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FrmSimple
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(788, 603);
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MinimizeBox = false;
            this.Name = "FrmSimple";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmSimple";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.FrmSimple_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSimple_FormClosing);
            this.Load += new System.EventHandler(this.FrmSimple_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}