﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GoldManager.Views.UserControl
{
    public interface IFormCallBack
    {
        //Callback for close window from Home
        void closeForm(Form formName);

        //Callback for update Menu
        void activatedForm(string nameForm);
    }
}
