﻿using System;
using System.Windows.Forms;

namespace GoldManager.Views.UserControl
{
    public partial class FrmSimple : Form
    {
        protected const string STATUS_ITEMS_ADD = "STATUS_ITEMS_ADD";
        protected const string STATUS_ITEMS_MOD = "STATUS_ITEMS_MOD";
        protected const string STATUS_ITEMS_DEL = "STATUS_ITEMS_DEL";
        protected const string STATUS_ITEMS_SAVE = "STATUS_ITEMS_SAVE";
        protected const string STATUS_ITEMS_CEL = "STATUS_ITEMS_CEL";
        protected const string STATUS_ITEMS_NORMAL = "STATUS_ITEMS_NORMAL";


        /// <summary>
        /// Trạng thái hiện tại của màn hình
        /// </summary>
        protected string mStatus {get; set;}



        /// <summary>
        /// Dòng hiện tại đang được cập nhật dữ liệu
        /// </summary>
        protected int mCurrentRowUpdate { get; set; }



        private IFormCallBack mFormCallBack;

        public FrmSimple()
        {
            InitializeComponent();
        }


        private void FrmSimple_Load(object sender, EventArgs e)
        {
            setStatusItems(STATUS_ITEMS_NORMAL);
        }

        private void FrmSimple_Activated(object sender, EventArgs e)
        {
            if (this.mFormCallBack != null)
            {
                this.mFormCallBack.activatedForm(this.Name);
            }
        }


        /*
         * Set form callback
         */
        public void setFormCallBack(IFormCallBack callback)
        {
            this.mFormCallBack = callback;
        }


        private void FrmSimple_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing){

                DialogResult result = MessageBox.Show("Bạn có chắc muốn đóng màn hình này?", "Xác nhận", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (mFormCallBack != null)
                    {
                        mFormCallBack.closeForm(this);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        /*
         * Show alert confirm delete
         */
        protected bool showConfirmDelete()
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn xóa?", "Xác nhận xóa", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.OK)
            {
                return true;
            }

            return false;
        }


        protected bool showConfirmYesNo(string msg, string title)
        {
            DialogResult result = MessageBox.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                return true;
            }
            return false;
        }


        /*
         * Show warning
         */
        protected void showWarning(string msg)
        {
            MessageBox.Show(msg, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }


        protected void showInfomation(string msg)
        {
            MessageBox.Show(msg, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        protected virtual void setStatusItems(string status)
        {
            mStatus = status;
        }

        protected virtual void disable()
        {
        }
    }
}
