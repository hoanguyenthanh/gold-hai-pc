﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Views.UserControl;
using GoldManager.Process;
using GoldManager.Res;

namespace GoldManager.Views
{
    public partial class FrmGoldType : FrmSimple
    {
        private string goldCodeCurrent = "";
        
        public FrmGoldType()
        {
            InitializeComponent();
        }

        private void FrmGoldType_Load(object sender, EventArgs e)
        {
            getListGoldType();
            setEnableItems();

            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        protected override void setStatusItems(string status)
        {

            switch (status)
            {
                case STATUS_ITEMS_MOD:
                    this.btnSave.Enabled = true;
                    this.btnCancel.Enabled = true;
                    break;
                case STATUS_ITEMS_ADD:
                case STATUS_ITEMS_CEL:
                case STATUS_ITEMS_NORMAL:
                    this.btnSave.Enabled = false;
                    this.btnCancel.Enabled = false;
                    break;
            }
        }



        private void getListGoldType()
        {
            using (GoldType_Process process = new GoldType_Process())
            {
                process.intActionName = GoldType_Process.ACT_GET_GOLD_TYPE;
                process.ExecuteProcess();

                this.dgvGoldType.AutoGenerateColumns = false;
                this.dgvGoldType.DataSource = process.DtResult;

                selectRow ();
            }
        }

        private void bindDataRow2TextBox(DataGridViewRow row)
        {
            if (row != null)
            {

                this.goldCodeCurrent = row.Cells["GoldCode"].Value.ToString();

                this.txtGoldCode.Text = row.Cells["GoldCode"].Value.ToString();
                this.txtGoldDesc.Text = row.Cells["GoldDesc"].Value.ToString();
                this.txtPriceSell.Text = row.Cells["PriceSell"].Value.ToString();
                this.txtPriceBuy.Text = row.Cells["PriceBuy"].Value.ToString();
                this.txtNote.Text = row.Cells["Notes"].Value.ToString();
                this.txtAgeGold.Text = row.Cells["Old"].Value.ToString();

                int active = (int)row.Cells["Active"].Value;
                this.chkActive.Checked = active == 1 ? true : false;
            }
        }

        private void selectRow (){
            if (!string.IsNullOrEmpty(goldCodeCurrent) && this.dgvGoldType != null && this.dgvGoldType.Rows.Count > 0)
            {
                dgvGoldType.ClearSelection();
                foreach (DataGridViewRow row in this.dgvGoldType.Rows)
                {
                    string _code = row.Cells["GoldCode"].Value.ToString();
                    if (goldCodeCurrent.Equals(_code))
                    {
                        row.Selected = true;
                    }
                }
            }
        }

        private void dgvGoldType_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid != null && grid.CurrentRow != null && grid.CurrentRow.Index > -1)
            {
                this.bindDataRow2TextBox(grid.CurrentRow);
            }
        }

        


        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.goldCodeCurrent = "";

            this.btnAdd.Enabled = false;
            this.btnUpdate.Enabled = false;

            this.btnSave.Enabled = true;
            this.btnCancel.Enabled = true;

            this.txtGoldCode.Text = "";
            this.txtGoldDesc.Text = "";
            this.txtNote.Text = "";
            this.txtPriceBuy.Text = "";
            this.txtPriceSell.Text = "";
            this.txtAgeGold.Text = "";
            this.chkActive.Checked = true;

            setEnableItems();
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dgvGoldType.CurrentRow != null)
            {
                mCurrentRowUpdate = dgvGoldType.CurrentRow.Index;
                setStatusItems(STATUS_ITEMS_MOD);

                btnAdd.Enabled = false;
                btnUpdate.Enabled = false;

                this.btnSave.Enabled = true;
                this.btnCancel.Enabled = true;

                setEnableItems();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int iEffected = -1;

            if (string.IsNullOrEmpty(this.goldCodeCurrent))
            {
                if (isValidInsert()){
                    using (GoldType_Process insert = new GoldType_Process())
                    {
                        insert.intActionName = GoldType_Process.ACT_IST_GOLD_TYPE;

                        insert.strGoldCode = this.txtGoldCode.Text;
                        insert.strGoldDesc = this.txtGoldDesc.Text;
                        insert.strNote = this.txtNote.Text;
                        insert.strOld = this.txtAgeGold.Text;
                        insert.douPriceBuy = double.Parse(this.txtPriceBuy.Text);
                        insert.douPriceSell = double.Parse(this.txtPriceSell.Text);
                        insert.intActive = this.chkActive.Checked ? 1 : 0;

                        iEffected = insert.ExecuteProcess();
                    }
                }
            }
            else if (!string.IsNullOrEmpty(this.goldCodeCurrent))
            {
                if (isValidUpdate())
                {
                    using (GoldType_Process update = new GoldType_Process())
                    {
                        update.intActionName = GoldType_Process.ACT_UPD_GOLD_TYPE;

                        update.strGoldCode = this.txtGoldCode.Text;
                        update.strGoldDesc = this.txtGoldDesc.Text;
                        update.strNote = this.txtNote.Text;
                        update.strOld = this.txtAgeGold.Text;
                        update.douPriceBuy = double.Parse(this.txtPriceBuy.Text);
                        update.douPriceSell = double.Parse(this.txtPriceSell.Text);
                        update.intActive = this.chkActive.Checked ? 1 : 0;

                        iEffected = update.ExecuteProcess();
                    }
                }
            }

            if (iEffected > 0)
            {
                this.goldCodeCurrent = this.txtGoldCode.Text;

                this.btnSave.Enabled = false;
                this.btnCancel.Enabled = false;

                this.btnAdd.Enabled = true;
                this.btnUpdate.Enabled = true;

                this.setEnableItems();
                this.getListGoldType();
            }
            else if (iEffected == 0)
            {
                showWarning("Không thêm hoặc cập nhật loại vàng");
            }
        }

        private bool isValidInsert(){
            if (this.txtGoldCode.Text.Length == 0
                    || this.txtGoldDesc.Text.Length == 0
                    || this.txtPriceBuy.Text.Length == 0
                    || this.txtPriceSell.Text.Length == 0
                    || this.txtAgeGold.Text.Length == 0)
            {
                MessageBox.Show("Vui lòng nhập các trường: \"Mã vàng\", \"Diễn giải\", \"Giá bán ra\", \"Giá mua vào\", \"Tuổi vàng\"");
                return false;
            }

            if (checkExistsGoldType())
            {
                MessageBox.Show("Mã vàng này đã tồn tại, vui lòng kiểm tra lại");
                return false;
            }
            return true;
        }


        private bool isValidUpdate(){
            if (string.IsNullOrEmpty(this.txtGoldDesc.Text)
                    || string.IsNullOrEmpty(this.txtPriceBuy.Text)
                    || string.IsNullOrEmpty(this.txtPriceSell.Text)
                    || string.IsNullOrEmpty(this.txtAgeGold.Text))
            {
                MessageBox.Show("Vui lòng nhập các trường: \"Diễn giải\", \"Giá bán ra\", \"Giá mua vào\", \"Tuổi vàng\"");
                return false;
            }
            return true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
            btnUpdate.Enabled = true;

            this.btnSave.Enabled = false;
            this.btnCancel.Enabled = false;

            setStatusItems(STATUS_ITEMS_NORMAL);

            dgvGoldType_SelectionChanged(dgvGoldType, e);
        }


        private void dgvGoldType_SelectionChanged(object sender, EventArgs e)
        {
            setEnableItems();
        }

        private void setEnableItems()
        {
            if (btnSave.Enabled)
            {
                txtGoldCode.Enabled = string.IsNullOrEmpty(goldCodeCurrent) ? true : false;
                txtGoldDesc.Enabled = true;
                txtPriceSell.Enabled = true;
                txtPriceBuy.Enabled = true;
                txtNote.Enabled = true;
                txtAgeGold.Enabled = true;
                chkActive.Enabled = true;
                txtAgeGold.Enabled = true;


                if (dgvGoldType.CurrentRow != null)
                {
                    int index = dgvGoldType.CurrentRow.Index;

                    if (mStatus == STATUS_ITEMS_MOD && index >= 0 && mCurrentRowUpdate != index)
                    {
                        txtGoldCode.Enabled = false;
                        txtGoldDesc.Enabled = false;
                        txtPriceSell.Enabled = false;
                        txtPriceBuy.Enabled = false;
                        txtNote.Enabled = false;
                        chkActive.Enabled = false;
                        txtAgeGold.Enabled = false;
                    }
                }
            }
            else
            {
                txtGoldCode.Enabled = false;
                txtGoldDesc.Enabled = false;
                txtPriceSell.Enabled = false;
                txtPriceBuy.Enabled = false;
                txtNote.Enabled = false;
                txtAgeGold.Enabled = false;
                chkActive.Enabled = false;
            }
        }

        private bool checkExistGoldTypeRefe()
        {
            using (GoldType_Process process = new GoldType_Process())
            {
                process.intActionName = GoldType_Process.ACT_EXISTS_REFE;
                process.strGoldCode = this.txtGoldCode.Text;

                int iResult = process.ExecuteProcess();
                return iResult > 0;
            }
        }

        private bool checkExistsGoldType()
        {
            using (GoldType_Process process = new GoldType_Process())
            {
                process.intActionName = GoldType_Process.ACT_EXISTS_GOLD_TYPE;
                process.strGoldCode = this.txtGoldCode.Text;

                int iResult = process.ExecuteProcess();
                return iResult > 0;
            }
        }

        /*
        * Validation only input text number
        */
        protected void txtNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !Utilities.isOnlyNumber(sender, e);
        }
    }
}
