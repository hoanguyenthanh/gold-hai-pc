﻿namespace GoldManager.Views
{
    partial class FrmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InputToolStripMenuItem_ProudctIn = new System.Windows.Forms.ToolStripMenuItem();
            this.InvoiceToolStripMenuItem_Invoice = new System.Windows.Forms.ToolStripMenuItem();
            this.WholeSaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhMucToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goldTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goldGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tCCSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heThongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserInfo = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem,
            this.danhMucToolStripMenuItem,
            this.heThongToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1435, 33);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem
            // 
            this.toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InputToolStripMenuItem_ProudctIn,
            this.InvoiceToolStripMenuItem_Invoice,
            this.WholeSaleToolStripMenuItem});
            this.toolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.toolStripMenuItem.Name = "toolStripMenuItem";
            this.toolStripMenuItem.Size = new System.Drawing.Size(104, 29);
            this.toolStripMenuItem.Text = "Giao dịch";
            // 
            // InputToolStripMenuItem_ProudctIn
            // 
            this.InputToolStripMenuItem_ProudctIn.Name = "InputToolStripMenuItem_ProudctIn";
            this.InputToolStripMenuItem_ProudctIn.Size = new System.Drawing.Size(247, 30);
            this.InputToolStripMenuItem_ProudctIn.Text = "Nhập hàng";
            this.InputToolStripMenuItem_ProudctIn.Click += new System.EventHandler(this.InputToolStripMenuItem_ProudctIn_Click);
            // 
            // InvoiceToolStripMenuItem_Invoice
            // 
            this.InvoiceToolStripMenuItem_Invoice.Name = "InvoiceToolStripMenuItem_Invoice";
            this.InvoiceToolStripMenuItem_Invoice.Size = new System.Drawing.Size(247, 30);
            this.InvoiceToolStripMenuItem_Invoice.Text = "Danh sách hóa đơn";
            this.InvoiceToolStripMenuItem_Invoice.Click += new System.EventHandler(this.InvoiceToolStripMenuItem_Invoice_Click);
            // 
            // WholeSaleToolStripMenuItem
            // 
            this.WholeSaleToolStripMenuItem.Name = "WholeSaleToolStripMenuItem";
            this.WholeSaleToolStripMenuItem.Size = new System.Drawing.Size(247, 30);
            this.WholeSaleToolStripMenuItem.Text = "Toa hàng";
            this.WholeSaleToolStripMenuItem.Click += new System.EventHandler(this.WholeSaleToolStripMenuItem_Click);
            // 
            // danhMucToolStripMenuItem
            // 
            this.danhMucToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goldTypeToolStripMenuItem,
            this.goldGroupToolStripMenuItem,
            this.tCCSToolStripMenuItem,
            this.userToolStripMenuItem,
            this.CustomerToolStripMenuItem});
            this.danhMucToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.danhMucToolStripMenuItem.Name = "danhMucToolStripMenuItem";
            this.danhMucToolStripMenuItem.Size = new System.Drawing.Size(110, 29);
            this.danhMucToolStripMenuItem.Text = "Danh mục";
            // 
            // goldTypeToolStripMenuItem
            // 
            this.goldTypeToolStripMenuItem.Name = "goldTypeToolStripMenuItem";
            this.goldTypeToolStripMenuItem.Size = new System.Drawing.Size(226, 30);
            this.goldTypeToolStripMenuItem.Text = "Loại vàng và giá";
            this.goldTypeToolStripMenuItem.Click += new System.EventHandler(this.goldTypeToolStripMenuItem_Click);
            // 
            // goldGroupToolStripMenuItem
            // 
            this.goldGroupToolStripMenuItem.Name = "goldGroupToolStripMenuItem";
            this.goldGroupToolStripMenuItem.Size = new System.Drawing.Size(226, 30);
            this.goldGroupToolStripMenuItem.Text = "Nhóm vàng";
            this.goldGroupToolStripMenuItem.Click += new System.EventHandler(this.goldGroupToolStripMenuItem_Click);
            // 
            // tCCSToolStripMenuItem
            // 
            this.tCCSToolStripMenuItem.Name = "tCCSToolStripMenuItem";
            this.tCCSToolStripMenuItem.Size = new System.Drawing.Size(226, 30);
            this.tCCSToolStripMenuItem.Text = "Tiêu chuẩn cơ sở";
            this.tCCSToolStripMenuItem.Click += new System.EventHandler(this.tCCSToolStripMenuItem_Click);
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(226, 30);
            this.userToolStripMenuItem.Text = "Người dùng";
            this.userToolStripMenuItem.Click += new System.EventHandler(this.userToolStripMenuItem_Click);
            // 
            // CustomerToolStripMenuItem
            // 
            this.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem";
            this.CustomerToolStripMenuItem.Size = new System.Drawing.Size(226, 30);
            this.CustomerToolStripMenuItem.Text = "Khánh hàng";
            this.CustomerToolStripMenuItem.Click += new System.EventHandler(this.CustomerToolStripMenuItem_Click);
            // 
            // heThongToolStripMenuItem
            // 
            this.heThongToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printerToolStripMenuItem});
            this.heThongToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.heThongToolStripMenuItem.Name = "heThongToolStripMenuItem";
            this.heThongToolStripMenuItem.Size = new System.Drawing.Size(102, 29);
            this.heThongToolStripMenuItem.Text = "Hệ thống";
            // 
            // printerToolStripMenuItem
            // 
            this.printerToolStripMenuItem.Name = "printerToolStripMenuItem";
            this.printerToolStripMenuItem.Size = new System.Drawing.Size(147, 30);
            this.printerToolStripMenuItem.Text = "Thiết bị";
            this.printerToolStripMenuItem.Click += new System.EventHandler(this.printerToolStripMenuItem_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.ItemSize = new System.Drawing.Size(0, 30);
            this.tabControl.Location = new System.Drawing.Point(0, 33);
            this.tabControl.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Drawing.Point(0, 0);
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1435, 551);
            this.tabControl.TabIndex = 2;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabForms_SelectedIndexChanged);
            this.tabControl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControl_MouseClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(9, 590);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Xin chào:";
            // 
            // lblUserInfo
            // 
            this.lblUserInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserInfo.AutoSize = true;
            this.lblUserInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblUserInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblUserInfo.Location = new System.Drawing.Point(84, 590);
            this.lblUserInfo.Name = "lblUserInfo";
            this.lblUserInfo.Size = new System.Drawing.Size(71, 17);
            this.lblUserInfo.TabIndex = 6;
            this.lblUserInfo.Text = "username";
            // 
            // FrmHome
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1435, 615);
            this.Controls.Add(this.lblUserInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.Black;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmHome";
            this.Text = "Quản lý bán hàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmHome_FormClosed);
            this.Load += new System.EventHandler(this.FrmHome_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem InputToolStripMenuItem_ProudctIn;
        private System.Windows.Forms.ToolStripMenuItem InvoiceToolStripMenuItem_Invoice;
        private System.Windows.Forms.ToolStripMenuItem danhMucToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserInfo;
        private System.Windows.Forms.ToolStripMenuItem goldTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goldGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tCCSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heThongToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStripMenuItem CustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WholeSaleToolStripMenuItem;
    }
}