﻿using GoldManager.Views.UserControl;
using System;
using System.Drawing;
using System.Windows.Forms;
using GoldManager.Models;
using System.Collections.Generic;

namespace GoldManager.Views
{
    public partial class FrmHome : Form, IFormCallBack
    {
        private FrmInvoice mFrmInvoice;
        private FrmProductInput mFrmProductIn;
        private FrmPrinter mFrmPrinter;
        private FrmGoldGroup mFrmGoldGroup;
        private FrmGoldType mFrmGoldType;
        private FrmUser mFrmUser;
        private FrmBaseRule mFrmBaseRule;
        private FrmCustomer mFrmCustomer;
        private FrmInvoiceWholeSale mFrmInvoiceWholeSale;

        public static UserDto userInfo;

        public Image CloseImage;


        public FrmHome()
        {
            InitializeComponent();
        }


        private void FrmHome_Load(object sender, EventArgs e)
        {
            this.drawTabItem();

            if (userInfo != null)
            {
                if (userInfo.IsAdmin == 1){
                    lblUserInfo.Text = userInfo.FullName + " [" + userInfo.UserName + "], Quyền: quản trị";
                } else {
                    lblUserInfo.Text = userInfo.FullName + " [" + userInfo.UserName + "], Quyền: thành viên";
                }
            } else {
                lblUserInfo.Text = "";
            }
            mFrmProductIn = new FrmProductInput();

        }

        public void setUserInfo(UserDto user)
        {
            userInfo = user; 
        }


        private void AddNewTab(FrmSimple frm)
        {
            TabPage tabExist = GetTabByFormName(frm);
            if (tabExist != null)
            {
                tabControl.SelectedTab = tabExist;
                return;
            }

            TabPage tab = new TabPage(frm.Text);

            tab.Tag = frm;

            tab.Name = "Tab" + frm.Name;
            tab.AutoScroll = false;
            tab.Width = this.Width;
            tab.Height = this.Height - 140;


            frm.TopLevel = false;
            frm.Visible = true;
            frm.Width = tab.Width - 20;
            frm.Height = tab.Height;

            frm.FormBorderStyle = FormBorderStyle.None;
            frm.WindowState = FormWindowState.Normal;
            
            frm.AllowDrop = false;
            frm.setFormCallBack(this);
            frm.Text = "";
            frm.MdiParent = this;
            frm.Parent = tab;
            frm.Show();


            tabControl.TabPages.Add(tab);
            frm.Location = new Point((tab.Width - frm.Width) / 2, (tab.Height - frm.Height) / 2);
            tabControl.SelectedTab = tab;
        }

        private TabPage GetTabByFormName(Form frm)
        {
            if (frm == null) return null;
            if (tabControl != null)
            {
                foreach (TabPage tab in tabControl.TabPages)
                {
                    if (tab.Name.Equals("Tab" + frm.Name))
                    {
                        return tab;
                    }
                }
            }
            return null;
        }


        //Method overite of interface IFormCallBack
        public void closeForm(Form formName)
        {
            TabPage tab = GetTabByFormName(formName);
            if (tab != null)
            {
                int index = tabControl.TabPages.IndexOf(tab);
                tabControl.TabPages.RemoveByKey(tab.Name);
                enableMenu(formName.Name, true);

                if (index > 0)
                {
                    tabControl.SelectedIndex = index - 1;
                }
            }
        }

        public void activatedForm(string nameForm)
        {
            enableMenu(nameForm, false);
        }

        protected void enableMenu(string nameForm, bool enable)
        {
            switch (nameForm)
            {
                case "FrmGoldType":
                case "TabFrmGoldType":
                    goldTypeToolStripMenuItem.Enabled = enable;
                    break;
                case "FrmInvoice": 
                case "TabFrmInvoice":
                    InvoiceToolStripMenuItem_Invoice.Enabled = enable;
                    break;
                case "FrmProductIn":
                case "TabFrmProductIn":
                    InputToolStripMenuItem_ProudctIn.Enabled = enable;
                    break;
                case "FrmPrinter":
                case "TabFrmPrinter":
                    printerToolStripMenuItem.Enabled = enable;
                    break;
                case "FrmGoldGroup":
                case "TabFrmGoldGroup":
                    goldGroupToolStripMenuItem.Enabled = enable;
                    break;
                case "FrmUser":
                case "TabFrmUser":
                    userToolStripMenuItem.Enabled = enable;
                    break;
                case "FrmBaseRule":
                case "TabFrmBaseRule":
                    tCCSToolStripMenuItem.Enabled = enable;
                    break;
                case "FrmProductInput":
                case "TabFrmProductInput":
                    InputToolStripMenuItem_ProudctIn.Enabled = enable;
                    break;

                case "FrmCustomer":
                case "TabFrmCustomer":
                    CustomerToolStripMenuItem.Enabled = enable;
                    break;

                case "FrmInvoiceWholeSale":
                case "TabFrmInvoiceWholeSale":
                    WholeSaleToolStripMenuItem.Enabled = enable;
                    break;
            }
        }


        //Event open form from menu
        private void InvoiceToolStripMenuItem_Invoice_Click(object sender, EventArgs e)
        {
            mFrmInvoice = new FrmInvoice();
            AddNewTab(mFrmInvoice);
        }

        private void InputToolStripMenuItem_ProudctIn_Click(object sender, EventArgs e)
        {
            mFrmProductIn = new FrmProductInput();
            this.AddNewTab(mFrmProductIn);
        }

        private void printerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmPrinter = new FrmPrinter();
           this.AddNewTab(mFrmPrinter);
        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmUser = new FrmUser();
           this.AddNewTab(mFrmUser);
        }

        private void tCCSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmBaseRule = new FrmBaseRule();
            this.AddNewTab(mFrmBaseRule);
        }

        private void goldGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmGoldGroup = new FrmGoldGroup();
            this.AddNewTab(mFrmGoldGroup);
        }

        private void goldTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmGoldType = new FrmGoldType();
            this.AddNewTab(mFrmGoldType);
        }

        private void CustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmCustomer = new FrmCustomer();
            this.AddNewTab(mFrmCustomer);
        }

        private void WholeSaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mFrmInvoiceWholeSale = new FrmInvoiceWholeSale();
            this.AddNewTab(mFrmInvoiceWholeSale);
        }




        private void FrmHome_MdiChildActivate(object sender, EventArgs e)
        {   
            if (this.ActiveMdiChild.Tag == null)
            {
                //TabPage tabpage = new TabPage(this.ActiveMdiChild.Text);
                //tabpage.Tag = this.ActiveMdiChild;
                //tabpage.Name = TAB_NAME_PREFIX + this.ActiveMdiChild.Name;
                //tabpage.Parent = tabControl;
                //tabControl.SelectedTab = tabpage;
                
                //this.ActiveMdiChild.Parent = tabpage;
                //this.ActiveMdiChild.Location = new Point(0, 0);
                //this.ActiveMdiChild.Width = tabpage.Width;
                //this.ActiveMdiChild.Height = tabpage.Height;

                //this.ActiveMdiChild.Tag = tabpage;
                
                //(this.ActiveMdiChild as FrmSimple).setFormCallBack(this);
                //this.ActiveMdiChild.FormClosed += new FormClosedEventHandler(ActiveMdiChild_FormClosed);                
            }
        }


        private void tabForms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab != null && tabControl.SelectedTab.Tag != null)
            {
                //(tabControl.SelectedTab.Tag as Form).Select();
                (tabControl.SelectedTab.Tag as Form).MdiParent = this;
                (tabControl.SelectedTab.Tag as Form).Parent = tabControl.SelectedTab;
                (tabControl.SelectedTab.Tag as Form).Show();
            }
        }

        private void FrmHome_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


        #region DRAW BUTTON CLOSE
        private void drawTabItem()
        {
            this.CloseImage = Properties.Resources.img_close;

            this.tabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            //this.tabControl.DrawItem += TabControl_DrawItem;
            this.tabControl.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.TabControl_DrawItem);
            this.tabControl.Padding = new Point(18, 0);
            tabControl.MouseClick += tabControl_MouseClick;
        }

        private void TabControl_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            try
            {
                var tabRect = this.tabControl.GetTabRect(e.Index);
                tabRect.Inflate(-2, -2);
                var imageRect = new Rectangle(tabRect.Right - CloseImage.Width, tabRect.Top + (tabRect.Height - CloseImage.Height) / 2, CloseImage.Width, CloseImage.Height);

                var sf = new StringFormat(StringFormat.GenericDefault);
                if (this.tabControl.RightToLeft == System.Windows.Forms.RightToLeft.Yes && this.tabControl.RightToLeftLayout == true)
                {
                    tabRect = GetRTLCoordinates(this.tabControl.ClientRectangle, tabRect);
                    imageRect = GetRTLCoordinates(this.tabControl.ClientRectangle, imageRect);
                    sf.FormatFlags |= StringFormatFlags.DirectionRightToLeft;
                }

                Font font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                Brush br = new SolidBrush(Color.Gray);

                //Background header
                //e.Graphics.FillRectangle(br, e.Bounds);

                e.Graphics.DrawString(this.tabControl.TabPages[e.Index].Text, font, Brushes.Black, tabRect, sf);
                e.Graphics.DrawImage(CloseImage, imageRect.Location);
            }
            catch (Exception) { }
        }

        private const int waittime = 2;
        private DateTime clickTime = DateTime.Now;
        private void tabControl_MouseClick(object sender, MouseEventArgs e)
        {

            for (var i = 0; i < this.tabControl.TabPages.Count; i++)
            {
                var tabRect = this.tabControl.GetTabRect(i);
                tabRect.Inflate(-5, -5);
                var imageRect = new Rectangle(tabRect.Right - CloseImage.Width,
                                         tabRect.Top + (tabRect.Height - CloseImage.Height) / 2,
                                         CloseImage.Width,
                                         CloseImage.Height);

                if (imageRect.Contains(e.Location))
                {
                    //Anti double click
                    if ((DateTime.Now - clickTime).Seconds < waittime)
                    {
                        return;
                    } else {
                        clickTime = DateTime.Now;
                    }

                    String tabName = this.tabControl.TabPages[i].Name;
                    enableMenu(tabName, true);

                    this.tabControl.TabPages.RemoveAt(i);
                    break;
                }
            }
        }

        public static Rectangle GetRTLCoordinates(Rectangle container, Rectangle drawRectangle)
        {
            return new Rectangle(
                container.Width - drawRectangle.Width - drawRectangle.X,
                drawRectangle.Y,
                drawRectangle.Width,
                drawRectangle.Height);
        }
        #endregion DRAW BUTTON CLOSE 
    }
}
