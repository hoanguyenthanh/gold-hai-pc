﻿namespace GoldManager.Views
{
    partial class FrmInvoiceWholeSale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtProductCodeS = new System.Windows.Forms.TextBox();
            this.txtProductDescS = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtOrderCode = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvProducts = new System.Windows.Forms.DataGridView();
            this.ProductCodeList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductDescList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldCodeList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldWeightS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiamondWeightS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeightS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceSellS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPush = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTotalSalary = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTotalWeight = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtTotalDiamandWeight = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTotalGoldWeight = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTotalCountRows = new System.Windows.Forms.TextBox();
            this.dgvOrderDetail = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldWeightDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiamondWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeightDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Depot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderDetailId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPriceBuy = new System.Windows.Forms.TextBox();
            this.txtPriceSale = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.btnDeleteProductSelected = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.txtMoneyExtand = new System.Windows.Forms.TextBox();
            this.txtMoneyDiscount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dtCreateDate = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnComplete = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnDeleteWholeSale = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtWeightGoldReal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtWeightAll = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOldGold = new System.Windows.Forms.TextBox();
            this.txtSalaryRemaining = new System.Windows.Forms.TextBox();
            this.txtSalaryDiscount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.combGoldType = new System.Windows.Forms.ComboBox();
            this.txtCusAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCusName = new System.Windows.Forms.TextBox();
            this.txtCusCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.txtProductCodeS);
            this.panel2.Controls.Add(this.txtProductDescS);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(499, 30);
            this.panel2.TabIndex = 62;
            // 
            // txtProductCodeS
            // 
            this.txtProductCodeS.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtProductCodeS.Location = new System.Drawing.Point(35, 3);
            this.txtProductCodeS.Name = "txtProductCodeS";
            this.txtProductCodeS.Size = new System.Drawing.Size(119, 24);
            this.txtProductCodeS.TabIndex = 15;
            this.txtProductCodeS.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtProductCodeS_KeyUp);
            // 
            // txtProductDescS
            // 
            this.txtProductDescS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProductDescS.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtProductDescS.Location = new System.Drawing.Point(199, 2);
            this.txtProductDescS.Name = "txtProductDescS";
            this.txtProductDescS.Size = new System.Drawing.Size(231, 24);
            this.txtProductDescS.TabIndex = 16;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(160, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 18);
            this.label17.TabIndex = 53;
            this.label17.Text = "Tên";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 18);
            this.label16.TabIndex = 52;
            this.label16.Text = "Mã";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(436, 0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(63, 30);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtOrderCode
            // 
            this.txtOrderCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtOrderCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtOrderCode.BackColor = System.Drawing.SystemColors.Info;
            this.txtOrderCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderCode.Location = new System.Drawing.Point(319, 9);
            this.txtOrderCode.Name = "txtOrderCode";
            this.txtOrderCode.ReadOnly = true;
            this.txtOrderCode.Size = new System.Drawing.Size(91, 24);
            this.txtOrderCode.TabIndex = 61;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(219, 12);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 18);
            this.label26.TabIndex = 60;
            this.label26.Text = "Mã Toa Hàng";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 73);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.dgvProducts);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.dgvOrderDetail);
            this.splitContainer1.Panel2.Controls.Add(this.btnPush);
            this.splitContainer1.Panel2.Controls.Add(this.label7);
            this.splitContainer1.Panel2.Controls.Add(this.btnDeleteProductSelected);
            this.splitContainer1.Size = new System.Drawing.Size(1497, 453);
            this.splitContainer1.SplitterDistance = 503;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 59;
            // 
            // dgvProducts
            // 
            this.dgvProducts.AllowUserToAddRows = false;
            this.dgvProducts.AllowUserToDeleteRows = false;
            this.dgvProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProducts.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductCodeList,
            this.ProductDescList,
            this.GoldCodeList,
            this.GoldWeightS,
            this.DiamondWeightS,
            this.TotalWeightS,
            this.PriceSellS});
            this.dgvProducts.Location = new System.Drawing.Point(0, 36);
            this.dgvProducts.Name = "dgvProducts";
            this.dgvProducts.Size = new System.Drawing.Size(500, 417);
            this.dgvProducts.TabIndex = 43;
            // 
            // ProductCodeList
            // 
            this.ProductCodeList.DataPropertyName = "ProductCode";
            this.ProductCodeList.HeaderText = "Mã hàng";
            this.ProductCodeList.Name = "ProductCodeList";
            this.ProductCodeList.ReadOnly = true;
            // 
            // ProductDescList
            // 
            this.ProductDescList.DataPropertyName = "ProductDesc";
            this.ProductDescList.HeaderText = "Mô tả";
            this.ProductDescList.Name = "ProductDescList";
            this.ProductDescList.ReadOnly = true;
            // 
            // GoldCodeList
            // 
            this.GoldCodeList.DataPropertyName = "GoldCode";
            this.GoldCodeList.HeaderText = "Loại vàng";
            this.GoldCodeList.Name = "GoldCodeList";
            this.GoldCodeList.ReadOnly = true;
            // 
            // GoldWeightS
            // 
            this.GoldWeightS.DataPropertyName = "GoldWeight";
            this.GoldWeightS.HeaderText = "TL Vàng";
            this.GoldWeightS.Name = "GoldWeightS";
            this.GoldWeightS.ReadOnly = true;
            // 
            // DiamondWeightS
            // 
            this.DiamondWeightS.DataPropertyName = "DiamondWeight";
            this.DiamondWeightS.HeaderText = "TL Đá";
            this.DiamondWeightS.Name = "DiamondWeightS";
            this.DiamondWeightS.ReadOnly = true;
            // 
            // TotalWeightS
            // 
            this.TotalWeightS.DataPropertyName = "TotalWeight";
            this.TotalWeightS.HeaderText = "Tổng TL";
            this.TotalWeightS.Name = "TotalWeightS";
            this.TotalWeightS.ReadOnly = true;
            // 
            // PriceSellS
            // 
            this.PriceSellS.DataPropertyName = "PriceSell";
            this.PriceSellS.HeaderText = "Giá bán";
            this.PriceSellS.Name = "PriceSellS";
            this.PriceSellS.ReadOnly = true;
            // 
            // btnPush
            // 
            this.btnPush.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPush.Location = new System.Drawing.Point(1, 2);
            this.btnPush.Name = "btnPush";
            this.btnPush.Size = new System.Drawing.Size(214, 27);
            this.btnPush.TabIndex = 54;
            this.btnPush.Text = ">>> Thêm hàng vào toa";
            this.btnPush.UseVisualStyleBackColor = true;
            this.btnPush.Click += new System.EventHandler(this.btPush_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.txtTotalAmount);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txtTotalSalary);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.txtTotalWeight);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.txtTotalDiamandWeight);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.txtTotalGoldWeight);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.txtTotalCountRows);
            this.panel1.Location = new System.Drawing.Point(3, 417);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(986, 35);
            this.panel1.TabIndex = 65;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(778, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 18);
            this.label25.TabIndex = 30;
            this.label25.Text = "Thành tiền:";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalAmount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTotalAmount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTotalAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(858, 6);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(123, 24);
            this.txtTotalAmount.TabIndex = 64;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(4, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 18);
            this.label20.TabIndex = 25;
            this.label20.Text = "Số dòng:";
            // 
            // txtTotalSalary
            // 
            this.txtTotalSalary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalSalary.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTotalSalary.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTotalSalary.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalSalary.Location = new System.Drawing.Point(657, 6);
            this.txtTotalSalary.Name = "txtTotalSalary";
            this.txtTotalSalary.Size = new System.Drawing.Size(106, 24);
            this.txtTotalSalary.TabIndex = 64;
            this.txtTotalSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(125, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 18);
            this.label21.TabIndex = 26;
            this.label21.Text = "TL vàng:";
            // 
            // txtTotalWeight
            // 
            this.txtTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalWeight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTotalWeight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTotalWeight.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalWeight.Location = new System.Drawing.Point(480, 6);
            this.txtTotalWeight.Name = "txtTotalWeight";
            this.txtTotalWeight.Size = new System.Drawing.Size(77, 24);
            this.txtTotalWeight.TabIndex = 63;
            this.txtTotalWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(277, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 18);
            this.label22.TabIndex = 27;
            this.label22.Text = "TL đá:";
            // 
            // txtTotalDiamandWeight
            // 
            this.txtTotalDiamandWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDiamandWeight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTotalDiamandWeight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTotalDiamandWeight.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalDiamandWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiamandWeight.Location = new System.Drawing.Point(330, 6);
            this.txtTotalDiamandWeight.Name = "txtTotalDiamandWeight";
            this.txtTotalDiamandWeight.Size = new System.Drawing.Size(75, 24);
            this.txtTotalDiamandWeight.TabIndex = 62;
            this.txtTotalDiamandWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(409, 9);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 18);
            this.label23.TabIndex = 28;
            this.label23.Text = "Tổng TL:";
            // 
            // txtTotalGoldWeight
            // 
            this.txtTotalGoldWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalGoldWeight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTotalGoldWeight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTotalGoldWeight.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalGoldWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalGoldWeight.Location = new System.Drawing.Point(189, 6);
            this.txtTotalGoldWeight.Name = "txtTotalGoldWeight";
            this.txtTotalGoldWeight.Size = new System.Drawing.Size(71, 24);
            this.txtTotalGoldWeight.TabIndex = 61;
            this.txtTotalGoldWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(574, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 18);
            this.label24.TabIndex = 29;
            this.label24.Text = "Tiền công:";
            // 
            // txtTotalCountRows
            // 
            this.txtTotalCountRows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalCountRows.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTotalCountRows.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTotalCountRows.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalCountRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalCountRows.Location = new System.Drawing.Point(78, 6);
            this.txtTotalCountRows.Name = "txtTotalCountRows";
            this.txtTotalCountRows.Size = new System.Drawing.Size(37, 24);
            this.txtTotalCountRows.TabIndex = 60;
            this.txtTotalCountRows.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dgvOrderDetail
            // 
            this.dgvOrderDetail.AllowUserToAddRows = false;
            this.dgvOrderDetail.AllowUserToResizeRows = false;
            this.dgvOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOrderDetail.BackgroundColor = System.Drawing.Color.White;
            this.dgvOrderDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOrderDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvOrderDetail.ColumnHeadersHeight = 30;
            this.dgvOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvOrderDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Check,
            this.ProductCode,
            this.GoldDesc,
            this.GroupName,
            this.ProductDesc,
            this.GoldWeightDT,
            this.DiamondWeight,
            this.TotalWeightDT,
            this.PriceDT,
            this.Salary,
            this.AmountDT,
            this.Depot,
            this.OrderDetailId});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvOrderDetail.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvOrderDetail.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvOrderDetail.Location = new System.Drawing.Point(3, 36);
            this.dgvOrderDetail.MultiSelect = false;
            this.dgvOrderDetail.Name = "dgvOrderDetail";
            this.dgvOrderDetail.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOrderDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvOrderDetail.RowHeadersVisible = false;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvOrderDetail.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvOrderDetail.RowTemplate.Height = 45;
            this.dgvOrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvOrderDetail.Size = new System.Drawing.Size(986, 381);
            this.dgvOrderDetail.TabIndex = 0;
            this.dgvOrderDetail.DataSourceChanged += new System.EventHandler(this.dgvOrderDetail_DataSourceChanged);
            this.dgvOrderDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderDetail_CellContentClick);
            // 
            // Check
            // 
            this.Check.FalseValue = "false";
            this.Check.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Check.HeaderText = "Chọn";
            this.Check.Name = "Check";
            this.Check.ReadOnly = true;
            this.Check.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Check.TrueValue = "true";
            this.Check.Width = 50;
            // 
            // ProductCode
            // 
            this.ProductCode.DataPropertyName = "ProductCode";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ProductCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProductCode.HeaderText = "Mã hàng";
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.ReadOnly = true;
            this.ProductCode.Width = 95;
            // 
            // GoldDesc
            // 
            this.GoldDesc.DataPropertyName = "GoldDesc";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GoldDesc.DefaultCellStyle = dataGridViewCellStyle3;
            this.GoldDesc.HeaderText = "Loại vàng";
            this.GoldDesc.Name = "GoldDesc";
            this.GoldDesc.ReadOnly = true;
            this.GoldDesc.Width = 104;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "GroupName";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GroupName.DefaultCellStyle = dataGridViewCellStyle4;
            this.GroupName.HeaderText = "Nhóm vàng";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            this.GroupName.Width = 114;
            // 
            // ProductDesc
            // 
            this.ProductDesc.DataPropertyName = "ProductDesc";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ProductDesc.DefaultCellStyle = dataGridViewCellStyle5;
            this.ProductDesc.HeaderText = "Diễn giải";
            this.ProductDesc.Name = "ProductDesc";
            this.ProductDesc.ReadOnly = true;
            this.ProductDesc.Width = 250;
            // 
            // GoldWeightDT
            // 
            this.GoldWeightDT.DataPropertyName = "GoldWeightDT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N3";
            dataGridViewCellStyle6.NullValue = null;
            this.GoldWeightDT.DefaultCellStyle = dataGridViewCellStyle6;
            this.GoldWeightDT.HeaderText = "TL Vàng";
            this.GoldWeightDT.Name = "GoldWeightDT";
            this.GoldWeightDT.ReadOnly = true;
            // 
            // DiamondWeight
            // 
            this.DiamondWeight.DataPropertyName = "DiamondWeight";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N3";
            dataGridViewCellStyle7.NullValue = null;
            this.DiamondWeight.DefaultCellStyle = dataGridViewCellStyle7;
            this.DiamondWeight.HeaderText = "TL Đá";
            this.DiamondWeight.Name = "DiamondWeight";
            this.DiamondWeight.ReadOnly = true;
            // 
            // TotalWeightDT
            // 
            this.TotalWeightDT.DataPropertyName = "TotalWeightDT";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N3";
            dataGridViewCellStyle8.NullValue = null;
            this.TotalWeightDT.DefaultCellStyle = dataGridViewCellStyle8;
            this.TotalWeightDT.HeaderText = "Tổng TL";
            this.TotalWeightDT.Name = "TotalWeightDT";
            this.TotalWeightDT.ReadOnly = true;
            this.TotalWeightDT.Width = 110;
            // 
            // PriceDT
            // 
            this.PriceDT.DataPropertyName = "PriceDT";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "#,#";
            dataGridViewCellStyle9.NullValue = null;
            this.PriceDT.DefaultCellStyle = dataGridViewCellStyle9;
            this.PriceDT.HeaderText = "Giá bán";
            this.PriceDT.Name = "PriceDT";
            this.PriceDT.ReadOnly = true;
            // 
            // Salary
            // 
            this.Salary.DataPropertyName = "Salary";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Salary.DefaultCellStyle = dataGridViewCellStyle10;
            this.Salary.HeaderText = "Tiền công";
            this.Salary.Name = "Salary";
            this.Salary.ReadOnly = true;
            this.Salary.Width = 105;
            // 
            // AmountDT
            // 
            this.AmountDT.DataPropertyName = "AmountDT";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Format = "#,#";
            this.AmountDT.DefaultCellStyle = dataGridViewCellStyle11;
            this.AmountDT.HeaderText = "Thành Tiền";
            this.AmountDT.Name = "AmountDT";
            this.AmountDT.ReadOnly = true;
            // 
            // Depot
            // 
            this.Depot.DataPropertyName = "Depot";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Depot.DefaultCellStyle = dataGridViewCellStyle12;
            this.Depot.HeaderText = "Kệ";
            this.Depot.Name = "Depot";
            this.Depot.ReadOnly = true;
            this.Depot.Width = 52;
            // 
            // OrderDetailId
            // 
            this.OrderDetailId.DataPropertyName = "OrderDetailId";
            this.OrderDetailId.HeaderText = "OrderDetailId";
            this.OrderDetailId.Name = "OrderDetailId";
            this.OrderDetailId.ReadOnly = true;
            this.OrderDetailId.Visible = false;
            // 
            // txtPriceBuy
            // 
            this.txtPriceBuy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPriceBuy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPriceBuy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPriceBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPriceBuy.Location = new System.Drawing.Point(710, 40);
            this.txtPriceBuy.Name = "txtPriceBuy";
            this.txtPriceBuy.Size = new System.Drawing.Size(173, 24);
            this.txtPriceBuy.TabIndex = 58;
            // 
            // txtPriceSale
            // 
            this.txtPriceSale.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPriceSale.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPriceSale.BackColor = System.Drawing.SystemColors.Window;
            this.txtPriceSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPriceSale.Location = new System.Drawing.Point(499, 40);
            this.txtPriceSale.Name = "txtPriceSale";
            this.txtPriceSale.Size = new System.Drawing.Size(106, 24);
            this.txtPriceSale.TabIndex = 57;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(636, 43);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 18);
            this.label19.TabIndex = 56;
            this.label19.Text = "Giá mua";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(429, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 18);
            this.label18.TabIndex = 55;
            this.label18.Text = "Giá bán";
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCustomer.Location = new System.Drawing.Point(1350, 7);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(100, 27);
            this.btnAddCustomer.TabIndex = 51;
            this.btnAddCustomer.Text = "Chọn KH";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // btnDeleteProductSelected
            // 
            this.btnDeleteProductSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteProductSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProductSelected.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteProductSelected.Location = new System.Drawing.Point(808, 2);
            this.btnDeleteProductSelected.Name = "btnDeleteProductSelected";
            this.btnDeleteProductSelected.Size = new System.Drawing.Size(181, 27);
            this.btnDeleteProductSelected.TabIndex = 50;
            this.btnDeleteProductSelected.Text = "Xóa hàng đã chọn";
            this.btnDeleteProductSelected.UseVisualStyleBackColor = true;
            this.btnDeleteProductSelected.Click += new System.EventHandler(this.btnDeleteProductSelected_Click);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(324, 567);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 18);
            this.label15.TabIndex = 49;
            this.label15.Text = "Ghi chú";
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.Location = new System.Drawing.Point(390, 564);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(328, 24);
            this.txtNote.TabIndex = 48;
            // 
            // txtMoneyExtand
            // 
            this.txtMoneyExtand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtMoneyExtand.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoneyExtand.Location = new System.Drawing.Point(880, 534);
            this.txtMoneyExtand.Name = "txtMoneyExtand";
            this.txtMoneyExtand.Size = new System.Drawing.Size(116, 24);
            this.txtMoneyExtand.TabIndex = 47;
            this.txtMoneyExtand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMoneyExtand.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMoneyExtand_KeyPress);
            // 
            // txtMoneyDiscount
            // 
            this.txtMoneyDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtMoneyDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoneyDiscount.Location = new System.Drawing.Point(391, 535);
            this.txtMoneyDiscount.Name = "txtMoneyDiscount";
            this.txtMoneyDiscount.Size = new System.Drawing.Size(327, 24);
            this.txtMoneyDiscount.TabIndex = 46;
            this.txtMoneyDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMoneyDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMoneyDiscount_KeyPress);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(749, 537);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 18);
            this.label6.TabIndex = 45;
            this.label6.Text = "Tiền thu thêm";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(324, 535);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 44;
            this.label5.Text = "Tiền bớt";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 18);
            this.label14.TabIndex = 42;
            this.label14.Text = "Ngày lập";
            // 
            // dtCreateDate
            // 
            this.dtCreateDate.CustomFormat = "dd-MM-yyyy";
            this.dtCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCreateDate.Location = new System.Drawing.Point(87, 9);
            this.dtCreateDate.Name = "dtCreateDate";
            this.dtCreateDate.Size = new System.Drawing.Size(122, 24);
            this.dtCreateDate.TabIndex = 41;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(1383, 605);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(126, 30);
            this.btnExit.TabIndex = 40;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnComplete
            // 
            this.btnComplete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnComplete.Location = new System.Drawing.Point(1119, 605);
            this.btnComplete.Name = "btnComplete";
            this.btnComplete.Size = new System.Drawing.Size(125, 30);
            this.btnComplete.TabIndex = 38;
            this.btnComplete.Text = "Hoàn tất";
            this.btnComplete.UseVisualStyleBackColor = true;
            this.btnComplete.Click += new System.EventHandler(this.btnComplete_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(998, 605);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(115, 30);
            this.btnPrint.TabIndex = 37;
            this.btnPrint.Text = "In";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnDeleteWholeSale
            // 
            this.btnDeleteWholeSale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteWholeSale.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteWholeSale.Location = new System.Drawing.Point(1250, 605);
            this.btnDeleteWholeSale.Name = "btnDeleteWholeSale";
            this.btnDeleteWholeSale.Size = new System.Drawing.Size(127, 30);
            this.btnDeleteWholeSale.TabIndex = 35;
            this.btnDeleteWholeSale.Text = "Xóa toa hàng";
            this.btnDeleteWholeSale.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(850, 605);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(142, 30);
            this.btnUpdate.TabIndex = 34;
            this.btnUpdate.Text = "Tính tiền";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1291, 573);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 18);
            this.label13.TabIndex = 33;
            this.label13.Text = "TL vàng";
            // 
            // txtWeightGoldReal
            // 
            this.txtWeightGoldReal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtWeightGoldReal.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeightGoldReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightGoldReal.Location = new System.Drawing.Point(1388, 567);
            this.txtWeightGoldReal.Name = "txtWeightGoldReal";
            this.txtWeightGoldReal.Size = new System.Drawing.Size(120, 24);
            this.txtWeightGoldReal.TabIndex = 32;
            this.txtWeightGoldReal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWeightGoldReal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWeightGoldReal_KeyPress);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1027, 570);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 18);
            this.label12.TabIndex = 31;
            this.label12.Text = "TL hột";
            // 
            // txtWeightAll
            // 
            this.txtWeightAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtWeightAll.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeightAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightAll.Location = new System.Drawing.Point(880, 564);
            this.txtWeightAll.Name = "txtWeightAll";
            this.txtWeightAll.Size = new System.Drawing.Size(116, 24);
            this.txtWeightAll.TabIndex = 29;
            this.txtWeightAll.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWeightAll.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWeightAll_KeyPress);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(750, 567);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(111, 18);
            this.label11.TabIndex = 28;
            this.label11.Text = "TL vàng + hột";
            // 
            // txtOldGold
            // 
            this.txtOldGold.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtOldGold.BackColor = System.Drawing.SystemColors.Info;
            this.txtOldGold.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOldGold.Location = new System.Drawing.Point(986, 40);
            this.txtOldGold.Name = "txtOldGold";
            this.txtOldGold.Size = new System.Drawing.Size(358, 24);
            this.txtOldGold.TabIndex = 27;
            this.txtOldGold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOldGold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOldGold_KeyPress);
            // 
            // txtSalaryRemaining
            // 
            this.txtSalaryRemaining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtSalaryRemaining.BackColor = System.Drawing.SystemColors.Info;
            this.txtSalaryRemaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalaryRemaining.Location = new System.Drawing.Point(1388, 535);
            this.txtSalaryRemaining.Name = "txtSalaryRemaining";
            this.txtSalaryRemaining.Size = new System.Drawing.Size(121, 24);
            this.txtSalaryRemaining.TabIndex = 26;
            this.txtSalaryRemaining.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSalaryRemaining.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalaryRemaining_KeyPress);
            // 
            // txtSalaryDiscount
            // 
            this.txtSalaryDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtSalaryDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalaryDiscount.Location = new System.Drawing.Point(1131, 534);
            this.txtSalaryDiscount.Name = "txtSalaryDiscount";
            this.txtSalaryDiscount.Size = new System.Drawing.Size(121, 24);
            this.txtSalaryDiscount.TabIndex = 25;
            this.txtSalaryDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSalaryDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalaryDiscount_KeyPress);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(908, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 18);
            this.label10.TabIndex = 24;
            this.label10.Text = "Tuổi vàng";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1291, 538);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 18);
            this.label9.TabIndex = 23;
            this.label9.Text = "Công còn lại";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1027, 537);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 18);
            this.label8.TabIndex = 22;
            this.label8.Text = "Tiền công bớt";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(377, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(259, 18);
            this.label7.TabIndex = 20;
            this.label7.Text = "DANH SÁCH HÀNG TRONG TOA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "Loại vàng";
            // 
            // combGoldType
            // 
            this.combGoldType.FormattingEnabled = true;
            this.combGoldType.Location = new System.Drawing.Point(86, 40);
            this.combGoldType.Name = "combGoldType";
            this.combGoldType.Size = new System.Drawing.Size(324, 26);
            this.combGoldType.TabIndex = 14;
            this.combGoldType.SelectedIndexChanged += new System.EventHandler(this.combGoldType_SelectedIndexChanged);
            // 
            // txtCusAddress
            // 
            this.txtCusAddress.BackColor = System.Drawing.SystemColors.Info;
            this.txtCusAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusAddress.Location = new System.Drawing.Point(986, 8);
            this.txtCusAddress.Multiline = true;
            this.txtCusAddress.Name = "txtCusAddress";
            this.txtCusAddress.ReadOnly = true;
            this.txtCusAddress.Size = new System.Drawing.Size(358, 25);
            this.txtCusAddress.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(911, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Địa chỉ";
            // 
            // txtCusName
            // 
            this.txtCusName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCusName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCusName.BackColor = System.Drawing.SystemColors.Info;
            this.txtCusName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusName.Location = new System.Drawing.Point(710, 8);
            this.txtCusName.Name = "txtCusName";
            this.txtCusName.ReadOnly = true;
            this.txtCusName.Size = new System.Drawing.Size(173, 24);
            this.txtCusName.TabIndex = 11;
            // 
            // txtCusCode
            // 
            this.txtCusCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCusCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCusCode.BackColor = System.Drawing.SystemColors.Info;
            this.txtCusCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusCode.Location = new System.Drawing.Point(499, 9);
            this.txtCusCode.Name = "txtCusCode";
            this.txtCusCode.ReadOnly = true;
            this.txtCusCode.Size = new System.Drawing.Size(106, 24);
            this.txtCusCode.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(635, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Tên KH";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(428, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Mã KH";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(1131, 567);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 24);
            this.textBox1.TabIndex = 62;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FrmInvoiceWholeSale
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1521, 647);
            this.ControlBox = true;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtOrderCode);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.txtPriceBuy);
            this.Controls.Add(this.txtPriceSale);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.btnAddCustomer);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.txtMoneyExtand);
            this.Controls.Add(this.txtMoneyDiscount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.dtCreateDate);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnComplete);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnDeleteWholeSale);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtWeightGoldReal);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtWeightAll);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtOldGold);
            this.Controls.Add(this.txtSalaryRemaining);
            this.Controls.Add(this.txtSalaryDiscount);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.combGoldType);
            this.Controls.Add(this.txtCusAddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCusName);
            this.Controls.Add(this.txtCusCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmInvoiceWholeSale";
            this.Text = "TOA HÀNG";
            this.Load += new System.EventHandler(this.FrmInvoiceWholeSale_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected DevComponents.DotNetBar.Controls.DataGridViewX dgvOrderDetail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCusCode;
        private System.Windows.Forms.TextBox txtCusName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCusAddress;
        private System.Windows.Forms.TextBox txtProductCodeS;
        private System.Windows.Forms.TextBox txtProductDescS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSalaryDiscount;
        private System.Windows.Forms.TextBox txtSalaryRemaining;
        private System.Windows.Forms.TextBox txtOldGold;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtWeightAll;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtWeightGoldReal;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDeleteWholeSale;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnComplete;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DateTimePicker dtCreateDate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgvProducts;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMoneyDiscount;
        private System.Windows.Forms.TextBox txtMoneyExtand;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnDeleteProductSelected;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox combGoldType;
        private System.Windows.Forms.Button btnPush;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPriceSale;
        private System.Windows.Forms.TextBox txtPriceBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCodeList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductDescList;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldCodeList;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldWeightS;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiamondWeightS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWeightS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceSellS;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.TextBox txtTotalSalary;
        private System.Windows.Forms.TextBox txtTotalWeight;
        private System.Windows.Forms.TextBox txtTotalDiamandWeight;
        private System.Windows.Forms.TextBox txtTotalGoldWeight;
        private System.Windows.Forms.TextBox txtTotalCountRows;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldWeightDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiamondWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWeightDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Depot;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderDetailId;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtOrderCode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBox1;

    }
}