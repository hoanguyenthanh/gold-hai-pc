﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoldManager.Views.UserControl;
using GoldManager.Process;
using GoldManager.Models;
using GoldManager.Res;

namespace GoldManager.Views
{
    public partial class FrmGoldGroup : FrmSimple
    {
        private string groupCodeCurrent;

        public FrmGoldGroup()
        {
            InitializeComponent();
        }

        private void FrmGoldGroup_Load(object sender, EventArgs e)
        {
            loadGroup();
        }

        private void loadGroup()
        {
            using (GoldGroup_Process proGet = new GoldGroup_Process())
            {
                proGet.intActionName = GoldGroup_Process.ACT_GET_GROUP;
                proGet.ExecuteProcess();
                this.dgvGroup.AutoGenerateColumns = false;
                this.dgvGroup.DataSource = proGet.DtResult;

                selectRow();
            }
        }

        private void selectRow()
        {
            if (!string.IsNullOrEmpty(this.groupCodeCurrent) && this.dgvGroup != null && this.dgvGroup.RowCount > 0)
            {
                dgvGroup.ClearSelection();
                foreach (DataGridViewRow row in this.dgvGroup.Rows)
                {
                    string _code = row.Cells["GroupCode"].Value.ToString();
                    row.Selected = _code.Equals(this.groupCodeCurrent) ? true : false;
                }
            }
        }

        private void dgvGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvGroup_CellClick(object sender, DataGridViewCellEventArgs e)
        {   
            DataGridView grid = sender as DataGridView;
            if (grid != null && grid.CurrentRow != null && grid.CurrentRow.Index > -1)
            {
                bindDataRowToTextBox(grid.CurrentRow);
            }
        }

        private void bindDataRowToTextBox(DataGridViewRow row)
        {
            groupCodeCurrent = row.Cells["GroupCode"].Value.ToString();
            txtGroupCode.Text = row.Cells["GroupCode"].Value.ToString();
            txtGroupName.Text = row.Cells["GroupName"].Value.ToString();

            string active = row.Cells["Active"].Value.ToString();
            cbxActiveYesNo.Checked = active.Equals(MConts.DATA_ACTIVE.ToString()) ? true : false;
            
            selectRow();
        }

        private void dgvGroup_Sorted(object sender, EventArgs e)
        {
            selectRow();
        }


        private bool isValidInsert()
        {
            if (isExistGroupcode())
            {
                showWarning(string.Format("Nhóm vàng \"{0}\" đã tồn tại, vui lòng kiểm tra lại", txtGroupCode.Text));
                return false;
            }

            if (string.IsNullOrEmpty(txtGroupCode.Text) || string.IsNullOrEmpty(txtGroupName.Text))
            {
                showWarning("Vui lòng nhập đầy đủ: \"Mã nhóm\", \"Tên nhóm\"");
                return false;
            }
            return true;
        }

        private bool isValidUpdate()
        {
            if (string.IsNullOrEmpty(txtGroupName.Text))
            {
                showWarning("Vui lòng nhập: \"Tên nhóm\"");
                return false;
            }
            return true;
        }

        /*
         * Create GroupID manual
         */
        private string createGroupID()
        {
            using (GoldGroup_Process process = new GoldGroup_Process())
            {
                process.intActionName = GoldGroup_Process.ACT_CREATE_GROUPD_ID;
                process.ExecuteProcess();
                return string.IsNullOrEmpty(process.strGroupID) ? "" : process.strGroupID;
            }
        }

        private bool isExistGroupcode()
        {
            using (GoldGroup_Process process = new GoldGroup_Process())
            {
                process.intActionName = GoldGroup_Process.ACT_EXISTS_GROUP;
                process.strGroupCode = txtGroupCode.Text;
                process.ExecuteProcess();

                return process.isExistGroupCode; 
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            int result = -1;

            if (string.IsNullOrEmpty(this.groupCodeCurrent))
            {
                if (isValidInsert())
                {
                    string groupId = createGroupID();
                    using (GoldGroup_Process process = new GoldGroup_Process())
                    {
                        process.intActionName = GoldGroup_Process.ACT_IST_GROUP;
                        process.strGroupID = groupId;
                        process.strGroupCode = txtGroupCode.Text;
                        process.strGroupName = txtGroupName.Text;
                        process.intActive = cbxActiveYesNo.Checked ? MConts.DATA_ACTIVE : MConts.DATA_UNACTIVE;

                        result = process.ExecuteProcess();
                    }
                }
            }
            else if (!string.IsNullOrEmpty(this.groupCodeCurrent))
            {
                if (isValidUpdate())
                {
                    using (GoldGroup_Process process = new GoldGroup_Process())
                    {
                        process.intActionName = GoldGroup_Process.ACT_UPD_GROUP;
                        process.strGroupCode = txtGroupCode.Text;
                        process.strGroupName = txtGroupName.Text;
                        process.intActive = cbxActiveYesNo.Checked ? MConts.DATA_ACTIVE : MConts.DATA_UNACTIVE;

                        result = process.ExecuteProcess();
                    }
                }
            }

            if (result > 0)
            {
                groupCodeCurrent = txtGroupCode.Text;
                loadGroup();
                setStatusItems(STATUS_ITEMS_NORMAL);
            } else if (result == 0)
            {
                showWarning("Không thể thêm hoặc cập nhật loại vàng");
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            groupCodeCurrent = "";
            txtGroupCode.Text = "";
            txtGroupName.Text = "";
            setStatusItems(STATUS_ITEMS_ADD);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            setStatusItems(STATUS_ITEMS_MOD);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            setStatusItems(STATUS_ITEMS_CEL);
        }

        protected override void setStatusItems(string status)
        {
            switch (status)
            {
                case STATUS_ITEMS_ADD:
                    txtGroupCode.Enabled = true;
                    txtGroupName.Enabled = true;
                    cbxActiveYesNo.Enabled = true;

                    btnAdd.Enabled = false;
                    btnModify.Enabled = false;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    break;

                case STATUS_ITEMS_MOD:
                    txtGroupCode.Enabled = false;
                    txtGroupName.Enabled = true;
                    cbxActiveYesNo.Enabled = true;

                    btnAdd.Enabled = false;
                    btnModify.Enabled = false;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    break;
                case STATUS_ITEMS_SAVE:
                    txtGroupCode.Enabled = false;
                    txtGroupName.Enabled = false;
                    cbxActiveYesNo.Enabled = false;

                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    break;
                case STATUS_ITEMS_CEL:
                    txtGroupCode.Enabled = false;
                    txtGroupName.Enabled = false;
                    cbxActiveYesNo.Enabled = false;

                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    break;

                case STATUS_ITEMS_NORMAL:
                    txtGroupCode.Enabled = false;
                    txtGroupName.Enabled = false;
                    cbxActiveYesNo.Enabled = false;

                    btnAdd.Enabled = true;
                    btnModify.Enabled = true;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    break;
            }
        }
    }
}
