﻿namespace GoldManager.Views
{
    partial class FrmBaseRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBaseRule));
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.btnModify = new DevComponents.DotNetBar.ButtonX();
            this.cbxActive = new System.Windows.Forms.CheckBox();
            this.txtBaseRuleDesc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblBaseRuleName = new DevComponents.DotNetBar.LabelX();
            this.txtBaseRuleName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblBaseRuleDesc = new DevComponents.DotNetBar.LabelX();
            this.btnAdd = new DevComponents.DotNetBar.ButtonX();
            this.grdBaseRule = new System.Windows.Forms.DataGridView();
            this.BaseRuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseRuleDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiveDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseRuleId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdBaseRule)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(619, 76);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(99, 32);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(501, 76);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(99, 32);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnModify
            // 
            this.btnModify.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnModify.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnModify.Image = ((System.Drawing.Image)(resources.GetObject("btnModify.Image")));
            this.btnModify.Location = new System.Drawing.Point(383, 76);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(99, 32);
            this.btnModify.TabIndex = 9;
            this.btnModify.Text = "Sửa";
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // cbxActive
            // 
            this.cbxActive.AutoSize = true;
            this.cbxActive.Location = new System.Drawing.Point(625, 15);
            this.cbxActive.Name = "cbxActive";
            this.cbxActive.Size = new System.Drawing.Size(93, 21);
            this.cbxActive.TabIndex = 8;
            this.cbxActive.Text = "Hoạt động";
            this.cbxActive.UseVisualStyleBackColor = true;
            // 
            // txtBaseRuleDesc
            // 
            // 
            // 
            // 
            this.txtBaseRuleDesc.Border.Class = "TextBoxBorder";
            this.txtBaseRuleDesc.Location = new System.Drawing.Point(319, 10);
            this.txtBaseRuleDesc.Name = "txtBaseRuleDesc";
            this.txtBaseRuleDesc.Size = new System.Drawing.Size(281, 23);
            this.txtBaseRuleDesc.TabIndex = 7;
            // 
            // lblBaseRuleName
            // 
            this.lblBaseRuleName.Location = new System.Drawing.Point(12, 12);
            this.lblBaseRuleName.Name = "lblBaseRuleName";
            this.lblBaseRuleName.Size = new System.Drawing.Size(73, 21);
            this.lblBaseRuleName.TabIndex = 0;
            this.lblBaseRuleName.Text = "Tên TCCS";
            // 
            // txtBaseRuleName
            // 
            // 
            // 
            // 
            this.txtBaseRuleName.Border.Class = "TextBoxBorder";
            this.txtBaseRuleName.Location = new System.Drawing.Point(91, 10);
            this.txtBaseRuleName.Name = "txtBaseRuleName";
            this.txtBaseRuleName.Size = new System.Drawing.Size(133, 23);
            this.txtBaseRuleName.TabIndex = 1;
            // 
            // lblBaseRuleDesc
            // 
            this.lblBaseRuleDesc.Location = new System.Drawing.Point(265, 12);
            this.lblBaseRuleDesc.Name = "lblBaseRuleDesc";
            this.lblBaseRuleDesc.Size = new System.Drawing.Size(48, 21);
            this.lblBaseRuleDesc.TabIndex = 0;
            this.lblBaseRuleDesc.Text = "Mô tả";
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(265, 76);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(99, 32);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grdBaseRule
            // 
            this.grdBaseRule.AllowUserToAddRows = false;
            this.grdBaseRule.AllowUserToDeleteRows = false;
            this.grdBaseRule.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.grdBaseRule.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdBaseRule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdBaseRule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BaseRuleName,
            this.BaseRuleDesc,
            this.ActiveDesc,
            this.Active,
            this.BaseRuleId});
            this.grdBaseRule.Location = new System.Drawing.Point(6, 117);
            this.grdBaseRule.Name = "grdBaseRule";
            this.grdBaseRule.ReadOnly = true;
            this.grdBaseRule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdBaseRule.Size = new System.Drawing.Size(711, 422);
            this.grdBaseRule.TabIndex = 13;
            this.grdBaseRule.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdBaseRule_CellClick);
            this.grdBaseRule.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdBaseRule_CellContentClick);
            this.grdBaseRule.Sorted += new System.EventHandler(this.grdBaseRule_Sorted);
            // 
            // BaseRuleName
            // 
            this.BaseRuleName.DataPropertyName = "BaseRuleName";
            this.BaseRuleName.HeaderText = "Tên tiêu chuẩn";
            this.BaseRuleName.Name = "BaseRuleName";
            this.BaseRuleName.ReadOnly = true;
            this.BaseRuleName.Width = 200;
            // 
            // BaseRuleDesc
            // 
            this.BaseRuleDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaseRuleDesc.DataPropertyName = "BaseRuleDesc";
            this.BaseRuleDesc.HeaderText = "Diễn giải";
            this.BaseRuleDesc.Name = "BaseRuleDesc";
            this.BaseRuleDesc.ReadOnly = true;
            // 
            // ActiveDesc
            // 
            this.ActiveDesc.DataPropertyName = "ActiveDesc";
            this.ActiveDesc.HeaderText = "Hoạt động";
            this.ActiveDesc.Name = "ActiveDesc";
            this.ActiveDesc.ReadOnly = true;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Visible = false;
            // 
            // BaseRuleId
            // 
            this.BaseRuleId.DataPropertyName = "BaseRuleId";
            this.BaseRuleId.HeaderText = "BaseRuleId";
            this.BaseRuleId.Name = "BaseRuleId";
            this.BaseRuleId.ReadOnly = true;
            this.BaseRuleId.Visible = false;
            // 
            // FrmBaseRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(729, 551);
            this.ControlBox = true;
            this.Controls.Add(this.grdBaseRule);
            this.Controls.Add(this.cbxActive);
            this.Controls.Add(this.txtBaseRuleDesc);
            this.Controls.Add(this.lblBaseRuleName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtBaseRuleName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblBaseRuleDesc);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnAdd);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmBaseRule";
            this.Text = "Tiêu chuẩn cơ sở";
            this.Load += new System.EventHandler(this.FrmBaseRule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdBaseRule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.ButtonX btnModify;
        private System.Windows.Forms.CheckBox cbxActive;
        private DevComponents.DotNetBar.Controls.TextBoxX txtBaseRuleDesc;
        private DevComponents.DotNetBar.LabelX lblBaseRuleName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtBaseRuleName;
        private DevComponents.DotNetBar.LabelX lblBaseRuleDesc;
        private DevComponents.DotNetBar.ButtonX btnAdd;
        private System.Windows.Forms.DataGridView grdBaseRule;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseRuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseRuleDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActiveDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseRuleId;
    }
}