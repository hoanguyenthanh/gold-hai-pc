﻿namespace GoldManager.Views
{
    partial class FrmGoldTypeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dgvGoldType = new System.Windows.Forms.DataGridView();
            this.GoldCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceSell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoldType)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vui lòng chọn loại vàng";
            // 
            // dgvGoldType
            // 
            this.dgvGoldType.AllowUserToAddRows = false;
            this.dgvGoldType.AllowUserToDeleteRows = false;
            this.dgvGoldType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGoldType.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvGoldType.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvGoldType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGoldType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GoldCode,
            this.GoldDesc,
            this.PriceSell,
            this.PriceBuy});
            this.dgvGoldType.Location = new System.Drawing.Point(3, 29);
            this.dgvGoldType.Name = "dgvGoldType";
            this.dgvGoldType.ReadOnly = true;
            this.dgvGoldType.RowHeadersVisible = false;
            this.dgvGoldType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGoldType.Size = new System.Drawing.Size(606, 259);
            this.dgvGoldType.TabIndex = 1;
            this.dgvGoldType.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGoldType_CellContentClick);
            this.dgvGoldType.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGoldType_CellDoubleClick);
            this.dgvGoldType.SelectionChanged += new System.EventHandler(this.dgvGoldType_SelectionChanged);
            // 
            // GoldCode
            // 
            this.GoldCode.DataPropertyName = "GoldCode";
            this.GoldCode.HeaderText = "Mã";
            this.GoldCode.Name = "GoldCode";
            this.GoldCode.ReadOnly = true;
            // 
            // GoldDesc
            // 
            this.GoldDesc.DataPropertyName = "GoldDesc";
            this.GoldDesc.HeaderText = "Tên loại";
            this.GoldDesc.Name = "GoldDesc";
            this.GoldDesc.ReadOnly = true;
            // 
            // PriceSell
            // 
            this.PriceSell.DataPropertyName = "PriceSell";
            this.PriceSell.HeaderText = "Giá bán";
            this.PriceSell.Name = "PriceSell";
            this.PriceSell.ReadOnly = true;
            // 
            // PriceBuy
            // 
            this.PriceBuy.DataPropertyName = "PriceBuy";
            this.PriceBuy.HeaderText = "Giá mua";
            this.PriceBuy.Name = "PriceBuy";
            this.PriceBuy.ReadOnly = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(518, 293);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 37);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Location = new System.Drawing.Point(421, 293);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(91, 37);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "Chọn";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // FrmGoldTypeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(614, 342);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvGoldType);
            this.Controls.Add(this.label1);
            this.Name = "FrmGoldTypeDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách loại vàng";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FrmGoldTypeDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoldType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView dgvGoldType;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoldDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceSell;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceBuy;
    }
}