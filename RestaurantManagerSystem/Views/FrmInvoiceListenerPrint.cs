﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using System.Data;
using GoldManager.Res.Reports;
using GoldManager.Process;
using GoldManager.Models;

namespace GoldManager.Views
{
    partial class FrmInvoice
    {
        /***************************************PRINTER FROM ANDROID******************************************/

        #region START PRINTER LISTENER
        private PageSettings mPageSetting;
        private PrinterSettings mPrintSetting;


        //Variable printer listener
        private static bool listennerPrinter = false;
        private System.Timers.Timer timer = new System.Timers.Timer(1000);
        private bool isPrinting;
        private long OrderIdPrint = 0;
        private string TypeTrans = "";
        private DataRow[] DtRowInvoiceReport;
        private DataRow[] DtRowInvoiceBuyPrintReport;
        private DataTable DtInvoiceReport = new DataTable("invoice");
        private DataTable DtInvoiceBuyPrintReport = new DataTable("invoice_buy");

        //Report
        private RptInvoiceSell rptInvoiceSell;
        private RptInvoiceBuy rptInvoiceBuy;
        private RptInvoiceSellBuy rptInvoiceSellBuy;


        //Flag is first load to initizal printer listener
        private bool IsFirstLoad { get; set; }

        private void initPagePrint()
        {
            mPrintSetting = new PrinterSettings();

            if (!string.IsNullOrEmpty(Properties.Settings.Default.printer))
            {
                mPrintSetting.PrinterName = Properties.Settings.Default.printer;
            }
            else
            {
                showWarning("Vui lòng cài đặt máy in");
            }
            mPageSetting = new PageSettings(mPrintSetting);
            mPageSetting.Landscape = false;
        }


        private void printInvoice(object source, System.Timers.ElapsedEventArgs e)
        {
            //Check has statement waitting.
            if (!isPrinting)
            {
                using (Order_Process process = new Order_Process())
                {
                    ResultObject result = process.checkPrinterWaitting();

                    if (result.Result > 0){
                        OrderIdPrint = process.OrderId;
                        TypeTrans = process.TypeTrans;
                    }
                }
            }


            if (OrderIdPrint > 0 && TypeTrans.Length > 0)
            {
                this.isPrinting = true;
                //Get data report
                using (RptGetInvoiceList_Process process = new RptGetInvoiceList_Process())
                {
                    process.OrderId = OrderIdPrint;
                    process.ExecuteProcess();
                    DtInvoiceReport = process.DtResult;

                }

                //Update flag report printed
                using (Order_Process proces = new Order_Process())
                {
                    proces.updateInvoicePrinted(OrderIdPrint);
                }
                this.OrderIdPrint = 0;


                if (TypeTrans != null && TypeTrans.Length > 0)
                {
                    if (TypeTrans == "sell")
                    {
                        rptInvoiceSell = new RptInvoiceSell();
                        rptInvoiceSell.SummaryInfo.ReportTitle = "HoaDon" + DateTime.Now.ToString();
                        rptInvoiceSell.SetDataSource(this.DtInvoiceReport);
                        rptInvoiceSell.PrintToPrinter(mPrintSetting, mPageSetting, false);
                    }
                    else if (TypeTrans == "sell_and_buy")
                    {
                        this.DtRowInvoiceBuyPrintReport = DtInvoiceReport.Select("TypeTrans = 'buy'");
                        this.DtRowInvoiceReport = DtInvoiceReport.Select("TypeTrans = 'sell'");

                        if (DtRowInvoiceBuyPrintReport != null && DtRowInvoiceBuyPrintReport.Length > 0)
                        {
                            this.DtInvoiceBuyPrintReport = this.DtRowInvoiceBuyPrintReport.CopyToDataTable();
                        }
                        else
                        {
                            this.DtInvoiceBuyPrintReport = new DataTable();
                        }

                        if (this.DtRowInvoiceReport != null && this.DtRowInvoiceReport.Length > 0)
                        {
                            this.DtInvoiceReport = this.DtRowInvoiceReport.CopyToDataTable();
                        }
                        else
                        {
                            this.DtInvoiceReport = new DataTable();
                        }

                        rptInvoiceSellBuy = new RptInvoiceSellBuy();
                        rptInvoiceSellBuy.SummaryInfo.ReportTitle = "HoaDon" + DateTime.Now.ToString();

                        rptInvoiceSellBuy.SetDataSource(this.DtInvoiceReport);
                        rptInvoiceSellBuy.Subreports[0].SetDataSource(this.DtInvoiceBuyPrintReport);

                        rptInvoiceSellBuy.PrintToPrinter(mPrintSetting, mPageSetting, false);
                    }
                    else if (TypeTrans == "buy")
                    {
                        this.DtInvoiceBuyPrintReport = DtInvoiceReport.Select("TypeTrans = 'buy'").CopyToDataTable();
                        this.rptInvoiceBuy = new RptInvoiceBuy();

                        rptInvoiceBuy.SummaryInfo.ReportTitle = "HoaDon" + DateTime.Now.ToString();
                        rptInvoiceBuy.SetDataSource(this.DtInvoiceBuyPrintReport);
                        rptInvoiceBuy.PrintToPrinter(mPrintSetting, mPageSetting, false);
                    }
                }

                //reset value
                this.DtInvoiceBuyPrintReport.Clear();
                this.DtInvoiceReport.Clear();
                this.TypeTrans = "";
                this.isPrinting = false;

                if (this.rptInvoiceSell != null)
                {
                    this.rptInvoiceSell.Dispose();
                    this.rptInvoiceSell.Close();
                }
                if (this.rptInvoiceBuy != null)
                {
                    this.rptInvoiceBuy.Dispose();
                    this.rptInvoiceBuy.Close();
                }
                if (this.rptInvoiceSellBuy != null)
                {
                    this.rptInvoiceSellBuy.Dispose();
                    this.rptInvoiceSellBuy.Close();
                }
            }
        }

        private void btnListennerPrinter_Click(object sender, EventArgs e)
        {
            if (listennerPrinter || IsFirstLoad)
            {
                timer.Enabled = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(printInvoice);
                timer.AutoReset = true;

                listennerPrinter = false;
                IsFirstLoad = false;
            }
            else
            {
                timer.Enabled = false;
                listennerPrinter = true;
            }
        }
        #endregion END PRINTER LISTENER
    }
}
