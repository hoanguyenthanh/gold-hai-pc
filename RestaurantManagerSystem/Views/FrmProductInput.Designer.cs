﻿using System.Windows.Forms;
namespace GoldManager.Views
{
    partial class FrmProductInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductInput));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnPrintBarcode = new System.Windows.Forms.Button();
            this.grdProducts = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ProductCode_Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiamondWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseRule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StallsDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrintYNDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeight_Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoldlWeight_Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiamondWeight_Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SectionID_Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StallsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrnID_Old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdProducts)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(1435, 13);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(99, 32);
            this.btnCopy.TabIndex = 24;
            this.btnCopy.Text = "Sao chép";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnPrintBarcode
            // 
            this.btnPrintBarcode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrintBarcode.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintBarcode.Image")));
            this.btnPrintBarcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrintBarcode.Location = new System.Drawing.Point(253, 13);
            this.btnPrintBarcode.Name = "btnPrintBarcode";
            this.btnPrintBarcode.Size = new System.Drawing.Size(103, 32);
            this.btnPrintBarcode.TabIndex = 23;
            this.btnPrintBarcode.Text = "In tem";
            this.btnPrintBarcode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrintBarcode.Click += new System.EventHandler(this.btnPrintBarcode_Click);
            // 
            // grdProducts
            // 
            this.grdProducts.AllowUserToAddRows = false;
            this.grdProducts.AllowUserToDeleteRows = false;
            this.grdProducts.AllowUserToResizeRows = false;
            this.grdProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProducts.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProducts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk,
            this.ProductCode_Old,
            this.ProductCode,
            this.ProductDesc,
            this.GoldWeight,
            this.DiamondWeight,
            this.TotalWeight,
            this.GoldCode,
            this.GoldDesc,
            this.GroupName,
            this.Salary,
            this.BaseRule,
            this._date,
            this._time,
            this.StallsDesc,
            this.PrintYNDesc,
            this.TotalWeight_Old,
            this.GoldlWeight_Old,
            this.DiamondWeight_Old,
            this.SectionID_Old,
            this.StallsId,
            this.TrnID_Old,
            this.GroupID,
            this.ProductId});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProducts.DefaultCellStyle = dataGridViewCellStyle15;
            this.grdProducts.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.grdProducts.Location = new System.Drawing.Point(12, 62);
            this.grdProducts.MultiSelect = false;
            this.grdProducts.Name = "grdProducts";
            this.grdProducts.ReadOnly = true;
            this.grdProducts.RowHeadersWidth = 30;
            this.grdProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdProducts.Size = new System.Drawing.Size(1522, 556);
            this.grdProducts.TabIndex = 16;
            this.grdProducts.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProducts_CellContentClick);
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(12, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnAdd.Size = new System.Drawing.Size(106, 33);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(369, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnDelete.Size = new System.Drawing.Size(113, 33);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnModify.Image = ((System.Drawing.Image)(resources.GetObject("btnModify.Image")));
            this.btnModify.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModify.Location = new System.Drawing.Point(133, 12);
            this.btnModify.Name = "btnModify";
            this.btnModify.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnModify.Size = new System.Drawing.Size(106, 33);
            this.btnModify.TabIndex = 19;
            this.btnModify.Text = "Sửa";
            this.btnModify.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // chk
            // 
            this.chk.HeaderText = "Chọn";
            this.chk.Name = "chk";
            this.chk.ReadOnly = true;
            this.chk.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.chk.Width = 45;
            // 
            // ProductCode_Old
            // 
            this.ProductCode_Old.DataPropertyName = "ProductCode_Old";
            this.ProductCode_Old.HeaderText = "Mã (Cũ)";
            this.ProductCode_Old.Name = "ProductCode_Old";
            this.ProductCode_Old.ReadOnly = true;
            // 
            // ProductCode
            // 
            this.ProductCode.DataPropertyName = "ProductCode";
            this.ProductCode.HeaderText = "Mã";
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.ReadOnly = true;
            this.ProductCode.Width = 106;
            // 
            // ProductDesc
            // 
            this.ProductDesc.DataPropertyName = "ProductDesc";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ProductDesc.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProductDesc.HeaderText = "Mô tả";
            this.ProductDesc.Name = "ProductDesc";
            this.ProductDesc.ReadOnly = true;
            this.ProductDesc.Width = 200;
            // 
            // GoldWeight
            // 
            this.GoldWeight.DataPropertyName = "GoldWeight";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N3";
            dataGridViewCellStyle3.NullValue = "0";
            this.GoldWeight.DefaultCellStyle = dataGridViewCellStyle3;
            this.GoldWeight.HeaderText = "TL vàng";
            this.GoldWeight.Name = "GoldWeight";
            this.GoldWeight.ReadOnly = true;
            this.GoldWeight.Width = 105;
            // 
            // DiamondWeight
            // 
            this.DiamondWeight.DataPropertyName = "DiamondWeight";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N3";
            dataGridViewCellStyle4.NullValue = "0";
            this.DiamondWeight.DefaultCellStyle = dataGridViewCellStyle4;
            this.DiamondWeight.HeaderText = "TL đá";
            this.DiamondWeight.Name = "DiamondWeight";
            this.DiamondWeight.ReadOnly = true;
            this.DiamondWeight.Width = 106;
            // 
            // TotalWeight
            // 
            this.TotalWeight.DataPropertyName = "TotalWeight";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N3";
            dataGridViewCellStyle5.NullValue = "0";
            this.TotalWeight.DefaultCellStyle = dataGridViewCellStyle5;
            this.TotalWeight.HeaderText = "TL tổng";
            this.TotalWeight.Name = "TotalWeight";
            this.TotalWeight.ReadOnly = true;
            this.TotalWeight.Width = 106;
            // 
            // GoldCode
            // 
            this.GoldCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.GoldCode.DataPropertyName = "GoldCode";
            this.GoldCode.HeaderText = "Loại vàng";
            this.GoldCode.Name = "GoldCode";
            this.GoldCode.ReadOnly = true;
            this.GoldCode.Width = 95;
            // 
            // GoldDesc
            // 
            this.GoldDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.GoldDesc.DataPropertyName = "GoldDesc";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GoldDesc.DefaultCellStyle = dataGridViewCellStyle6;
            this.GoldDesc.HeaderText = "Diễn giải loại vàng";
            this.GoldDesc.Name = "GoldDesc";
            this.GoldDesc.ReadOnly = true;
            this.GoldDesc.Width = 200;
            // 
            // GroupName
            // 
            this.GroupName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.GroupName.DataPropertyName = "GroupName";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.GroupName.DefaultCellStyle = dataGridViewCellStyle7;
            this.GroupName.HeaderText = "Nhóm";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            this.GroupName.Width = 70;
            // 
            // Salary
            // 
            this.Salary.DataPropertyName = "Salary";
            dataGridViewCellStyle8.Format = "#,#";
            this.Salary.DefaultCellStyle = dataGridViewCellStyle8;
            this.Salary.HeaderText = "Tiền công";
            this.Salary.Name = "Salary";
            this.Salary.ReadOnly = true;
            // 
            // BaseRule
            // 
            this.BaseRule.DataPropertyName = "BaseRule";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BaseRule.DefaultCellStyle = dataGridViewCellStyle9;
            this.BaseRule.HeaderText = "TCCS";
            this.BaseRule.Name = "BaseRule";
            this.BaseRule.ReadOnly = true;
            this.BaseRule.Width = 106;
            // 
            // _date
            // 
            this._date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._date.DataPropertyName = "_date";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._date.DefaultCellStyle = dataGridViewCellStyle10;
            this._date.FillWeight = 78.17259F;
            this._date.HeaderText = "Ngày";
            this._date.Name = "_date";
            this._date.ReadOnly = true;
            this._date.Width = 66;
            // 
            // _time
            // 
            this._time.DataPropertyName = "_time";
            this._time.HeaderText = "Giờ";
            this._time.Name = "_time";
            this._time.ReadOnly = true;
            // 
            // StallsDesc
            // 
            this.StallsDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.StallsDesc.DataPropertyName = "StallsDesc";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.StallsDesc.DefaultCellStyle = dataGridViewCellStyle11;
            this.StallsDesc.FillWeight = 121.8274F;
            this.StallsDesc.HeaderText = "Quầy";
            this.StallsDesc.Name = "StallsDesc";
            this.StallsDesc.ReadOnly = true;
            this.StallsDesc.Width = 67;
            // 
            // PrintYNDesc
            // 
            this.PrintYNDesc.DataPropertyName = "PrintYNDesc";
            this.PrintYNDesc.HeaderText = "In tem";
            this.PrintYNDesc.Name = "PrintYNDesc";
            this.PrintYNDesc.ReadOnly = true;
            this.PrintYNDesc.Width = 106;
            // 
            // TotalWeight_Old
            // 
            this.TotalWeight_Old.DataPropertyName = "TotalWeight_Old";
            dataGridViewCellStyle12.NullValue = "0";
            this.TotalWeight_Old.DefaultCellStyle = dataGridViewCellStyle12;
            this.TotalWeight_Old.HeaderText = "TL tổng (Cũ)";
            this.TotalWeight_Old.Name = "TotalWeight_Old";
            this.TotalWeight_Old.ReadOnly = true;
            // 
            // GoldlWeight_Old
            // 
            this.GoldlWeight_Old.DataPropertyName = "GoldlWeight_Old";
            dataGridViewCellStyle13.NullValue = "0";
            this.GoldlWeight_Old.DefaultCellStyle = dataGridViewCellStyle13;
            this.GoldlWeight_Old.HeaderText = "TL vàng (Cũ)";
            this.GoldlWeight_Old.Name = "GoldlWeight_Old";
            this.GoldlWeight_Old.ReadOnly = true;
            // 
            // DiamondWeight_Old
            // 
            this.DiamondWeight_Old.DataPropertyName = "DiamondWeight_Old";
            dataGridViewCellStyle14.NullValue = "0";
            this.DiamondWeight_Old.DefaultCellStyle = dataGridViewCellStyle14;
            this.DiamondWeight_Old.HeaderText = "TL đá (Cũ)";
            this.DiamondWeight_Old.Name = "DiamondWeight_Old";
            this.DiamondWeight_Old.ReadOnly = true;
            // 
            // SectionID_Old
            // 
            this.SectionID_Old.HeaderText = "SectionID_Old";
            this.SectionID_Old.Name = "SectionID_Old";
            this.SectionID_Old.ReadOnly = true;
            this.SectionID_Old.Visible = false;
            // 
            // StallsId
            // 
            this.StallsId.DataPropertyName = "StallsId";
            this.StallsId.HeaderText = "StallsId";
            this.StallsId.Name = "StallsId";
            this.StallsId.ReadOnly = true;
            this.StallsId.Visible = false;
            // 
            // TrnID_Old
            // 
            this.TrnID_Old.HeaderText = "TrnID_Old";
            this.TrnID_Old.Name = "TrnID_Old";
            this.TrnID_Old.ReadOnly = true;
            this.TrnID_Old.Visible = false;
            // 
            // GroupID
            // 
            this.GroupID.DataPropertyName = "GroupID";
            this.GroupID.HeaderText = "GroupID";
            this.GroupID.Name = "GroupID";
            this.GroupID.ReadOnly = true;
            this.GroupID.Visible = false;
            // 
            // ProductId
            // 
            this.ProductId.DataPropertyName = "ProductId";
            this.ProductId.HeaderText = "ProductId";
            this.ProductId.Name = "ProductId";
            this.ProductId.ReadOnly = true;
            this.ProductId.Visible = false;
            // 
            // FrmProductInput
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1546, 630);
            this.ControlBox = true;
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnPrintBarcode);
            this.Controls.Add(this.grdProducts);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmProductInput";
            this.Text = "Nhập hàng";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmProductInput_FormClosing);
            this.Load += new System.EventHandler(this.FrmProductIn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdProducts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.DataGridView grdProducts;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnPrintBarcode;
        private Button btnCopy;
        private DataGridViewCheckBoxColumn chk;
        private DataGridViewTextBoxColumn ProductCode_Old;
        private DataGridViewTextBoxColumn ProductCode;
        private DataGridViewTextBoxColumn ProductDesc;
        private DataGridViewTextBoxColumn GoldWeight;
        private DataGridViewTextBoxColumn DiamondWeight;
        private DataGridViewTextBoxColumn TotalWeight;
        private DataGridViewTextBoxColumn GoldCode;
        private DataGridViewTextBoxColumn GoldDesc;
        private DataGridViewTextBoxColumn GroupName;
        private DataGridViewTextBoxColumn Salary;
        private DataGridViewTextBoxColumn BaseRule;
        private DataGridViewTextBoxColumn _date;
        private DataGridViewTextBoxColumn _time;
        private DataGridViewTextBoxColumn StallsDesc;
        private DataGridViewTextBoxColumn PrintYNDesc;
        private DataGridViewTextBoxColumn TotalWeight_Old;
        private DataGridViewTextBoxColumn GoldlWeight_Old;
        private DataGridViewTextBoxColumn DiamondWeight_Old;
        private DataGridViewTextBoxColumn SectionID_Old;
        private DataGridViewTextBoxColumn StallsId;
        private DataGridViewTextBoxColumn TrnID_Old;
        private DataGridViewTextBoxColumn GroupID;
        private DataGridViewTextBoxColumn ProductId;
    }
}