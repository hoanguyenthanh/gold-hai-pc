﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class Product
    {
        public long ProductId;
        public string ProductCode;
        public int StallsId;
        public string GoldCode;
        public string ProductDesc;
        public string GroupID;
        public decimal Quantity;
        
        public decimal GoldWeight;
        public decimal DiamondWeight;
        public decimal TotalWeight;
        public int TotalWeightRounding;//Số chữ số làm tròn
        public decimal StampWeight;

        public string PriceCcy;
        public string KyHieu;
        public string NCC;
        public DateTime ModDt;
        public long Salary;
        public int PrintYN;
        public string BaseRule;
        public int Printing;

        public string ProductCode_Old;
        public string TrnID_Old;
        public string SectionID_Old;

        public decimal TotalWeight_Old;
        public decimal GoldlWeight_Old;
        public decimal DiamondWeight_Old;
    }
}
