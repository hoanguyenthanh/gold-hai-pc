﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class TypeInvoice
    {
        public const string KEY_SI_VALUE = "si";
        public const string KEY_SI_NAME = "Sỉ";

        public const string KEY_LE_VALUE = "le";
        public const string KEY_LE_NAME = "Lẻ";

        public string type {get; set;}
        public string name { get; set; }

        public TypeInvoice(string t, string n)
        {
            this.type = t;
            this.name = n;
        }
    }
}
