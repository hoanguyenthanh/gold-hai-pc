﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class OrderData
    {
        public string Method;
        public long OrderId;
        public long OrderDetailId;
        public long ProductId;

        public string TypeTrans; //'sell' or 'buy'
        public string ActionTrans; //

        public string ModUser;
        public string TrnID;
        public string ProductCode;
        public string ProductDesc;

        public decimal DiamondWeight;
        public decimal TotalWeightDT;
        
        public int TotalWeightDT_L;
        public int TotalWeightDT_C;
        public int TotalWeightDT_P;
        public int TotalWeightDT_Li;
        public string TotalWeightDTDesc;

        public decimal PriceDT;
        public decimal PayBonus;
        public decimal PayBonusAmount;
        public decimal Salary;
        public decimal AmountDT;


        public int Depot;
        public string GroupID;
        public string GoldType;

        public int CusId;
    }
}
