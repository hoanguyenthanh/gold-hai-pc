﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldManager.Models
{
    public class UserDto
    {
        public string UserId {get; set;}
        public string UserName {get; set;}
        public string FullName {get; set;}
        public int IsAdmin {get; set;}
    }
}
