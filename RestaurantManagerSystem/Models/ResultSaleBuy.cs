﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class ResultSaleBuy
    {
        public int Result = -1;
        public string ErrorDesc = string.Empty;
        public long OrderId;
        public long OrderDetailId;
    }
}
