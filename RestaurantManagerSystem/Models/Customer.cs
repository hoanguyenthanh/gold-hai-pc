﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class Customer
    {
        public int CusId;
        public string CusCode;
        public string CusName;
        public string Phone;
        public string Address;
        public string CMND;
        public string Note;
    }
}
