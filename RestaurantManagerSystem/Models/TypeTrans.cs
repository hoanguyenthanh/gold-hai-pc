﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class TypeTrans
    {
        public string Trans { get; set; }
        public string Name { get; set; }

        public TypeTrans(string trans, string name)
        {
            this.Trans = trans;
            this.Name = name;
        }
    }
}
