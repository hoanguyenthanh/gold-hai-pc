﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class ResultObject
    {
        public int Result;
        public string ErrorDesc;

        public ResultObject()
        {
            Result = 1;
            ErrorDesc = "";
        }
    }
}
