﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoldManager.Models
{
    public class GoldType
    {
        public string GoldCode;
        public string GoldDesc;
        public string PriceCcy;
        public string PriceUnit;
        public string Notes;
        public int OrderBy;
        public int Active;
        public string Old;
        public double HS;
        public decimal PriceSell;
        public decimal PriceBuy;

        

        public void clear(){
            GoldCode = "";
            GoldDesc = "";
            PriceCcy = "";

            PriceUnit = "";
            Notes = "";
            OrderBy = 0;
            Active = 1;
            Old = "0";
            HS = 0;
            PriceSell = 0;
            PriceBuy = 0;
        }
    }
}
